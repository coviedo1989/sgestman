package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.carlos.sgestman.entity.Solicitudes

@Dao
interface SolicitudesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(solicitudes: Solicitudes?)

    @get:Query("SELECT * from Solicitudes")
    val allSolicitud: LiveData<Solicitudes?>?

    @Query("DELETE FROM Solicitudes")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSolicitud(solicitudes: Solicitudes?)

    @Query("SELECT * FROM Solicitudes ORDER BY fecha DESC")
    fun allSolicitudPagin(): DataSource.Factory<Int, Solicitudes>

}