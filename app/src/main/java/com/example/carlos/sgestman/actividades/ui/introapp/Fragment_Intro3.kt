package com.example.carlos.sgestman.actividades.ui.introapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.carlos.sgestman.R

/**
 * A simple [Fragment] subclass.
 */
class Fragment_Intro3 : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_fragment__intro3, container, false)
        val textView = view.findViewById<View>(R.id.textView40) as TextView
        //Typeface font = Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Black.ttf");
        // textView.setTypeface(font);
        return view
    }
}