package com.example.carlos.sgestman.entity

import com.example.carlos.sgestman.model.SDTSDDashboard
import com.google.gson.annotations.SerializedName

class DashBoard
{
    @SerializedName("SDTSDDashboard")
    var sDTSDDashboard: SDTSDDashboard? = null

    init {
        sDTSDDashboard = SDTSDDashboard()
    }

}