package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.entity.OrdenReporteWS

///interfaz para las orden reporte del servidor
@Dao
interface OrdenReporteWSDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ordenReporte: OrdenReporteWS?)

    @get:Query("SELECT * from OrdenReporteWS")
    val allOrdenReportews: LiveData<List<OrdenReporteWS>?>?

    @Query("DELETE FROM OrdenReporteWS")
    fun deleteAll()

    @Query("DELETE FROM OrdenReporteWS where numeroOT = :numeroOT")
    fun deleteByNumeroOT(numeroOT:String)

    @Delete
    fun delete(ordenReporte: OrdenReporteWS?)

    @Query("Select * from OrdenReporteWS where numeroOT = :numeroOT")
    fun selectByNumeroOT(numeroOT:String):List<OrdenReporteWS>
}