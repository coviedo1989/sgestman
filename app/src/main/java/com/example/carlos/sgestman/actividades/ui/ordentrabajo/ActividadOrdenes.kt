package com.example.carlos.sgestman.actividades.ui.ordentrabajo

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.actividades.ui.solicitud.ActividadCrearSolicitud
import com.example.carlos.sgestman.adapter.CustomLoadStateAdapter
import com.example.carlos.sgestman.adapter.OrdenesAdapter
import com.example.carlos.sgestman.adapter.OrdenesAdapterListener
import com.example.carlos.sgestman.entity.Ordenes
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.constant.REQUESTCODE
import com.example.carlos.sgestman.utils.enums.EnumOrdenes
import com.example.carlos.sgestman.viewmodel.OrdenesViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_solicitudes.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*


class ActividadOrdenes : AppCompatActivity(),OrdenesAdapterListener
{
    private var iscargo = false
    private var isbyFilter: Boolean? = null
    private lateinit var toolbar: Toolbar
    private var estadoInt = 0
    //private val viewModelRecent by viewModels<SolicitudesViewModel1>()
    lateinit var viewModelRecent: OrdenesViewModel
    var adapter : OrdenesAdapter? =null
    var orden: Ordenes?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_ordenes)
        Init()
        setupView()


    }
    private fun Init()
    {
        viewModelRecent = OrdenesViewModel()
        isbyFilter = Prefs.getBoolean(PREFS.FILTER_ORD_ISFILTER,false)
        //viewModelRecent = ViewModelProvider(this)[SolicitudesViewModel1::class.java]

    }

    private fun setupView()
    {
        setTollbar()
        adapter = OrdenesAdapter(applicationContext,this)
        reciclerview.layoutManager = (LinearLayoutManager(this))
        reciclerview.adapter =  adapter?.withLoadStateFooter(footer = CustomLoadStateAdapter(adapter!!::retry,applicationContext))

        adapter!!.addLoadStateListener { loadState ->

//            Log.e("raya" , "---------------------")
//            Log.e("loadState.append" , loadState.append.endOfPaginationReached.toString())
//            Log.e("loadState.refresh" , loadState.refresh.endOfPaginationReached.toString())
//            Log.e("loadState.source.refresh ", loadState.source.refresh.endOfPaginationReached.toString())
//            Log.e("is Refresh ", (loadState.source.refresh is LoadState.NotLoading).toString())

            isbyFilter = Prefs.getBoolean(PREFS.FILTER_ORD_ISFILTER,false)
            if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached)
            {
                when {
                    adapter!!.itemCount < 1 -> {
                        showStateEmpty(isbyFilter!!)
                        iscargo = false
                    }
                }
            }
            else if (adapter!!.itemCount > 1 && !iscargo)
            {
                iscargo = true
                showStateEmpty(isbyFilter!!)
            }

        }

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary)
        swipeRefreshLayout.setOnRefreshListener { getOrdenes() }



        getOrdenes()
    }

    fun setTollbar()
    {
        toolbar = findViewById<View>(R.id.my_awesome_toolbar) as Toolbar
        toolbar.title = ""
        toolbar.navigationIcon= ContextCompat.getDrawable(this,R.drawable.ic_left_arrow)
        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        val mtitleToolbar = toolbar.findViewById(R.id.toolbar_title) as TextView
        mtitleToolbar.text = "Órdenes"
    }


    private suspend fun showOnRecyclerView1(t: PagingData<Ordenes>)
    {
        adapter!!.submitData(t)

      //  reciclerview.adapter = adapter1?.withLoadStateFooter(footer = CustomLoadStateAdapter(adapter1!!::retry))

        if(swipeRefreshLayout.isRefreshing)
        {
            swipeRefreshLayout.isRefreshing = false
        }

        //showStateEmpty(isbyFilter!!)

    }

    fun getOrdenes()
    {
        lifecycleScope.launch{
            //@OptIn(ExperimentalCoroutinesApi::class)
            viewModelRecent.itemsOrdenes.collectLatest { pagingData ->
                //showStateEmpty(isbyFilter!!)
                showOnRecyclerView1(pagingData)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_filter, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        val id = item.itemId

        if (id == R.id.menu_filter)
        {
            ShowFilter()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun addOrden()
    {
        val intent = Intent(this, ActividadCrearSolicitud::class.java)
        startActivityForResult(intent, REQUESTCODE.ACTIVIDAD_ADD_SOL)
    }

    private fun ShowFilter()
    {

        val mBottomSheetDialog = BottomSheetDialog(this, R.style.BaseBottomSheetDialog_BottomSheetDialogThemeRound)
        val sheetView = layoutInflater.inflate(R.layout.filter_ordenes, null)

        mBottomSheetDialog.setContentView(sheetView)
        val mBehavior = BottomSheetBehavior.from(sheetView.parent as View)
        mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        mBottomSheetDialog.setCancelable(true)
        mBottomSheetDialog.show()

        val btnAplicar = sheetView.findViewById<Button>(R.id.btnAplicar)
        val btnLimpiar = sheetView.findViewById<Button>(R.id.btnLimpiar)

        val codigoOrden = sheetView.findViewById<TextView>(R.id.codigo_orden)
        val numeroOT = sheetView.findViewById<TextView>(R.id.numero_OT)
        val codigoCliente = sheetView.findViewById<TextView>(R.id.codigoCliente)
        val codigoEjecutor = sheetView.findViewById<TextView>(R.id.codigoEjecutor)
        val codigoObjeto = sheetView.findViewById<TextView>(R.id.codigoObjeto)
        val estadoOT = sheetView.findViewById<ChipGroup>(R.id.estadoOT)
        val fechaInicio = sheetView.findViewById<EditText>(R.id.fechaInicio)
        val fechaFin = sheetView.findViewById<EditText>(R.id.fechaFin)


        OnclickImageDateFechaView(sheetView)

        estadoOT.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.chipNoInciada -> {
                    Prefs.putInt(PREFS.FILTER_ORD_ESTADO, EnumOrdenes.NOINICIADA.estate)
                    estadoInt = EnumOrdenes.NOINICIADA.estate
                }
                R.id.chipPermiso -> {
                    Prefs.putInt(PREFS.FILTER_ORD_ESTADO, EnumOrdenes.PERMISOSEG.estate)
                    estadoInt = EnumOrdenes.PERMISOSEG.estate
                }
                R.id.chipVigente -> {
                    Prefs.putInt(PREFS.FILTER_ORD_ESTADO, EnumOrdenes.VIGENTE.estate)
                    estadoInt = EnumOrdenes.VIGENTE.estate
                }
                R.id.chipPendiente -> {
                    Prefs.putInt(PREFS.FILTER_ORD_ESTADO, EnumOrdenes.PENDIENTE.estate)
                    estadoInt = EnumOrdenes.PENDIENTE.estate
                }
                R.id.chipTerminada -> {
                    Prefs.putInt(PREFS.FILTER_ORD_ESTADO, EnumOrdenes.TERMINADA.estate)
                    estadoInt = EnumOrdenes.TERMINADA.estate
                }
                R.id.chipConfirmada -> {
                    Prefs.putInt(PREFS.FILTER_ORD_ESTADO, EnumOrdenes.CONFIRMADA.estate)
                    estadoInt = EnumOrdenes.CONFIRMADA.estate
                }
                R.id.chipCancelada -> {
                    Prefs.putInt(PREFS.FILTER_SOL_ESTADO, EnumOrdenes.CANCELADA.estate)
                    estadoInt = EnumOrdenes.CANCELADA.estate
                }
            }

        }


        AddFormato(fechaInicio,fechaFin)

        ///
        LlenarFiltros(sheetView)

        btnAplicar.setOnClickListener(View.OnClickListener
        {
            filtros(codigoOrden.text.toString(),
                    numeroOT.text.toString(),
                    codigoCliente.text.toString(),
                    codigoEjecutor.text.toString(),
                    codigoObjeto.text.toString(),
                    estadoInt,
                    fechaInicio.text.toString(),
                    fechaFin.text.toString())
            mBottomSheetDialog.dismiss()
            getOrdenes()
        })
        btnLimpiar.setOnClickListener(View.OnClickListener
        {
            limpiarFiltros()
            mBottomSheetDialog.dismiss()
            getOrdenes()
        })
    }

    @SuppressLint("SetTextI18n")
    private fun OnclickImageDateFechaView(sheetView: View?)
    {

        val fechaInicio = sheetView?.findViewById<EditText>(R.id.fechaInicio)
        val fechaFin = sheetView?.findViewById<EditText>(R.id.fechaFin)
        val fechaInicioImg = sheetView?.findViewById<ImageView>(R.id.fechaInicioImg)
        val fechaFinImg = sheetView?.findViewById<ImageView>(R.id.fechaFinImg)


        fechaInicioImg?.setOnClickListener {

            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->

                        val dia = if (dayOfMonth < 10) {"0$dayOfMonth"}else {"$dayOfMonth"}
                        val mes = if (month < 10) {"0$month"}else {"$month"}
                        fechaInicio?.setText("$dia/$mes/$year")
                    }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }

        fechaFinImg?.setOnClickListener {

            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                        val dia = if (dayOfMonth < 10) {"0$dayOfMonth"}else {"$dayOfMonth"}
                        val mes = if (month < 10) {"0$month"}else {"$month"}
                        fechaFin?.setText("$dia/$mes/$year")
                    }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }
    }

    private fun AddFormato(fechaInicio:EditText,fechaFin:EditText)
    {
        fechaInicio.addTextChangedListener(object:TextWatcher
        {
            var sb : StringBuilder = StringBuilder("")

            var _ignore = false

            override fun afterTextChanged(s: Editable?)
            {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
            {

                if(_ignore){
                    _ignore = false
                    return
                }

                sb.clear()
                sb.append(if(s!!.length > 10){ s.subSequence(0,10) }else{ s })

                if(sb.lastIndex == 2)
                {
                    when {
                        sb[2] != '/' -> {
                            val sb1 = sb.toString().substring(1..2)
                            if(sb1.toInt() in 1..31) {
                                sb.insert(2,"/")
                            } else {
                                sb.clear()
                            }
                        }
                    }

                } else if(sb.lastIndex == 5)
                {
                    when {
                        sb[5] != '/' -> {
                            val sb1 = sb.toString().substring(3..4)
                            if (sb1.toString().toInt() in 2..12) {
                                sb.insert(5,"/")
                            } else {
                                sb.clear()
                            }
                        }
                    }

                }
                else if(sb.lastIndex == 9)
                {
                    val sb1 = sb.toString().substring(6,10)
                    when {
                        sb1.toInt() < 2015 || sb1.toInt() > 2030 -> {
                            sb.clear()
                        }
                    }
                }

                _ignore = true
                fechaInicio.setText(sb.toString())
                fechaInicio.setSelection(sb.length)
            }
        })
        fechaFin.addTextChangedListener(object:TextWatcher
        {
            var sb : StringBuilder = StringBuilder("")

            var _ignore = false

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
            {

                if(_ignore){
                    _ignore = false
                    return
                }

                sb.clear()
                sb.append(if(s!!.length > 10){ s.subSequence(0,10) }else{ s })

                if(sb.lastIndex == 2)
                {
                    when {
                        sb[2] != '/' -> {
                            val sb1 = sb.toString().substring(1..2)
                            if(sb1.toString().toInt() in 1..31) {
                                sb.insert(2,"/")
                            } else {
                                sb.clear()
                            }
                        }
                    }

                } else if(sb.lastIndex == 5)
                {
                    when {
                        sb[5] != '/' -> {
                            val sb1 = sb.toString().substring(3..4)
                            if (sb1.toString().toInt() in 2..12) {
                                sb.insert(5,"/")
                            } else {
                                sb.clear()
                            }
                        }
                    }

                }
                else if(sb.lastIndex == 9)
                {
                    val sb1 = sb.toString().substring(6,10)
                    when {
                        sb1.toInt() < 2015 || sb1.toInt() > 2030 -> {
                            sb.clear()
                        }
                    }
                }

                _ignore = true
                fechaFin.setText(sb.toString())
                fechaFin.setSelection(sb.length)
            }
        })

        fechaInicio.setOnFocusChangeListener { view, focus -> onFocusEditext(focus,view) }
        fechaFin.setOnFocusChangeListener { view, focus -> onFocusEditext(focus,view) }
    }

    private fun onFocusEditext(b: Boolean,view:View)
    {
        view as TextView
        when {
            !b && view.text.length< 10-> {
                view.text = ""
            }
        }
    }

    private fun LlenarFiltros(sheetView: View)
    {
        val codigoOrden = sheetView.findViewById<TextView>(R.id.codigo_orden)
        val numeroOT = sheetView.findViewById<TextView>(R.id.numero_OT)
        val codigoCliente = sheetView.findViewById<TextView>(R.id.codigoCliente)
        val codigoEjecutor = sheetView.findViewById<TextView>(R.id.codigoEjecutor)
        val codigoObjeto = sheetView.findViewById<TextView>(R.id.codigoObjeto)
        val fechaInicio = sheetView.findViewById<TextView>(R.id.fechaInicio)
        val fechaFin = sheetView.findViewById<TextView>(R.id.fechaFin)
        val estadoOT = sheetView.findViewById<ChipGroup>(R.id.estadoOT)

        codigoOrden.text = Prefs.getString(PREFS.FILTER_ORD_CODIGO,"")
        numeroOT.text = Prefs.getString(PREFS.FILTER_ORD_NUMERO,"")
        codigoCliente.text = Prefs.getString(PREFS.FILTER_ORD_CLIENTE,"")
        codigoEjecutor.text = Prefs.getString(PREFS.FILTER_ORD_EJECUTOR,"")
        codigoObjeto.text = Prefs.getString(PREFS.FILTER_ORD_OBJETO,"")
        fechaInicio.text = Prefs.getString(PREFS.FILTER_ORD_FECHAINICIO,"")
        fechaFin.text = Prefs.getString(PREFS.FILTER_ORD_FECHAFIN,"")
        val estadoint = Prefs.getInt(PREFS.FILTER_ORD_ESTADO,0)

        when {
            estadoint != 0 -> {
                val chip = estadoOT.getChildAt(estadoint-1) as Chip
                chip.isChecked = true
            }
        }


    }


    private fun filtros(codigo:String,numeroOT:String, codigoCliente:String, codigoEjecutor:String, codigoObjeto:String, estado:Int, fechaInicio:String, fechaFin:String)
    {
        Prefs.putString(PREFS.FILTER_ORD_CODIGO, codigo)
        Prefs.putString(PREFS.FILTER_ORD_NUMERO, numeroOT)
        Prefs.putString(PREFS.FILTER_ORD_CLIENTE, codigoCliente)
        Prefs.putString(PREFS.FILTER_ORD_EJECUTOR, codigoEjecutor)
        Prefs.putString(PREFS.FILTER_ORD_OBJETO, codigoObjeto)
        Prefs.putString(PREFS.FILTER_ORD_FECHAINICIO, fechaInicio)
        Prefs.putString(PREFS.FILTER_ORD_FECHAFIN, fechaFin)
        Prefs.putInt(PREFS.FILTER_ORD_ESTADO,estado)
        Prefs.putBoolean(PREFS.FILTER_ORD_ISFILTER,true)
        viewModelRecent = OrdenesViewModel()
    }
    private fun limpiarFiltros()
    {
        Prefs.putString(PREFS.FILTER_ORD_CODIGO,"")
        Prefs.putString(PREFS.FILTER_ORD_NUMERO,"")
        Prefs.putString(PREFS.FILTER_ORD_CLIENTE,"")
        Prefs.putString(PREFS.FILTER_ORD_EJECUTOR,"")
        Prefs.putString(PREFS.FILTER_ORD_OBJETO,"")
        Prefs.putString(PREFS.FILTER_ORD_FECHAINICIO,"")
        Prefs.putString(PREFS.FILTER_ORD_FECHAFIN,"")
        Prefs.putInt(PREFS.FILTER_ORD_ESTADO,0)
        Prefs.putBoolean(PREFS.FILTER_ORD_ISFILTER,false)
        viewModelRecent = OrdenesViewModel()
    }
    private fun showStateEmpty(isbyFiltro:Boolean)
    {
        when {
            isbyFiltro ->
            {

                ly_estate.setOnClickListener{ShowFilter()}
                textEmptySate.setText(R.string.emptystateDescFilter)
                imageState.setImageResource(R.drawable.ic_empty_state_filter)
                imageState.imageTintList = null
                buttonCrearEmpty.visibility = View.GONE
                if(reciclerview.adapter?.itemCount!! <= 1) {
                    ly_estate.visibility = View.VISIBLE
                    reciclerview.visibility = View.GONE
                    val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_out_in)
                    imageState.startAnimation(animation)
                } else {
                    ly_estate.visibility = View.GONE
                    reciclerview.visibility = View.VISIBLE
                }
            }
            else -> {

                        ly_estate.setOnClickListener{}
                        textEmptySate.setText(R.string.emptystatesolicitudesDesc)
                        imageState.setImageResource(R.drawable.ic_solicitudtrabajo)
                        buttonCrearEmpty.visibility = View.VISIBLE
                        imageState.imageTintList = ContextCompat.getColorStateList(this,R.color.colorPrimary)
                        if(reciclerview.adapter?.itemCount!! <= 1) {
                            ly_estate.visibility = View.VISIBLE
                            reciclerview.visibility = View.GONE
                            val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_out_in)
                            imageState.startAnimation(animation)
                        } else {
                            ly_estate.visibility = View.GONE
                            reciclerview.visibility = View.VISIBLE
                        }
            }
        }
    }


    override fun onContextItemSelected(item: MenuItem): Boolean
    {
        if(item.itemId == 1)
        {
            Toast.makeText(applicationContext,orden?.descripcion,Toast.LENGTH_SHORT).show()
        }
        return super.onContextItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                if (requestCode == REQUESTCODE.ACTIVIDAD_ADD_ORD) {

                    viewModelRecent = OrdenesViewModel()
                    getOrdenes()
                }
                if (requestCode == REQUESTCODE.ACTIVIDAD_DETAILS_ORD) {

                    viewModelRecent = OrdenesViewModel()
                    getOrdenes()
                }
            }
        }

    }


    override fun onClickOrdenListener(orden: Ordenes?)
    {
        this.orden = orden
        Toast.makeText(applicationContext,orden?.descripcion,Toast.LENGTH_SHORT).show()
    }

    override fun onLongOrdenListener(orden: Ordenes?)
    {
        this.orden = orden
    }


}

