package com.example.carlos.sgestman.model

import com.example.carlos.sgestman.model.ResponseOpcionPermisos
import com.example.carlos.sgestman.model.ResponseSdtresult
import com.google.gson.annotations.SerializedName

class ResponsePermisos
{

    @SerializedName("sdtresult")
    var sdtresult: ResponseSdtresult? = null

    var permisos:List<ResponseOpcionPermisos>? = null
}