package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.entity.OrdenReporteTiempo
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.repository.dao.OrdenReporteTiempoDao

class OrdenReporteTiempoRoomDBRepository(application: Application?) {
    private val ordenesReporteTiempoInfoDao: OrdenReporteTiempoDao?
    var allOrdenesReporte: LiveData<List<OrdenReporteTiempo>?>?

    private var _ordenReporteTiempo = MutableLiveData<OrdenReporteTiempo>()
    val ordenReporteTiempo : LiveData<OrdenReporteTiempo> get() = _ordenReporteTiempo




    init {
        val db = getDatabase(application!!)
        ordenesReporteTiempoInfoDao = db!!.ordenReporteTiempoInfoDao()
        allOrdenesReporte = ordenesReporteTiempoInfoDao!!.allOrdenReporteTiempo

    }

    fun getOrdenReportetiempo(numeroOT:String):LiveData<OrdenReporteTiempo>
    {
        return ordenesReporteTiempoInfoDao!!.ordenReporteTiempoByNumero(numeroOT)
    }
}