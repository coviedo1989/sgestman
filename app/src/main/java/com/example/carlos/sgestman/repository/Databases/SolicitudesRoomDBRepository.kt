package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.repository.dao.SolicitudesDao
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.entity.Solicitudes

class SolicitudesRoomDBRepository(application: Application?) {
    val SolicitudesInfoDao: SolicitudesDao?
    var allSolicitudesCount: LiveData<Solicitudes?>?

    fun insertSolicitudes(resultModel: Solicitudes?) {
        InsertAsyncTask(SolicitudesInfoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: SolicitudesDao?) : AsyncTask<Solicitudes?, Void?, Void?>() {
        override fun doInBackground(vararg globals: Solicitudes?): Void? {
            mAsyncTaskDao!!.insert(globals[0])
            return null
        }
    }

    init {
        val db = getDatabase(application!!)
        SolicitudesInfoDao = db!!.solicutdesInfoDao()
        allSolicitudesCount = SolicitudesInfoDao!!.allSolicitud
    }
}