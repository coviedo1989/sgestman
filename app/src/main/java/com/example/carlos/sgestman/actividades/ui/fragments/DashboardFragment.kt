package com.example.carlos.sgestman.actividades.ui.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.entity.CostosMtto
import com.example.carlos.sgestman.entity.DashBoard
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.entity.OrdenesCount
import com.example.carlos.sgestman.entity.SolicitudesCount
import com.example.carlos.sgestman.viewmodel.DashboardViewModel
import com.google.android.material.tabs.TabLayout
import com.pixplicity.easyprefs.library.Prefs

class DashboardFragment : Fragment(), OnRefreshListener {
    private var dashboardViewModel: DashboardViewModel? = null
    private var mFragmentAdapter: DasboardPagerAdapter? = null
    private var mViewPager: ViewPager? = null
    private var mTabLayout: TabLayout? = null
    private var mIndicator: View? = null
    private var indicatorWidth = 0
    private var swipeLayout: SwipeRefreshLayout? = null
    private var solicitudesCount: SolicitudesCount? = null
    private var ordenesCount: OrdenesCount? = null
    private var tab_position = 0
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dashboardViewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        SetupView(root)
        data
        //        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun SetupView(root: View) {
        tab_position = Prefs.getInt(PREFS.TAB_POSICION, 0)
        swipeLayout = root.findViewById(R.id.swipeLayout)
        mIndicator = root.findViewById(R.id.indicator)
        mTabLayout = root.findViewById(R.id.tab_layout)
        mViewPager = root.findViewById(R.id.viewpager)
        mFragmentAdapter = DasboardPagerAdapter(parentFragmentManager, DashBoard())

        mViewPager!!.adapter =  mFragmentAdapter
        // link the tabLayout and the viewpager together
        mTabLayout!!.setupWithViewPager(mViewPager)
        swipeLayout!!.setOnRefreshListener(this)
        Handler().postDelayed({
            mViewPager!!.setCurrentItem(tab_position, true)
            mTabLayout!!.setScrollPosition(tab_position, 0f, true)
        },
                50)
        mTabLayout!!.post(Runnable {
            indicatorWidth = mTabLayout!!.width / mTabLayout!!.tabCount

            //Assign new width
            val indicatorParams = mIndicator!!.layoutParams as FrameLayout.LayoutParams
            indicatorParams.width = indicatorWidth
            mIndicator!!.layoutParams= indicatorParams
        })
        mViewPager!!.addOnPageChangeListener(object : OnPageChangeListener {
            //To move the indicator as the user scroll, we will need the scroll offset values
            //positionOffset is a value from [0..1] which represents how far the page has been scrolled
            //see https://developer.android.com/reference/android/support/v4/view/ViewPager.OnPageChangeListener
            override fun onPageScrolled(i: Int, positionOffset: Float, positionOffsetPx: Int) {
                val params = mIndicator!!.layoutParams as FrameLayout.LayoutParams

                //Multiply positionOffset with indicatorWidth to get translation
                val translationOffset = (positionOffset + i) * indicatorWidth
                params.leftMargin = translationOffset.toInt()
                mIndicator!!.layoutParams = params
            }

            override fun onPageSelected(i: Int) {
                Prefs.putInt(PREFS.TAB_POSICION, i)
            }

            override fun onPageScrollStateChanged(i: Int) {}
        })
    }

    private val data: Unit
        get() {
            dashboardViewModel!!.sdtDashboard.observe(this, Observer {
                if (it!=null)
                {
                    mFragmentAdapter!!.dashBoard = it
                    mViewPager!!.adapter = mFragmentAdapter
                }
            })
            if (swipeLayout!!.isRefreshing) {
                swipeLayout!!.isRefreshing = false
            }
        }

    override fun onRefresh() {
        if (solicitudesCount != null) {
            dashboardViewModel!!.cargarViewModelAgain()
        }
        data
    }
}