package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.repository.dao.UserDao
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.entity.User

class UserRoomDBRepository(application: Application?) {
    private val userInfoDao: UserDao?
    var allUser: LiveData<User?>?

    fun insertGlobal(resultModel: User?) {
        InsertAsyncTask(userInfoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: UserDao?) : AsyncTask<User?, Void?, Void?>() {
        override fun doInBackground(vararg globals: User?): Void? {
            mAsyncTaskDao!!.insertGlobal(globals[0])
            return null
        }

    }

    init {
        val db = getDatabase(application!!)
        userInfoDao = db!!.userInfoDao()
        allUser = userInfoDao!!.allGlobal
    }
}