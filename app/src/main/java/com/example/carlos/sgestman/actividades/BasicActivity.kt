package com.example.carlos.sgestman.actividades

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.carlos.sgestman.model.ResponseSdtresult
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.BottomSheetMensaje
import com.example.carlos.sgestman.utils.views.CustomProgressDialog
import java.util.*

open class BasicActivity : AppCompatActivity()
{
    private var progressDialog: CustomProgressDialog? =null
    private var mensaje:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog= CustomProgressDialog(this)
    }

    protected fun showSimpleError(mensaje: String?, tipomensaje: TIPOMENSAJE?)
    {        val layoutInflater = layoutInflater
        BottomSheetMensaje.showMensaje(mensaje, tipomensaje, layoutInflater)
    }
    protected fun showErrorDenied(opcion: String?, action:String)
    {
        val layoutInflater = layoutInflater
        val mensaje = "No tiene permisos de ${action.toUpperCase(Locale.ROOT)} en ${opcion!!.toUpperCase(Locale.ROOT)}"
        BottomSheetMensaje.showMensaje(mensaje, TIPOMENSAJE.INFO, layoutInflater)
    }
    protected fun mostarTodosLosMensajes(sdtresult: ResponseSdtresult) {
        var mensaje:String?
        if (sdtresult.mensajes != null) {
            mensaje = " \n"
            for (i in sdtresult.mensajes!!.indices) {
                mensaje = """$mensaje${sdtresult.mensajes!![i].description}"""
            }
            showSimpleError(mensaje, TIPOMENSAJE.ERROR)
        }
    }

    fun showProgresBar()
    {
        if (mensaje==null)
        {
            mensaje = "Espere..."
        }
        if(progressDialog?.dialog?.isShowing!!)
        {
            progressDialog?.dimis()
        }
        else
        {
            progressDialog?.show("$mensaje")
        }
    }
}