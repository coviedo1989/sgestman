package com.example.carlos.sgestman.entity

import androidx.room.Embedded
import androidx.room.Relation
import java.io.Serializable

class OrdenReporteTiempo:Serializable
{
    @Embedded
    var ordenReporte: OrdenReporte ?=null


    @Relation(
            parentColumn = "idOrden",
            entityColumn = "idOrdenReporte"
            //entityColumn = "idReporte"
    )
    var listReporteTiempo: List<ReporteTiempo> = listOf()
}