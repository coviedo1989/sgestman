package com.example.carlos.sgestman.utils.enums

enum class EnumSolicitudes(var estate:Int)
{

        SINATENDER(1),
        ATENDIDA(2),
        INICIADA(3),
        PENDIENTE(4),
        TERMINADA(5),
        CERRADA(6),
        CANCELADA(7);

        companion object {
                fun fromInt(value: Int) = EnumSolicitudes.values().first { it.estate == value }
        }
}