package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.carlos.sgestman.entity.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(global: User?)

    @get:Query("SELECT * from Usuario")
    val allGlobal: LiveData<User?>?

    @Query("DELETE FROM Usuario")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGlobal(resultModel: User?)
}