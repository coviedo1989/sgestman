package com.example.carlos.sgestman.actividades.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.actividades.ui.ActividadAnalisis
import com.example.carlos.sgestman.actividades.ui.reportetrabajo.ActividadReporteTrabajo
import com.example.carlos.sgestman.actividades.ui.solicitud.ActividadSolicitudesTest
import com.example.carlos.sgestman.adapter.GridLayoutMenuAdapter
import com.example.carlos.sgestman.entity.Permisos
import com.example.carlos.sgestman.model.DataModelMenuInicio
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.utils.MiFecha
import com.example.carlos.sgestman.utils.constant.INTENT
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.BottomSheetMensaje.showMensaje
import com.example.carlos.sgestman.utils.views.CustomProgressDialog
import com.example.carlos.sgestman.viewmodel.HomeViewModel
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment(),GridLayoutMenuAdapter.ItemListener
{
    private lateinit var progressDialog: CustomProgressDialog
    private var arrayList= arrayListOf<DataModelMenuInicio>()
    private var arrayListAux= arrayListOf<DataModelMenuInicio>()
    private lateinit var listPermisos: ArrayList<Permisos>
    private var isTieneOpcionesMenu: Boolean = false
    private var emplID: Int? = null
    private var homeViewModel: HomeViewModel? = null
    private var txtEmpresa: TextView? = null
    private val isConnection = false
    private var relativeLayout: View? = null
    private lateinit var recyclerView:RecyclerView
    private lateinit var lyMenu:LinearLayout
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        homeViewModel = ViewModelProvider(requireActivity())[HomeViewModel::class.java]
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        setupView(root)
        gedData()
        return root
    }

    private fun setupView(root: View)
    {
        progressDialog= CustomProgressDialog(requireContext())
        val usuario = Prefs.getString(PREFS.USUARIO, "")
        val welcomStr = String.format(getString(R.string.welcome_text), usuario.toUpperCase())
        val tvWelcomeUser = root.findViewById<TextView>(R.id.txtwelcome)
        txtEmpresa = root.findViewById(R.id.txt_empresa)
        val txt_fecha = root.findViewById<TextView>(R.id.txtfecha)
        val date = Calendar.getInstance().time
        @SuppressLint("SimpleDateFormat") val df = MiFecha().simpleDateFormatHome
        val formattedDate = df.format(date)
        txt_fecha.text = formattedDate
        tvWelcomeUser.text = Html.fromHtml(welcomStr)
        txtEmpresa!!.text= getString(R.string.empresa)
        relativeLayout = root.findViewById(R.id.ry_progres)


        setupMenuApp(root)
    }

    private fun setupMenuApp(root: View)
    {
        lyMenu  = root.findViewById(R.id.lyMenu)
        recyclerView = root.findViewById<RecyclerView>(R.id.recyclerView)

        addDataMenuInicio()
//        arrayList.add(DataModel("Análisis","Análisis", R.drawable.ic_solicitudtrabajo, "#673BB7"))
//        arrayList.add(DataModel("Análisis","Análisis", R.drawable.ic_solicitudtrabajo, "#673BB7"))

        val adapter = GridLayoutMenuAdapter(requireContext(), arrayList, this)
        recyclerView.adapter = adapter

        /**
        AutoFitGridLayoutManager that auto fits the cells by the column width defined.
         **/

        /*AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(this, 500);
        recyclerView.setLayoutManager(layoutManager);*/


        /**
        Simple GridLayoutManager that spans two columns
         **/
        /**
         * AutoFitGridLayoutManager that auto fits the cells by the column width defined.
         */

        /*AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(this, 500);
        recyclerView.setLayoutManager(layoutManager);*/
        /**
         * Simple GridLayoutManager that spans two columns
         */
        val manager = GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = manager


    }

    private fun addDataMenuInicio()
    {
        arrayList.clear()
        arrayList.add(DataModelMenuInicio("Solicitud","Solicitud", R.drawable.ic_solicitudtrabajo, "#09A9FF"))
        arrayList.add(DataModelMenuInicio("Reporte Trabajo","ReporteTrabajo", R.drawable.ic_reportetrabajo, "#3E51B1"))
        arrayList.add(DataModelMenuInicio("Análisis","Análisis", R.drawable.ic_graph, "#673BB7"))
    }

    private fun gedData() {
        homeViewModel!!.getmAllUser()?.observe(viewLifecycleOwner, Observer { s ->
            if (s != null)
            {
                txtEmpresa!!.text = String.format(s.empresa!!)
                emplID = s.emplId
                Prefs.putInt(PREFS.USUARIO_EMPLID,if(emplID!=null) emplID!! else 0)
            }
        })
        homeViewModel!!.getAllPermiso?.observe(viewLifecycleOwner, Observer { list->
            if(list!=null)
            {
                //permisos para mostrar el home en dependecia de los que tenga el usuario
                permisosHome(list)
            }
        })
    }

    private fun permisosHome(list: List<Permisos>)
    {
        addDataMenuInicio()
        listPermisos = list as ArrayList<Permisos>
        listPermisos.forEach {permiso->
            if (permiso.acionden==1)
            {
                arrayList.removeIf { it.titledb == permiso.opcion}
            }
            else
            {
                isTieneOpcionesMenu = true
            }
        }
        recyclerView.adapter?.notifyDataSetChanged()

        if(!isTieneOpcionesMenu)
        {
            lyMenu.visibility = View.GONE
            ly_mensaje_noOpcion.visibility = View.VISIBLE
        }
        else
        {
            lyMenu.visibility = View.VISIBLE
            ly_mensaje_noOpcion.visibility = View.GONE
        }
    }

    private fun damePermiso(opcion: String?): Permisos? {
        var permisos= Permisos()
        listPermisos.forEach {
            if (it.opcion.equals(opcion))
            permisos = it
        }
        return permisos
    }


    fun showError(mensaje: String?, tipomensaje: TIPOMENSAJE?) {
        val layoutInflater = layoutInflater
        showMensaje(mensaje, tipomensaje, layoutInflater)
    }

    suspend fun testConection(v: View) {
        val call1 = RetrofitClient.instance?.api?.TestConexion()
        call1?.enqueue(object : Callback<String?> {
            override fun onResponse(call: Call<String?>, response: Response<String?>) {
                if (response.isSuccessful) {
                } else {
                    showError(response.message(), TIPOMENSAJE.ERROR)
                }
                showProgresBar()
            }

            override fun onFailure(call: Call<String?>, t: Throwable) {
                showProgresBar()
                showError(t.message, TIPOMENSAJE.ERROR)
            }
        })
    }

   fun showProgresBar()
   {
       if(progressDialog.dialog.isShowing)
       {
           progressDialog.dimis()
       }
       else
       {
           progressDialog.show("")
       }
   }

    override fun onItemClick(item: DataModelMenuInicio?) {
        if (item?.title.equals("Solicitud"))
        {
            val intent = Intent(requireContext(), ActividadSolicitudesTest::class.java)
            intent.putExtra(INTENT.EXTRA_PERMISOS,damePermiso(item?.titledb))
            startActivity(intent)
        }
        if (item?.title.equals("Reporte Trabajo"))
        {
            val intent = Intent(requireContext(), ActividadReporteTrabajo::class.java)
            intent.putExtra(INTENT.EXTRA_PERMISOS,damePermiso(item?.titledb))
            startActivity(intent)
        }
        if (item?.title.equals("Análisis"))
        {
            val intent = Intent(requireContext(), ActividadAnalisis::class.java)
            startActivity(intent)
        }
    }
}