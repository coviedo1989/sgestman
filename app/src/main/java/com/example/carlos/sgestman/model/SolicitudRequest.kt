package com.example.carlos.sgestman.model

import com.example.carlos.sgestman.entity.Solicitudes
import com.google.gson.annotations.SerializedName

class SolicitudRequest
{
    var usuario:String? = null

    @SerializedName("Solicitud")
    var solicitud: Solicitudes? = null

    var modeAction: MODEACTION? = null

    enum class MODEACTION
    {
        INS,UPD,DEL
    }
}