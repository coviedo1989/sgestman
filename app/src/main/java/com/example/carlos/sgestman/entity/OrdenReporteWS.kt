package com.example.carlos.sgestman.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

///interfaz para las orden reporte del servidor

@Entity(tableName = "OrdenReporteWS")
class OrdenReporteWS:Serializable
{
    @PrimaryKey(autoGenerate = true)
    var idOrden:Int? = null
    var numeroOT:String?= null

}