package com.example.carlos.sgestman

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.example.carlos.sgestman.actividades.ActivityPrincipal
import com.example.carlos.sgestman.actividades.IniciarSession
import com.example.carlos.sgestman.actividades.MainActivity
import com.example.carlos.sgestman.utils.constant.PREFS
import com.pixplicity.easyprefs.library.Prefs
import java.util.*

class ActividadSplashScren : AppCompatActivity() {
    private var inicio: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      //  requestWindowFeature(Window.FEATURE_NO_TITLE)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_actividad__splash_scren)

        setupView()
        TimerSplash()
    }

    private fun setupView() {
        inicio = Prefs.getBoolean(PREFS.OP_INICIAR_SESION_AUTOMATICA,false)
        val isNightMode = Prefs.getBoolean(PREFS.OP_MODE_NIGHT, false)

        if (isNightMode)
        {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        else
        {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }


    fun TimerSplash() {
        val task: TimerTask = object : TimerTask() {
            override fun run()
            {
                if (!inicio) //if(Prefs.getString(PREFS.USUARIO,"").isEmpty())
                {
                    //val intent = Intent(applicationContext, TestActivity::class.java)
                    val intent = Intent(applicationContext, IniciarSession::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(applicationContext, ActivityPrincipal::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
        // Simulate a long loading process on application startup.
        val timer = Timer()
        timer.schedule(task, SPLASH_SCREEN_DELAY)
    }

    companion object {
        private const val SPLASH_SCREEN_DELAY: Long = 1000
    }
}