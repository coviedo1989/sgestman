package com.example.carlos.sgestman.utils.views

import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.google.android.material.bottomsheet.BottomSheetDialog

object BottomSheetMensaje {
    @JvmStatic
    fun showMensaje(Mensaje: String?, tipoMensaje: TIPOMENSAJE?, layoutInflater: LayoutInflater) {
        val mBottomSheetDialog = BottomSheetDialog(layoutInflater.context, R.style.BaseBottomSheetDialog_BottomSheetDialogThemeRound)
        val sheetView = layoutInflater.inflate(R.layout.view_bottonsheet_error, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.setCancelable(false)
//        mBottomSheetDialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//        mBottomSheetDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val txtError = sheetView.findViewById<TextView>(R.id.txtTexto)
        val txtTitle = sheetView.findViewById<TextView>(R.id.txt_title)
        val button = sheetView.findViewById<Button>(R.id.button)
        val img = sheetView.findViewById<ImageView>(R.id.image)
        when (tipoMensaje) {
            TIPOMENSAJE.ERROR -> {
                img.setImageResource(R.drawable.ic_error)
                txtTitle.setText(R.string.error)
            }
            TIPOMENSAJE.INFO -> {
                img.setImageResource(R.drawable.ic_info)
                txtTitle.setText(R.string.info)
            }
            TIPOMENSAJE.SUCCES -> {
                img.setImageResource(R.drawable.ic_info)
                txtTitle.setText(R.string.succes)
            }
        }
        txtError.text = Mensaje
        button.setOnClickListener { mBottomSheetDialog.dismiss() }
        mBottomSheetDialog.show()
    }
}