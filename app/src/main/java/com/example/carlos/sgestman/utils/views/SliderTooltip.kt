package com.example.carlos.sgestman.utils.views

import android.app.ActionBar
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.marginBottom
import com.db.williamchart.Tooltip
import com.example.carlos.sgestman.R


class SliderTooltip : Tooltip {

    private lateinit var tooltipView: View
    private lateinit var texview:TextView

    var color = Color.BLACK
    var textSize = 16f

    var context : Context? = null
    //var text

    override fun onCreateTooltip(parentView: ViewGroup) {
        tooltipView =
            LayoutInflater.from(parentView.context)
                .inflate(R.layout.tooltip, parentView, false)
        tooltipView.setBackgroundColor(color)
        texview = tooltipView.findViewById(R.id.value)
        texview.textSize = textSize

        tooltipView.visibility = View.INVISIBLE
        parentView.addView(tooltipView)
    }

    override fun onDataPointTouch(x: Float, y: Float)
    {
        tooltipView.visibility = View.VISIBLE
        tooltipView.x = x - 40f - tooltipView.width / 2
        tooltipView.y = y - 40f - tooltipView.height / 2

        val animacion: Animation = AnimationUtils.loadAnimation(context, R.anim.anim)
        texview.startAnimation(animacion)
        //Toast.makeText(context, "$x $y", Toast.LENGTH_SHORT).show()
       // texview.text = y.toString()
    }

    override fun onDataPointClick(x: Float, y: Float) {}

    fun Texto (texto: String)
    {
        texview.text = texto
    }
}
