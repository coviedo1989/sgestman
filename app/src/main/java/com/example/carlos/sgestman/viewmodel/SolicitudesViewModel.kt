package com.example.carlos.sgestman.viewmodel

import androidx.lifecycle.*
import androidx.paging.*
import com.example.carlos.sgestman.entity.Permisos
import com.example.carlos.sgestman.entity.Solicitudes
import com.example.carlos.sgestman.repository.Databases.PermisosRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.SolicitudesDataSource
import com.example.carlos.sgestman.utils.constant.PREFS
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.coroutines.flow.map

class SolicitudesViewModel(): ViewModel()
{
    var codigo = Prefs.getString(PREFS.FILTER_SOL_CODIGO,"")
    var cliente = Prefs.getString(PREFS.FILTER_SOL_CLIENTE,"")
    var ejecutor = Prefs.getString(PREFS.FILTER_SOL_EJECUTOR,"")
    var objeto = Prefs.getString(PREFS.FILTER_SOL_OBJETO,"")
    var estado = Prefs.getInt(PREFS.FILTER_SOL_ESTADO,0)
    var fechaIncio = Prefs.getString(PREFS.FILTER_SOL_FECHAINICIO,"")
    var fechaFin = Prefs.getString(PREFS.FILTER_SOL_FECHAFIN,"")
    var itemsSolicitudes = Pager(
            // Configure how data is loaded by passing additional properties to
            // PagingConfig, such as prefetchDistance.
            PagingConfig(
                    pageSize = 20,
                    enablePlaceholders = true)
    )
    {
        SolicitudesDataSource(codigo,cliente,ejecutor,objeto,estado,fechaIncio,fechaFin)
    }.flow
           .cachedIn(viewModelScope)



}