package com.example.carlos.sgestman.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log

/**
 * Created by carlos on 19/03/2018.
 */
object PermisionApp {
    fun checkCamera(context: Context): Boolean {
        val permissionState = context.checkSelfPermission(Manifest.permission.CAMERA)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    fun checkwriteExternalStoragePermission(context: Context): Boolean {
        Log.w("PermissionChecker", "check WRITE_EXTERNAL_STORAGE")
        val permissionState = context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return permissionState == PackageManager.PERMISSION_GRANTED

    }

    fun checkreadExternalStoragePermission(context: Context): Boolean {
        Log.w("PermissionChecker", "check Read_EXTERNAL_STORAGE")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionState = context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            return permissionState == PackageManager.PERMISSION_GRANTED
        }
        return true
    }

    fun checkPhoneStatePermission(context: Context): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionState = context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
            return permissionState == PackageManager.PERMISSION_GRANTED
        }
        return true
    }

    fun checkAceesWifiStatePermission(context: Context): Boolean {
        Log.w("PermissionChecker", "check WRITE_EXTERNAL_STORAGE")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionState = context.checkSelfPermission(Manifest.permission.ACCESS_WIFI_STATE)
            return permissionState == PackageManager.PERMISSION_GRANTED
        }
        return true
    }

    fun checkInternetPermission(context: Context): Boolean {
        Log.w("PermissionChecker", "check WRITE_EXTERNAL_STORAGE")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionState = context.checkSelfPermission(Manifest.permission.INTERNET)
            return permissionState == PackageManager.PERMISSION_GRANTED
        }
        return true
    }

    fun checkPhoneCall(context: Context): Boolean {
        Log.w("PermissionChecker", "check WRITE_EXTERNAL_STORAGE")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionState = context.checkSelfPermission(Manifest.permission.CALL_PHONE)
            return permissionState == PackageManager.PERMISSION_GRANTED
        }
        return true
    }

    fun checkSystemAlert(context: Context): Boolean {
        Log.w("PermissionChecker", "check SYSTEM ALERT")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionState = context.checkSelfPermission(Manifest.permission.SYSTEM_ALERT_WINDOW)
            return permissionState == PackageManager.PERMISSION_GRANTED
        }
        return true
    }
}