package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.repository.dao.OrdenReporteDao

class OrdenReporteRoomDBRepository(application: Application?) {
    private val ordenesReporteInfoDao: OrdenReporteDao?
    var allOrdenesReporte: LiveData<List<OrdenReporte>?>?

    fun insertOrdenesReporte(resultModel: OrdenReporte?) {
        InsertAsyncTask(ordenesReporteInfoDao).execute(resultModel)
    }
    fun deleteOrdenesReporte(resultModel: OrdenReporte?) {
        DeleteAsyncTask(ordenesReporteInfoDao).execute(resultModel)
    }

    fun updateOrdenesReporte(resultModel: OrdenReporte?) {
        UpdateAsyncTask(ordenesReporteInfoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: OrdenReporteDao?) : AsyncTask<OrdenReporte?, Void?, Void?>() {
        override fun doInBackground(vararg params: OrdenReporte?): Void?
        {
            if(mAsyncTaskDao?.selectByNumeroOT(params[0]!!.numeroOT!!)?.count()==0)
            {
                mAsyncTaskDao.insert(params[0])
            }
            else
            {
                mAsyncTaskDao!!.updateEstado(params[0]!!.numeroOT!!, params[0]!!.estadoOT!!)
            }

            return null
        }
    }
    private class DeleteAsyncTask internal constructor(private val mAsyncTaskDao: OrdenReporteDao?):AsyncTask<OrdenReporte?,Void?,Void?>(){
        override fun doInBackground(vararg params: OrdenReporte?): Void? {
            mAsyncTaskDao!!.delete(params[0])
            return null
        }
    }
    private class UpdateAsyncTask internal constructor(private val mAsyncTaskDao: OrdenReporteDao?) : AsyncTask<OrdenReporte?, Void?, Void?>() {
        override fun doInBackground(vararg params: OrdenReporte?): Void? {
            mAsyncTaskDao!!.update(params[0])
            return null
        }
    }

    init {
        val db = getDatabase(application!!)
        ordenesReporteInfoDao = db!!.ordenReporteInfoDao()
        allOrdenesReporte = ordenesReporteInfoDao!!.allOrdenReporte
    }
}