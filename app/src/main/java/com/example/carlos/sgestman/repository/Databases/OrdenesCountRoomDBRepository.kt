package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.repository.dao.OrdenesCountDao
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.entity.OrdenesCount

class OrdenesCountRoomDBRepository(application: Application?) {
    private val OrdenesCountInfoDao: OrdenesCountDao?
    var allOrdenesCount: LiveData<OrdenesCount?>?

    fun insertOrdenesCount(resultModel: OrdenesCount?) {
        InsertAsyncTask(OrdenesCountInfoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: OrdenesCountDao?) : AsyncTask<OrdenesCount?, Void?, Void?>() {
        override fun doInBackground(vararg params: OrdenesCount?): Void? {
            mAsyncTaskDao!!.insert(params[0])
            return null
        }
    }

    init {
        val db = getDatabase(application!!)
        OrdenesCountInfoDao = db!!.ordenesCountInfoDao()
        allOrdenesCount = OrdenesCountInfoDao!!.allOrdenesCount
    }
}