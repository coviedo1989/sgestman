package com.example.carlos.sgestman.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "permisos")
class Permisos:Serializable
{

    @Ignore
    var modeModulos: MODEOPCIONAPP? = null

    @PrimaryKey
    @NonNull
    var opcion:String? =null

    @SerializedName("Add")
    var add:Int? = null

    @SerializedName("Up")
    var up:Int? = null

    @SerializedName("Del")
    var del:Int? = null

    @SerializedName("Ver")
    var ver:Int? = null

    @SerializedName("Imp")
    var imp:Int? = null

    @SerializedName("Exec")
    var exec:Int? = null

    @SerializedName("AccDen")
    var acionden:Int? = null


    enum class MODEOPCIONAPP
    {
        SOLICITUD,REPORTETRAB,ANALISIS
    }
}