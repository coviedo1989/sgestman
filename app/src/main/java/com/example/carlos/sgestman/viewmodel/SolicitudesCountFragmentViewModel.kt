package com.example.carlos.sgestman.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.carlos.sgestman.entity.SolicitudesCount
import com.example.carlos.sgestman.repository.Databases.SolicitudesCountRoomDBRepository
import com.example.carlos.sgestman.repository.ws.WebServiceRepository

class SolicitudesCountFragmentViewModel(application: Application) : AndroidViewModel(application)
{
    private val solicitudesCountRoomDBRepository:SolicitudesCountRoomDBRepository = SolicitudesCountRoomDBRepository(application)
    private val webServiceRepository:WebServiceRepository= WebServiceRepository(application)

    private var _solicitudesCount = MutableLiveData<SolicitudesCount>()
    val solicitudesCount : LiveData<SolicitudesCount?>?

    init {
        _solicitudesCount = webServiceRepository.providesDashSolicitudesCountWebService()
        solicitudesCount = solicitudesCountRoomDBRepository.allSolicitudesCount
    }
}