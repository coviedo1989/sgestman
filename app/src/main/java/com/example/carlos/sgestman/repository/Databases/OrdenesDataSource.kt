package com.example.carlos.sgestman.repository.Databases

import androidx.paging.PagingSource
import com.example.carlos.sgestman.entity.Ordenes
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import retrofit2.HttpException

class OrdenesDataSource(
        var codigo: String,
        var numero:String,
        var cliente: String,
        var ejecutor: String,
        var objeto:String,
        var estado:Int,
        var fechaIncio:String,
        var fechaFin:String) : PagingSource<Int, Ordenes>()
{
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Ordenes>
    {
        return try
        {
            val nextPageNumber = params.key ?: 1
            val response = RetrofitClient.
                                            instance?.api?.
            getDataOrdenesAll(nextPageNumber, 20,codigo,numero,cliente,ejecutor,objeto,estado,fechaIncio,fechaFin)
            val isFinish = response?.size==0

            LoadResult.Page(
                    data = response!!,
                    prevKey = null, // Only paging forward.
                    nextKey = if (isFinish) null else nextPageNumber.plus(1))

        }
        catch (e: Exception)
        {
            LoadResult.Error(e)
        }
        catch (e: HttpException) {
            // HttpException for any non-2xx HTTP status codes.
            LoadResult.Error(e)
        }

    }
}