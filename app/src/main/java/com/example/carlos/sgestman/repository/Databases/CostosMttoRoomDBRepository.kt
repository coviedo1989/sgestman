package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.entity.CostosMtto
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.entity.SolicitudesCount
import com.example.carlos.sgestman.repository.dao.CostosMttoDao

class CostosMttoRoomDBRepository(application: Application?) {
    private val costosMttoDao: CostosMttoDao?
    var allcostosMtto: LiveData<CostosMtto?>?

    fun insertCostosMtto(resultModel: CostosMtto?) {
        InsertAsyncTask(costosMttoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: CostosMttoDao?) : AsyncTask<CostosMtto?, Void?, Void?>() {
        override fun doInBackground(vararg globals: CostosMtto?): Void? {
            mAsyncTaskDao!!.insert(globals[0])
            return null
        }
    }

    init {
        val db = getDatabase(application!!)
        costosMttoDao = db!!.costosMttoInfoDao()
        allcostosMtto = costosMttoDao!!.allcostosMtto
    }
}