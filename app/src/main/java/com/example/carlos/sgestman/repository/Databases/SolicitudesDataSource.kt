package com.example.carlos.sgestman.repository.Databases

import androidx.paging.PagingSource
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.entity.Solicitudes
import retrofit2.HttpException

class SolicitudesDataSource(
        var codigo: String,
        var cliente: String,
        var ejecutor: String,
        var objeto:String,
        var estado:Int,
        var fechaIncio:String,
        var fechaFin:String) : PagingSource<Int, Solicitudes>()
{
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Solicitudes>
    {
        return try
        {
            val nextPageNumber = params.key ?: 1
            val response = RetrofitClient.
                                            instance?.api?.
                                            getDataSolicitudesAll(nextPageNumber, 20,codigo,cliente,ejecutor,objeto,estado,fechaIncio,fechaFin)
            val isFinish = response?.size==0

            LoadResult.Page(
                    data = response!!,
                    prevKey = null, // Only paging forward.
                    nextKey = if (isFinish) null else nextPageNumber.plus(1))

        }
        catch (e: Exception)
        {
            LoadResult.Error(e)
        }
        catch (e: HttpException) {
            // HttpException for any non-2xx HTTP status codes.
            LoadResult.Error(e)
        }

    }
    init
    {
        Filtros()
    }

    fun Filtros()
    {

    }
}