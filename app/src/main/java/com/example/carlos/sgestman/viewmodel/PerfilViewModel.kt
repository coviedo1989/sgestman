package com.example.carlos.sgestman.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.carlos.sgestman.entity.User
import com.example.carlos.sgestman.repository.Databases.UserRoomDBRepository
import com.example.carlos.sgestman.repository.ws.WebServiceRepository
import com.example.carlos.sgestman.utils.Aplication

class PerfilViewModel(application: Application) : AndroidViewModel(application)
{
    private val webServiceRepository: WebServiceRepository = WebServiceRepository(getApplication())
    private val roomDBRepository: UserRoomDBRepository = UserRoomDBRepository(application)
    private val mAllUser: LiveData<User?>?
    private val obserUser: LiveData<User>

    fun getmAllUser(): LiveData<User?>? {
        return mAllUser
    }


    init {
        obserUser = webServiceRepository.providesUserWebService()
        mAllUser = roomDBRepository.allUser
    }
}