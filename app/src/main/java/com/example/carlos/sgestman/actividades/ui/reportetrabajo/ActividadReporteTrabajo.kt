package com.example.carlos.sgestman.actividades.ui.reportetrabajo

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.actividades.BasicActivity
import com.example.carlos.sgestman.adapter.*
import com.example.carlos.sgestman.entity.*
import com.example.carlos.sgestman.model.OrdenTrabPrevistoRequest
import com.example.carlos.sgestman.model.ResponseWS
import com.example.carlos.sgestman.repository.Databases.OrdenReporteRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.OrdenReporteWSRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.ReporteTiempoRoomDBRepository
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.utils.MiFecha
import com.example.carlos.sgestman.utils.constant.INTENT
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.constant.REQUESTCODE
import com.example.carlos.sgestman.utils.enums.EnumOrdenes
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.CustomProgressDialog
import com.example.carlos.sgestman.viewmodel.OrdenReporteViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo.buttonCrearEmpty
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo.floatingActionButton
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo.imageState
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo.ly_estate
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo.reciclerview
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo.swipeRefreshLayout
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo.textEmptySate
import kotlinx.android.synthetic.main.activity_solicitudes.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class ActividadReporteTrabajo : BasicActivity(), AdapterReporteTrabajoListener,ItemPlayPauseListener1
{
    private lateinit var permisoReporTrab: Permisos
    private var isbyFilter: Boolean = false
    private var detailsPostion: Int = 0
    private lateinit var reporteTrabajoAdapter: ReporteTrabajoAdapter
    private lateinit var viewmodel: OrdenReporteViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var ordenReporteRoomDBRepository : OrdenReporteRoomDBRepository
    private lateinit var ordenReporteWSRoomDBRepository : OrdenReporteWSRoomDBRepository
    private lateinit var reporteTiempoRoomDBRepository: ReporteTiempoRoomDBRepository
    private var listOrdenesAdd = ArrayList<Ordenes>()
    private var listOrdenReporte = ArrayList<OrdenReporteTiempo>()
    private var estadoInt = 0
    private var estadoOrdenReporte = 0
    private lateinit var customAdapterExpande:CustomAdapter

    private var progressDialog: CustomProgressDialog? =null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actividad_reporte_trabajo)
        setupView()
    }



    private fun setupView()
    {
        setupVariables()
        setupVista()
        setTollbar()
        //checkEmpyState()
        setupViewModel()
    }

    private fun setupVista() {
        reciclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) // Puedes ocultarlo simplemente
                //fab.hide();
                // o añadir la animación deseada
                    floatingActionButton.animate().translationY(floatingActionButton.height +
                            resources.getDimension(R.dimen.fab_margin))
                            .setInterpolator(LinearInterpolator()).duration = 100
                else if (dy < 0) //fab.show();
                    floatingActionButton.animate().translationY(0F)
                            .setInterpolator(LinearInterpolator()).duration = 100
            }
        })
        floatingActionButton.setOnClickListener { adicionarOrdenTrabajo() }
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary)
        swipeRefreshLayout.setOnRefreshListener {
            viewmodel = OrdenReporteViewModel(application)
             setupViewModel()
        }
        floatingActionButton.visibility = if (permisoReporTrab.add==0) View.VISIBLE else View.GONE


    }

    private fun setupViewModel()
    {
        viewmodel = ViewModelProvider(this).get(OrdenReporteViewModel::class.java)
        getOrdenReporte()
    }

    private fun getOrdenReporte()
    {
        viewmodel.allOrdenReporteTiempo.observe(this, Observer
        {
            showMensaje(it)
        })
        viewmodel.allOrdenReporteWS.observe(this, Observer {
            actualizarOrdenReporteLocal(it)
        })
    }

    private fun actualizarOrdenReporteLocal(it: List<OrdenReporteWS>?) {
        listOrdenReporte.forEach { ordenReporteTiempo->

            if (it!=null)
            {
                if (!it.any { ordenReporte-> ordenReporte.numeroOT == ordenReporteTiempo.ordenReporte!!.numeroOT }) {
                    ordenReporteRoomDBRepository.deleteOrdenesReporte(ordenReporteTiempo.ordenReporte)
                }
            }
        }
    }

    private fun showMensaje(it: List<OrdenReporteTiempo>?)
    {
        if(it?.count() != listOrdenReporte.count())
        {
//            it?.sortedByDescending { it.activada }
//            listOrdenReporte.sortedByDescending { it.activada }
            filtrosOrdenar(it)
            reporteTrabajoAdapter = ReporteTrabajoAdapter(listOrdenReporte,this,this)

            //reporteTrabajoAdapter.setCount(listOrdenReporte.size)
            reciclerview.adapter = reporteTrabajoAdapter
            reciclerview.layoutManager = (LinearLayoutManager(this))
            checkEmpyState()

//            customAdapterExpande = CustomAdapter(this,listOrdenReporte,this)
//            expandelistView.setAdapter(customAdapterExpande)

            //expandelistView.setOnGroupClickListener(ExpandableListView.OnGroupClickListener { parent, v, groupPosition, id -> false })
        }
        else
        {
            if (it.count() != 0)
            {
                filtrosOrdenar(it)
                reciclerview.adapter?.notifyDataSetChanged()
                checkEmpyState()
            }
        }
        if (listOrdenReporte.size==0)
        {
            checkEmpyState()
        }
        if(swipeRefreshLayout.isRefreshing)
        {
            swipeRefreshLayout.isRefreshing = false
        }

    }

    private fun filtrosOrdenar(it: List<OrdenReporteTiempo>?)
    {
        val numeroOT = Prefs.getString(PREFS.FILTER_ORD_REPORTE_NUMERO,"")
        val estado = Prefs.getInt(PREFS.FILTER_ORD_REPORTE_ESTADO,0)
        val estadoReporte = Prefs.getInt(PREFS.FILTER_ORD_REPORTE_ITEMS_ESTADO,0)
        val fechaInicio = Prefs.getString(PREFS.FILTER_ORD_REPORTE_FECHAINICIO,"")
        val fechaFin = Prefs.getString(PREFS.FILTER_ORD_REPORTE_FECHAFIN,"")

        val srtEstado = if (estado==0) "" else estado.toString()
        var srtEstadoReportePlay=""
        var srtEstadoReportePause= ""
        if (estadoReporte==1)
        {
            srtEstadoReportePlay = true.toString()
        }
        if (estadoReporte==2)
        {
            srtEstadoReportePause = true.toString()
        }

       listOrdenReporte = it!!.filter {
            it.ordenReporte!!.numeroOT!!.contains(numeroOT) &&
            it.ordenReporte!!.estadoOT.toString().contains(srtEstado) &&
            it.ordenReporte!!.isplay!!.toString().contains(srtEstadoReportePlay) &&
            it.ordenReporte!!.ispasuse!!.toString().contains(srtEstadoReportePause)
        } as ArrayList<OrdenReporteTiempo>

//        listOrdenReporte.sortByDescending { it.ordenReporte!!.ispasuse }
//        listOrdenReporte.sortByDescending { it.ordenReporte!!.isplay }

        listOrdenReporte.sortWith(compareBy ({ it.ordenReporte?.ispasuse },{it.ordenReporte?.isplay}))

    }

    private fun checkEmpyState()
    {
        isbyFilter = Prefs.getBoolean(PREFS.FILTER_ORD_REPORTE_ISFILTER,false)

        if(reciclerview.adapter?.itemCount==0 ||  reciclerview.adapter?.itemCount == null)
        {
            if (isbyFilter)
            {
                ly_estate.setOnClickListener{showFilter()}
                textTitle.setText(R.string.emptystateFiltersReporteTrabajo)
                textEmptySate.setText(R.string.emptystateDescFilter)
                imageState.setImageResource(R.drawable.ic_empty_state_filter)
                imageState.imageTintList = null
                buttonCrearEmpty.visibility = View.GONE

            }
            else
            {
                ly_estate.setOnClickListener{}
                textTitle.setText(R.string.emptystateReporteTrabajo)
                textEmptySate.setText(R.string.emptystateReporteTrabajoDesc)
                imageState.setImageResource(R.drawable.ic_solicitudtrabajo)
                buttonCrearEmpty.visibility = View.VISIBLE
                imageState.imageTintList = ContextCompat.getColorStateList(this,R.color.colorPrimary)
            }
            ly_estate.visibility = View.VISIBLE
            reciclerview.visibility = View.GONE
            floatingActionButton.visibility = View.GONE
            val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_out_in)
            imageState.startAnimation(animation)
        }
        else
        {
            floatingActionButton.visibility = View.VISIBLE
            reciclerview.visibility = View.VISIBLE
            ly_estate.visibility = View.GONE
        }
    }

    private fun setupVariables()
    {
        isbyFilter = Prefs.getBoolean(PREFS.FILTER_ORD_REPORTE_ISFILTER,false)
        ordenReporteRoomDBRepository = OrdenReporteRoomDBRepository(application)
        reporteTiempoRoomDBRepository = ReporteTiempoRoomDBRepository(application)
        ordenReporteWSRoomDBRepository = OrdenReporteWSRoomDBRepository(application)
        progressDialog = CustomProgressDialog(this)
        permisoReporTrab = intent.extras?.get(INTENT.EXTRA_PERMISOS) as Permisos
    }
    @SuppressLint("SetTextI18n")
    fun setTollbar()
    {
        toolbar = findViewById<View>(R.id.my_awesome_toolbar) as Toolbar
        toolbar.title = ""
        toolbar.navigationIcon= ContextCompat.getDrawable(this,R.drawable.ic_left_arrow)
        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        val mtitleToolbar = toolbar.findViewById(R.id.toolbar_title) as TextView
        mtitleToolbar.text = "Reporte Trabajo"
    }
    private fun adicionarOrdenTrabajo() {
        val intent = Intent(this, ActividadSelectOrdenReporte::class.java)
        startActivityForResult(intent,REQUESTCODE.ACTIVIDAD_REPORTE_SELECT_OT)
    }
    fun btnAddOrden(view: View)
    {
        adicionarOrdenTrabajo()
    }

    //////////////////////////////OVERRRIDE//////////////////////
    ///////////////////////////////////////////////////////////
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_reporte_trabajo, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.menu_filter ->
            {
                showFilter()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK ->
            {
                if (requestCode == REQUESTCODE.ACTIVIDAD_REPORTE_SELECT_OT)
                {
                    listOrdenesAdd = (data!!.getParcelableArrayListExtra("ORDENESSELECT"))!!
                    enviarPeticion()
                }
                if(requestCode == REQUESTCODE.ACTIVIDAD_REPORTE_TIEMPO_DETAILS)
                {
                    val isElimino = data!!.getBooleanExtra(INTENT.EXTRA_ISELIMINO,false)
                    val isEdited = data.getBooleanExtra(INTENT.EXTRA_ISEDITED,false)
                    when {
                        isElimino -> {
                            actualizarEliminado()
                        }
                    }
                    when {
                        isEdited -> {
                            val ordenReporteTiempo = data.getSerializableExtra(INTENT.EXTRA_ORDEN_REPORTE_TIEMPO) as OrdenReporteTiempo
                            actualizarOrdenReporte(ordenReporteTiempo)
                        }
                    }
                }
            }
        }

    }

    private fun actualizarOrdenReporte(ordenReporteTiempo: OrdenReporteTiempo)
    {
        listOrdenReporte[detailsPostion] = ordenReporteTiempo
        reporteTrabajoAdapter.mFeedList[detailsPostion] = ordenReporteTiempo
        reporteTrabajoAdapter.notifyItemChanged(detailsPostion)
        filtrosOrdenar(listOrdenReporte)
    }

    private fun actualizarEliminado()
    {

        //listOrdenReporte.remove()
        reporteTrabajoAdapter.notifyItemRemoved(detailsPostion)
        //reporteTrabajoAdapter.notifyItemRemoved(detailsPostion)
        //reporteTrabajoAdapter.setCount(listOrdenReporte.size)
        checkEmpyState()
    }

    private fun enviarPeticion()
    {
        listOrdenesAdd.forEach {
            val ordenTrabPrevistoRequest = OrdenTrabPrevistoRequest()
            ordenTrabPrevistoRequest.emplID = Prefs.getInt(PREFS.USUARIO_EMPLID,0)
            ordenTrabPrevistoRequest.usuario = Prefs.getString(PREFS.USUARIO,"")
            ordenTrabPrevistoRequest.osejectrabobjID = it.numeroOT?.toInt()
            ordenTrabPrevistoRequest.modeAction = OrdenTrabPrevistoRequest.MODE_ACTION.INS
            val call = RetrofitClient.instance?.api?.enviarTrabPrevisto(ordenTrabPrevistoRequest)
            showProgresBar()
            call!!.enqueue(object : Callback<ResponseWS?>
            {

                override fun onFailure(call: Call<ResponseWS?>, t: Throwable)
                {
                    this@ActividadReporteTrabajo.showSimpleError(t.message, TIPOMENSAJE.ERROR)
                    showProgresBar()
                }

                override fun onResponse(call: Call<ResponseWS?>, response: Response<ResponseWS?>)
                {
                        showProgresBar()
                        if ( response.isSuccessful)
                        {
                            val responseWS = response.body()
                            if(responseWS!!.sdtresult!!.success)
                            {
                                insertarOrdenesAsignadas(it)
                            }
                            else
                            {
                                responseWS.sdtresult?.let { sdt -> this@ActividadReporteTrabajo.mostarTodosLosMensajes(sdt) }

                            }
                        }

                }
            })
        }

    }


    private fun insertarOrdenesAsignadas(it: Ordenes)
    {

            val ordenReporte= OrdenReporte()
            ordenReporte.activada = false
            ordenReporte.isplay = false
            ordenReporte.ispasuse = false
            ordenReporte.numeroOT = it.numeroOT
            ordenReporte.descripcion = it.descripcion
            ordenReporte.estadoOT = it.estado
            ordenReporte.fechacreada = it.fecha
            ordenReporte.fechaInicioSgm = it.fecha
            ordenReporte.tiempoTrabajo = 0f
            ordenReporte.fechaInicio = ""
            ordenReporte.fechaFin = ""
            ordenReporte.isplay = false
            ordenReporte.ispasuse = false

            var existe = false
            for( miOrdenreporte in listOrdenReporte)
            {
                if(miOrdenreporte.ordenReporte!!.numeroOT == ordenReporte.numeroOT)
                {
                    existe = true
                    break
                }
            }
            if(!existe)
            {
                ordenReporteRoomDBRepository.insertOrdenesReporte(ordenReporte)
            }

    }

    private fun enviarPlayOrdenSgestman(ordenreporte: OrdenReporte?, position: Int, itemOrdenReporte: OrdenReporteTiempo)
    {
        //listOrdenReporte.sortByDescending { it.ispasuse}
        //listOrdenReporte.sortByDescending { it.isplay}
        val reporteTiempo = ReporteTiempo()
        reporteTiempo.idOrdenReporte = ordenreporte?.idOrden
        reporteTiempo.fechaInicio = MiFecha().fecha
        reporteTiempo.fechaFin = ""
        reporteTiempoRoomDBRepository.insertReporteTiempo(reporteTiempo)
        //val id = reporteTiempoRoomDBRepository.selectMaxID(ordenreporte?.idOrden)

       // reporteTiempo.idReporte =id
        (itemOrdenReporte.listReporteTiempo as ArrayList).add(reporteTiempo)


        listOrdenReporte[position] = itemOrdenReporte

        reporteTrabajoAdapter.notifyItemChanged(position)
       (reciclerview.adapter as ReporteTrabajoAdapter).moveUp(position)

       reciclerview.scrollToPosition(0)



        var prueba = ""
        //(reciclerview.adapter as ReporteTrabajoAdapter).notifyItemChanged(position)
    }
    private fun enviarPauseOrdenSgestman(ordenreporte: OrdenReporte?, position: Int, it: OrdenReporteTiempo)
    {
        val reporteTiempo = ReporteTiempo()
        reporteTiempo.idOrdenReporte = ordenreporte?.idOrden
        reporteTiempo.fechaFin= MiFecha().fecha
        reporteTiempoRoomDBRepository.updateOrdenesReporte(reporteTiempo)
        it.listReporteTiempo.forEach { if (it.idOrdenReporte==ordenreporte?.idOrden && it.fechaFin!!.isEmpty())
        {
            it.fechaFin = MiFecha().fecha
        }}

        reporteTrabajoAdapter.notifyItemChanged(position)
        //(reciclerview.adapter as ReporteTrabajoAdapter).notifyItemChanged(position)
    }


    private fun showFilter()
    {

        val mBottomSheetDialog = BottomSheetDialog(this, R.style.BaseBottomSheetDialog_BottomSheetDialogThemeRound)
        val sheetView = layoutInflater.inflate(R.layout.filter_reporte_trabajo, null)

        mBottomSheetDialog.setContentView(sheetView)
        val mBehavior = BottomSheetBehavior.from(sheetView.parent as View)
        mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        mBottomSheetDialog.setCancelable(true)
        mBottomSheetDialog.show()

        val btnAplicar = sheetView.findViewById<Button>(R.id.btnAplicar)
        val btnLimpiar = sheetView.findViewById<Button>(R.id.btnLimpiar)

        val numeroOT = sheetView.findViewById<TextView>(R.id.numero_OT)
        val estadoOT = sheetView.findViewById<ChipGroup>(R.id.estadoOT)
        val estadoReporte = sheetView.findViewById<ChipGroup>(R.id.estadoReporte)
        val fechaInicio = sheetView.findViewById<EditText>(R.id.fechaInicio)
        val fechaFin = sheetView.findViewById<EditText>(R.id.fechaFin)


        onclickImageDateFechaView(sheetView)

        estadoOT.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.chipNoInciada -> {
                    Prefs.putInt(PREFS.FILTER_ORD_REPORTE_ESTADO, EnumOrdenes.NOINICIADA.estate)
                    estadoInt = EnumOrdenes.NOINICIADA.estate
                }
                R.id.chipVigente -> {
                    Prefs.putInt(PREFS.FILTER_ORD_REPORTE_ESTADO, EnumOrdenes.VIGENTE.estate)
                    estadoInt = EnumOrdenes.VIGENTE.estate
                }
            }

        }

        estadoReporte.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.chipPlay -> {
                    Prefs.putInt(PREFS.FILTER_ORD_REPORTE_ITEMS_ESTADO, 1)
                    estadoOrdenReporte = 1
                }
                R.id.chipPause -> {
                    Prefs.putInt(PREFS.FILTER_ORD_REPORTE_ITEMS_ESTADO, 2)
                    estadoOrdenReporte = 1
                }
            }

        }


        addFormato(fechaInicio,fechaFin)

        ///
        llenarFiltros(sheetView)

        btnAplicar.setOnClickListener {
            filtros(numeroOT.text.toString(),
                    estadoInt,
                    estadoOrdenReporte,
                    fechaInicio.text.toString(),
                    fechaFin.text.toString())
            mBottomSheetDialog.dismiss()
            getOrdenReporte()
        }
        btnLimpiar.setOnClickListener {
            limpiarFiltros()
            mBottomSheetDialog.dismiss()
            getOrdenReporte()
        }
    }

    private fun addFormato(fechaInicio:EditText, fechaFin:EditText)
    {
        fechaInicio.addTextChangedListener(object: TextWatcher
        {
            var sb : StringBuilder = StringBuilder("")

            var ignore = false

            override fun afterTextChanged(s: Editable?)
            {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
            {

                if(ignore){
                    ignore = false
                    return
                }

                sb.clear()
                sb.append(if(s!!.length > 10){ s.subSequence(0,10) }else{ s })

                when (sb.lastIndex) {
                    2 -> {
                        when {
                            sb[2] != '/' -> {
                                val sb1 = sb.toString().substring(1..2)
                                if(sb1.toInt() in 1..31) {
                                    sb.insert(2,"/")
                                } else {
                                    sb.clear()
                                }
                            }
                        }

                    }
                    5 -> {
                        when {
                            sb[5] != '/' -> {
                                val sb1 = sb.toString().substring(3..4)
                                if (sb1.toInt() in 2..12) {
                                    sb.insert(5,"/")
                                } else {
                                    sb.clear()
                                }
                            }
                        }

                    }
                    9 -> {
                        val sb1 = sb.toString().substring(6,10)
                        when {
                            sb1.toInt() < 2015 || sb1.toInt() > 2030 -> {
                                sb.clear()
                            }
                        }
                    }
                }

                ignore = true
                fechaInicio.setText(sb.toString())
                fechaInicio.setSelection(sb.length)
            }
        })
        fechaFin.addTextChangedListener(object: TextWatcher
        {
            var sb : StringBuilder = StringBuilder("")

            var ignore = false

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
            {

                if(ignore){
                    ignore = false
                    return
                }

                sb.clear()
                sb.append(if(s!!.length > 10){ s.subSequence(0,10) }else{ s })

                when (sb.lastIndex) {
                    2 -> {
                        when {
                            sb[2] != '/' -> {
                                val sb1 = sb.toString().substring(1..2)
                                if(sb1.toInt() in 1..31) {
                                    sb.insert(2,"/")
                                } else {
                                    sb.clear()
                                }
                            }
                        }

                    }
                    5 -> {
                        when {
                            sb[5] != '/' -> {
                                val sb1 = sb.toString().substring(3..4)
                                if (sb1.toInt() in 2..12) {
                                    sb.insert(5,"/")
                                } else {
                                    sb.clear()
                                }
                            }
                        }

                    }
                    9 -> {
                        val sb1 = sb.toString().substring(6,10)
                        when {
                            sb1.toInt() < 2015 || sb1.toInt() > 2030 -> {
                                sb.clear()
                            }
                        }
                    }
                }

                ignore = true
                fechaFin.setText(sb.toString())
                fechaFin.setSelection(sb.length)
            }
        })


        fechaInicio.setOnFocusChangeListener { view, focus -> onFocusEditext(focus,view) }
        fechaFin.setOnFocusChangeListener { view, focus -> onFocusEditext(focus,view) }
    }
    private fun onFocusEditext(b: Boolean,view:View)
    {
        view as TextView
        when {
            !b && view.text.length< 10-> {
                view.text = ""
            }
        }
    }
    private fun llenarFiltros(sheetView: View)
    {
        val numeroOT = sheetView.findViewById<TextView>(R.id.numero_OT)
        val fechaInicio = sheetView.findViewById<TextView>(R.id.fechaInicio)
        val fechaFin = sheetView.findViewById<TextView>(R.id.fechaFin)
        val estadoOT = sheetView.findViewById<ChipGroup>(R.id.estadoOT)
        val estadoReporte = sheetView.findViewById<ChipGroup>(R.id.estadoReporte)


        numeroOT.text = Prefs.getString(PREFS.FILTER_ORD_REPORTE_NUMERO,"")
        fechaInicio.text = Prefs.getString(PREFS.FILTER_ORD_REPORTE_FECHAINICIO,"")
        fechaFin.text = Prefs.getString(PREFS.FILTER_ORD_REPORTE_FECHAFIN,"")
        var estadoint = Prefs.getInt(PREFS.FILTER_ORD_REPORTE_ESTADO,0)
        val estadoOrdenReporte = Prefs.getInt(PREFS.FILTER_ORD_REPORTE_ITEMS_ESTADO,0)

        when {
            estadoint != 0 ->
            {
                ///esto es porque el chip llega hasta 2 valores y el estado de la orden vigente es 3
                if(estadoint ==3)
                {
                    estadoint = 2
                }
                val chip = estadoOT.getChildAt(estadoint-1) as Chip
                chip.isChecked = true
            }
        }
        when {
            estadoOrdenReporte != 0 -> {
                        val chip = estadoReporte.getChildAt(estadoOrdenReporte-1) as Chip
                        chip.isChecked = true
                    }
                }


    }

    private fun filtros(numeroOT:String, estado:Int,estadoReporte:Int, fechaInicio:String, fechaFin:String)
    {
        Prefs.putString(PREFS.FILTER_ORD_REPORTE_NUMERO, numeroOT)
        Prefs.putString(PREFS.FILTER_ORD_REPORTE_FECHAINICIO, fechaInicio)
        Prefs.putString(PREFS.FILTER_ORD_REPORTE_FECHAFIN, fechaFin)
        Prefs.putInt(PREFS.FILTER_ORD_REPORTE_ESTADO,estado)
        Prefs.putInt(PREFS.FILTER_ORD_REPORTE_ITEMS_ESTADO,estadoReporte)
        Prefs.putBoolean(PREFS.FILTER_ORD_REPORTE_ISFILTER,true)
        viewmodel = OrdenReporteViewModel(application)
    }
    private fun limpiarFiltros()
    {
        Prefs.putString(PREFS.FILTER_ORD_REPORTE_NUMERO,"")
        Prefs.putString(PREFS.FILTER_ORD_REPORTE_FECHAINICIO,"")
        Prefs.putString(PREFS.FILTER_ORD_REPORTE_FECHAFIN,"")
        Prefs.putInt(PREFS.FILTER_ORD_REPORTE_ESTADO,0)
        Prefs.putInt(PREFS.FILTER_ORD_REPORTE_ITEMS_ESTADO,0)
        Prefs.putBoolean(PREFS.FILTER_ORD_REPORTE_ISFILTER,false)
        viewmodel = OrdenReporteViewModel(application)
    }

    @SuppressLint("SetTextI18n")
    private fun onclickImageDateFechaView(sheetView: View?)
    {

        val fechaInicio = sheetView?.findViewById<EditText>(R.id.fechaInicio)
        val fechaFin = sheetView?.findViewById<EditText>(R.id.fechaFin)
        val fechaInicioImg = sheetView?.findViewById<ImageView>(R.id.fechaInicioImg)
        val fechaFinImg = sheetView?.findViewById<ImageView>(R.id.fechaFinImg)


        fechaInicioImg?.setOnClickListener {

            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->

                        val dia = if (dayOfMonth < 10) {"0$dayOfMonth"}else {"$dayOfMonth"}
                        val mes = if (month < 10) {"0$month"}else {"$month"}
                        fechaInicio?.setText("$dia/$mes/$year")
                    }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }

        fechaFinImg?.setOnClickListener {

            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                        val dia = if (dayOfMonth < 10) {"0$dayOfMonth"}else {"$dayOfMonth"}
                        val mes = if (month < 10) {"0$month"}else {"$month"}
                        fechaFin?.setText("$dia/$mes/$year")
                    }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }
    }



    ///////////////////////////////
    ///////////////////////////////
    //////LISTENER/////////////////
    ///////////////////////////////
    override fun playListener(ordenreporte: OrdenReporte?,doPlay:Boolean,position:Int)
    {
        if(doPlay)
        {

            listOrdenReporte.forEach { itemOrdenReporte-> if(itemOrdenReporte.ordenReporte!!.numeroOT == ordenreporte?.numeroOT)
            {
                // it.isplay = true
                // it.ispasuse= false
                itemOrdenReporte.ordenReporte!!.ispasuse = ordenreporte?.ispasuse
                itemOrdenReporte.ordenReporte!!.isplay = ordenreporte?.isplay
                itemOrdenReporte.ordenReporte!!.activada = ordenreporte?.activada
                ordenReporteRoomDBRepository.updateOrdenesReporte(ordenreporte)
                enviarPlayOrdenSgestman(ordenreporte, position,itemOrdenReporte)
            }}
        }
        else
        {
            Toast.makeText(applicationContext,"Ya hay iniciada una actividad",Toast.LENGTH_SHORT).show()
        }

    }
    override fun pauseListener(ordenreporte: OrdenReporte?,position: Int) {
        listOrdenReporte.forEach { if(it.ordenReporte!!.numeroOT == ordenreporte?.numeroOT)
        {
            it.ordenReporte!!.ispasuse = ordenreporte?.ispasuse
            it.ordenReporte!!.isplay = ordenreporte?.isplay
            it.ordenReporte!!.activada = ordenreporte?.activada
            ordenReporteRoomDBRepository.updateOrdenesReporte(ordenreporte)
            enviarPauseOrdenSgestman(ordenreporte,position,it)
        }}
    }

    override fun detailsListener(ordenreporte: OrdenReporte?, position: Int)
    {
        if(permisoReporTrab.ver==1)
        {
            detailsPostion = position
            var itemOrdenReporteTiempo: OrdenReporteTiempo
            listOrdenReporte.forEach {
                if (it.ordenReporte?.numeroOT.equals(ordenreporte?.numeroOT))
                {
                    itemOrdenReporteTiempo = it
                    val intent = Intent(this@ActividadReporteTrabajo,ActividadReporteTrabajoDetails::class.java)
                    intent.putExtra(INTENT.EXTRA_ORDEN_REPORTE_TIEMPO,itemOrdenReporteTiempo)
                    intent.putExtra(INTENT.EXTRA_HAY_OT_PLAY,isOTPlay())
                    intent.putExtra(INTENT.EXTRA_PERMISOS,permisoReporTrab)
                    startActivityForResult(intent,REQUESTCODE.ACTIVIDAD_REPORTE_TIEMPO_DETAILS)

                }
            }
        }
        else
        {
            Log.d("errorokok",permisoReporTrab.opcion!!)
            showErrorDenied(permisoReporTrab.opcion,"Ver")
        }

    }

    private fun isOTPlay(): Boolean {
        return listOrdenReporte.any { it.ordenReporte!!.isplay!! }
    }

    override fun longListener(ordenreportetiempo: OrdenReporteTiempo?,position: Int)
    {
        val mBottomSheetDialog = BottomSheetDialog(layoutInflater.context, R.style.BaseBottomSheetDialog_BottomSheetDialogThemeRound)
        val sheetView = layoutInflater.inflate(R.layout.view_bottonsheet_menu, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.setCancelable(true)

        val lyVer = sheetView.findViewById<LinearLayout>(R.id.lyVer)
        val lyEditar = sheetView.findViewById<LinearLayout>(R.id.lyEditar)
        val lyEliminar = sheetView.findViewById<LinearLayout>(R.id.lyEliminar)

        if(permisoReporTrab.ver == 0)
        {
            lyVer.visibility = View.GONE
        }
        if(permisoReporTrab.del == 0)
        {
            lyEliminar.visibility = View.GONE
        }
        lyEditar.visibility = View.GONE

        lyVer.setOnClickListener {
            mBottomSheetDialog.dismiss()
            val intent = Intent(this@ActividadReporteTrabajo,ActividadReporteTrabajoDetails::class.java)
            intent.putExtra(INTENT.EXTRA_ORDEN_REPORTE_TIEMPO,ordenreportetiempo)
            intent.putExtra(INTENT.EXTRA_PERMISOS,permisoReporTrab)
            startActivityForResult(intent,REQUESTCODE.ACTIVIDAD_REPORTE_TIEMPO_DETAILS)
        }
        lyEliminar.setOnClickListener {
            mBottomSheetDialog.dismiss()
            eliminarReporteTrabajo(ordenreportetiempo,position)
        }


        mBottomSheetDialog.show()
    }

    private fun eliminarReporteTrabajo(ordenreportetiempo: OrdenReporteTiempo?,position: Int)
    {
        ///primero eliminar del servidor luego esto
        eliminarWS(ordenreportetiempo,position)
    }

    private fun eliminarWS(ordenreportetiempo: OrdenReporteTiempo?,position: Int)
    {
        //muestro el progres bar
        showProgresBar()
        val ordenTrabPrevistoRequest = OrdenTrabPrevistoRequest()
        ordenTrabPrevistoRequest.osejectrabobjID = ordenreportetiempo!!.ordenReporte!!.numeroOT!!.toInt()
        ordenTrabPrevistoRequest.fechaDia = MiFecha().fechaRequest(ordenreportetiempo.ordenReporte!!.fechaInicioSgm)
        ordenTrabPrevistoRequest.usuario = Prefs.getString(PREFS.USUARIO,"")
        ordenTrabPrevistoRequest.emplID = Prefs.getInt(PREFS.USUARIO_EMPLID,0)
        ordenTrabPrevistoRequest.modeAction = OrdenTrabPrevistoRequest.MODE_ACTION.DEL

        val callEliminar = RetrofitClient.instance!!.api.enviarTrabPrevisto(ordenTrabPrevistoRequest)

        callEliminar.enqueue(object :Callback<ResponseWS>{
            override fun onFailure(call: Call<ResponseWS>, t: Throwable)
            {
                showProgresBar()
                showSimpleError(t.message,TIPOMENSAJE.ERROR)
            }

            override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>)
            {
                if (response.isSuccessful)
                {
                    if (response.body()!!.sdtresult!!.success)
                    {
                        eliminarLocal(ordenreportetiempo,position)
                    }
                    else
                    {
                        mostarTodosLosMensajes(response.body()!!.sdtresult!!)
                        showProgresBar()
                    }
                }
            }
        })
    }
    private fun eliminarLocal(ordenreportetiempo: OrdenReporteTiempo?,position: Int)
    {
        ordenReporteRoomDBRepository.deleteOrdenesReporte(ordenreportetiempo!!.ordenReporte)

        ///elimina del servidor las activas
        val ordenReporteWS = OrdenReporteWS()
        ordenReporteWS.numeroOT = ordenreportetiempo.ordenReporte!!.numeroOT
        ordenReporteWSRoomDBRepository.deleteOrdenesReporteWS(ordenReporteWS)

        listOrdenReporte.removeIf { it.ordenReporte!!.numeroOT.equals(ordenreportetiempo.ordenReporte!!.numeroOT) }
        //reporteTrabajoAdapter.mFeedList.removeIf{it.ordenReporte!!.numeroOT.equals(ordenreportetiempo!!.ordenReporte!!.numeroOT)}
        //reporteTrabajoAdapter.mFeedList = listOrdenReporte
        reporteTrabajoAdapter.removeAt(position)
        //reporteTrabajoAdapter.setCount(listOrdenReporte.size)
        checkEmpyState()
        showProgresBar()
    }

    override fun playListener1(ordenreporte: OrdenReporte?, doPlay: Boolean, position: Int)
    {
        if(doPlay)
        {

            listOrdenReporte.forEach { itemOrdenReporte-> if(itemOrdenReporte.ordenReporte!!.numeroOT == ordenreporte?.numeroOT)
            {
                // it.isplay = true
                // it.ispasuse= false
                itemOrdenReporte.ordenReporte!!.ispasuse = ordenreporte?.ispasuse
                itemOrdenReporte.ordenReporte!!.isplay = ordenreporte?.isplay
                itemOrdenReporte.ordenReporte!!.activada = ordenreporte?.activada
                ordenReporteRoomDBRepository.updateOrdenesReporte(ordenreporte)
                enviarPlayOrdenSgestman(ordenreporte, position,itemOrdenReporte)
            }}
        }
        else
        {
            showSimpleError("Ya existe iniciado otro trabajo",TIPOMENSAJE.INFO)
        }
    }

    override fun pauseListener1(ordenreporte: OrdenReporte?, position: Int)
    {

    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(INTENT.EXTRA_PERMISOS,permisoReporTrab)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        permisoReporTrab = savedInstanceState.getSerializable(INTENT.EXTRA_PERMISOS) as Permisos
    }
}