package com.example.carlos.sgestman.actividades.ui.solicitud

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.forEach
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.actividades.BasicActivity
import com.example.carlos.sgestman.adapter.AutoCompleClasifAdapter
import com.example.carlos.sgestman.adapter.AutoCompleClienteAdapter
import com.example.carlos.sgestman.adapter.AutoCompleEjecAdapter
import com.example.carlos.sgestman.entity.*
import com.example.carlos.sgestman.model.ResponseSdtresult
import com.example.carlos.sgestman.model.ResponseWS
import com.example.carlos.sgestman.model.SolicitudRequest
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.utils.HandleErrorApi
import com.example.carlos.sgestman.utils.PermisionApp
import com.example.carlos.sgestman.utils.constant.PERMISSION
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.constant.REQUESTCODE
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.BottomSheetMensaje
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_crear_solicitud.*
import moe.feng.common.stepperview.VerticalStepperItemView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream

class ActividadCrearSolicitud : BasicActivity() {
    private var fileUri: Uri? = null
    private var img_bitmap: Bitmap? = null
    private var add_solicitud = false
    private var isSelecEjecutor = false
    private var isSelecClasifcacion = false
    private var isSelecCliente = false
    private lateinit var ejecutorAdapter: AutoCompleEjecAdapter
    private lateinit var clienteAdapter: AutoCompleClienteAdapter
    private lateinit var clasifacioneAdapter: AutoCompleClasifAdapter
    private lateinit var toolbar: Toolbar
    private val mSteppers = ArrayList<VerticalStepperItemView>()
    private var isEjecutor = true //configuracion
    private var isObjetoQR = false //configuracion
    private var isClasificacion = false //configuracion
    private var listaEjecutores:List<Ejecutor>?= listOf()
    private var listaClientes:List<Cliente>?= listOf()
    private var listaClasiicaciones:List<Clasificaciones>? = listOf()
    private var solicitud = Solicitudes()

    var indexClasificacion = 0
    var indexEjecutor = 0
    var indexCliente = 0
    var indexObjeto = 0
    var indexImagen = 0
    var isError = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crear_solicitud)

        setupView()
    }


    private fun setupView()
    {
        setupVariables()
        setTollbar()
        setupPrefs()
        getAllList()

    }
    private fun setupVariables()
    {

        //showProgresBar()
    }

    private fun setupPrefs() {

        isEjecutor = Prefs.getBoolean(PREFS.CONFIGURACION_SOL_IS_EJECUTOR,true)
        isClasificacion = Prefs.getBoolean(PREFS.CONFIGURACION_SOL_IS_CLASIFICACION,true)
        isObjetoQR = false
    }


    @SuppressLint("SetTextI18n")
    fun setTollbar()
    {
        toolbar = findViewById<View>(R.id.my_awesome_toolbar) as Toolbar
        toolbar.title = ""
        toolbar.navigationIcon= ContextCompat.getDrawable(this,R.drawable.ic_left_arrow)
        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        val mtitleToolbar = toolbar.findViewById(R.id.toolbar_title) as TextView
        mtitleToolbar.text = "Crear Solicitud"
    }
    private fun setupStepper()
    {
        mSteppers.add(findViewById(R.id.stepper_1))
        mSteppers[0].index= mSteppers.size
        setupBtnStepper1()
        stepper_1.setOnClickListener { SiguientePaso(it) }

        if(isClasificacion)
        {
            mSteppers.add(findViewById(R.id.stepper_2))
            indexClasificacion = mSteppers.size - 1
            mSteppers[indexClasificacion].index= mSteppers.size
            autotextClasificacion.threshold = 1
//            imageDownClasificacion.setOnClickListener{
//                autotextClasificacion.showDropDown()}
            setupBtnStepper2()
            stepper_2.setOnClickListener { SiguientePaso(it) }
        }
        else
        {
            stepper_2.visibility = View.GONE
        }

        if(isEjecutor)
        {
            mSteppers.add(findViewById(R.id.stepper_3))
            autotextEjecutor.threshold = 1
            indexEjecutor = mSteppers.size - 1
            mSteppers[indexEjecutor].index= mSteppers.size
            imageDownEjecutor.setOnClickListener{
                autotextEjecutor.showDropDown()}
            setupBtnStepper3()
            stepper_3.setOnClickListener { SiguientePaso(it) }

        }
        else
        {
            stepper_3.visibility = View.GONE
        }
        if(!isObjetoQR)
        {
            mSteppers.add(findViewById(R.id.stepper_4))
            indexCliente = mSteppers.size - 1
            autotextCliente.threshold = 1
            mSteppers[indexCliente].index= mSteppers.size
            imageDownCliente.setOnClickListener{
                autotextCliente.showDropDown()}
            setupBtnStepper4()
            stepper_4.setOnClickListener { SiguientePaso(it) }
        }


        mSteppers.add(findViewById(R.id.stepper_5))
        indexObjeto = mSteppers.size - 1
        mSteppers[indexObjeto].index= mSteppers.size
        setupBtnStepper5()
        stepper_5.setOnClickListener { SiguientePaso(it) }
        addItemsLy()
        imgAddItems.setOnClickListener {
            if(!isObjetoQR)
            {
                addItemsLy()
            }
        }

        mSteppers.add(findViewById(R.id.stepper_6))
        indexImagen = mSteppers.size - 1
        mSteppers[indexImagen].index= mSteppers.size
        setupBtnStepper6()
        stepper_6.setOnClickListener { SiguientePaso(it) }

        VerticalStepperItemView.bindSteppers(mSteppers)


    }

    private fun SiguientePaso(it: View)
    {
        ///descripcion
        if(it.id == R.id.stepper_1)
        {
            mSteppers[1].prevStep()
        }
        ///////clasificacion
        if(it.id == R.id.stepper_2)
        {

            if(mSteppers[indexClasificacion].state == 2)
            {
                for(i in 0 until mSteppers.count())
                {
                    if(mSteppers[i].id == R.id.stepper_2)
                    {
                        for (j in i +1 until mSteppers.count())
                        {
                            if(mSteppers[j].state == 2 || mSteppers[j].state == 1)
                            {
                                mSteppers[j].prevStep()
                            }
                        }
                    }
                }

            }
            else
            {
                if(txt_soldescripcion.text.isBlank())
                {
                    mSteppers[0].errorText = "Falta la descripción de la solicitud"
                    txt_soldescripcion.setBackgroundResource(R.drawable.edit_round_focus)
                }
                else
                {
                    mSteppers[0].errorText= null
                    mSteppers[0].isActivated = true
                    mSteppers[0].nextStep()
                    solicitud.descripcion = txt_soldescripcion.text.toString()
                    txt_soldescripcion.setBackgroundResource(R.drawable.edit_round)
                    autotextClasificacion.requestFocus()
                }
            }

        }
        ////ejecutor
        if (it.id == R.id.stepper_3)
        {
            if(mSteppers[indexEjecutor].state==2)
            {
                for(i in 0 until mSteppers.count())
                {
                    if(mSteppers[i].id == R.id.stepper_3)
                    {
                        for (j in i +1 until mSteppers.count())
                        {
                            if(mSteppers[j].state == 2 || mSteppers[j].state == 1)
                            {
                                mSteppers[j].prevStep()
                            }
                        }
                    }
                }
            }
            else {
                val clasificacion = Clasificaciones()
                clasificacion.clasosDenom = autotextClasificacion.text.toString()
                if (autotextClasificacion.text.isBlank() || !checkClasificacion()) {
                    mSteppers[indexClasificacion].errorText = "Falta la clasificación de la solicitud"
                    ly_clasificacion.setBackgroundResource(R.drawable.edit_round_focus)
                } else {
                    mSteppers[indexClasificacion].errorText = null
                    mSteppers[indexClasificacion].isActivated = true
                    mSteppers[indexClasificacion].nextStep()
                    solicitud.clasosDenom = autotextClasificacion.text.toString()
                    ly_clasificacion.setBackgroundResource(R.drawable.edit_round)
                    autotextEjecutor.requestFocus()
                }
            }
        }
        ////cliente
        if (it.id == R.id.stepper_4)
        {
            if(mSteppers[indexCliente].state==2)
            {
                for(i in 0 until mSteppers.count())
                {
                    if(mSteppers[i].id == R.id.stepper_4)
                    {
                        for (j in i +1 until mSteppers.count())
                        {
                            if(mSteppers[j].state == 2 || mSteppers[j].state == 1)
                            {
                                mSteppers[j].prevStep()
                            }
                        }
                    }
                }
            }
            else {
                if(autotextEjecutor.text.isBlank() || !checkEjecutor())
                {
                    mSteppers[indexEjecutor].errorText = "Falta el ejecutor de la solicitud"
                    ly_ejecutor.setBackgroundResource(R.drawable.edit_round_focus)
                }
                else
                {
                    mSteppers[indexEjecutor].errorText= null
                    mSteppers[indexEjecutor].isActivated = true
                    mSteppers[indexEjecutor].nextStep()
                    solicitud.ejecutorCod = autotextEjecutor.text.toString()
                    ly_ejecutor.setBackgroundResource(R.drawable.edit_round)
                    autotextCliente.requestFocus()
                }
            }
        }
        ////objeto
        if (it.id == R.id.stepper_5)
        {
            if(mSteppers[indexObjeto].state==2)
            {
                for(i in 0 until mSteppers.count())
                {
                    if(mSteppers[i].id == R.id.stepper_5)
                    {
                        for (j in i +1 until mSteppers.count())
                        {
                            if(mSteppers[j].state == 2 || mSteppers[j].state == 1)
                            {
                                mSteppers[j].prevStep()
                            }
                        }
                    }
                }
            }
            else {
                if(autotextCliente.text.isBlank() || !checkCliente())
                {
                    mSteppers[indexCliente].errorText = "Falta el cliente de la solicitud"
                    ly_cliente.setBackgroundResource(R.drawable.edit_round_focus)
                }
                else
                {
                    mSteppers[indexCliente].errorText= null
                    mSteppers[indexCliente].isActivated = true
                    mSteppers[indexCliente].nextStep()
                    solicitud.clienteCod = autotextCliente.text.toString()
                    ly_cliente.setBackgroundResource(R.drawable.edit_round)
                }
            }
        }

        ////imagen
        if (it.id == R.id.stepper_6)
        {
            if(mSteppers[indexImagen].state==2)
            {
                for(i in 0 until mSteppers.count())
                {
                    if(mSteppers[i].id == R.id.stepper_6)
                    {
                        for (j in i +1 until mSteppers.count())
                        {
                            if(mSteppers[j].state == 2 || mSteppers[j].state == 1)
                            {
                                mSteppers[j].prevStep()
                            }
                        }
                    }
                }
            }
            else {
                    if(revisarAlgunoEnBlanco())
                    {
                        mSteppers[indexObjeto].errorText = "Falta el objeto de la solicitud"
                    }
                    else
                    {
                        mSteppers[indexObjeto].errorText= null
                        mSteppers[indexObjeto].isActivated = true
                        mSteppers[indexObjeto].nextStep()
                    }
                }
        }
    }


    private fun getAllList()
    {
        ly_mensajeEstate.visibility = View.GONE
        shimmer_view_container.visibility = View.VISIBLE
        shimmer_view_container.startShimmer()
        if(isClasificacion)
        {
            callClasificacion()
        }
        else if(isEjecutor)
        {
            callEjectuor()
        }
        else if(!isObjetoQR)
        {
            callCliente()
        }

    }
    private fun showErrorMensaje(error:String )
    {
        shimmer_view_container.visibility = View.GONE
        ly_mensajeEstate.visibility = View.VISIBLE
        load_state_errorMessage.text = error
        load_state_retry.setOnClickListener { getAllList() }
    }
    private fun showStepper()
    {
        shimmer_view_container.visibility = View.GONE
        ly_mensajeEstate.visibility = View.GONE
        load_state_errorMessage.text = ""
        nestedScrollView.visibility = View.VISIBLE
        llenarAdapterClasificacion()
        llenarAdapterEjecutor()
        llenarAdapterClientes()
        setupStepper()
    }
    private fun callClasificacion()
    {
        val call1 = RetrofitClient.instance?.api?.getClasificaciones()
        call1!!.enqueue(object : Callback<List<Clasificaciones>>{
            override fun onFailure(call: Call<List<Clasificaciones>>, t: Throwable)
            {
                showErrorMensaje(HandleErrorApi(applicationContext,t).mensaje())
            }

            override fun onResponse(call: Call<List<Clasificaciones>>, response: Response<List<Clasificaciones>>) {
                if (response.isSuccessful)
                {
                    if(!response.body().isNullOrEmpty())
                    {
                        listaClasiicaciones = response.body()
                    }
                    if (isEjecutor)
                        callEjectuor()
                    else if(!isEjecutor && !isObjetoQR)
                        callCliente()
                    else
                        showStepper()
                }
            }
        })
    }
    private fun callEjectuor()
    {
        val callEjecutor = RetrofitClient.instance?.api?.getEjecutores("","")
        callEjecutor!!.enqueue(object : Callback<List<Ejecutor>>{
            override fun onFailure(call: Call<List<Ejecutor>>, t: Throwable)
            {
                showErrorMensaje(HandleErrorApi(applicationContext,t).mensaje())
            }

            override fun onResponse(call: Call<List<Ejecutor>>, response: Response<List<Ejecutor>>) {
                if (response.isSuccessful)
                {
                    if(!response.body().isNullOrEmpty())
                    {
                        listaEjecutores = response.body()
                    }
                    if(!isObjetoQR)
                        callCliente()
                    else
                        showStepper()
                }
            }
        })
    }
    private fun callCliente()
    {
        val call1 = RetrofitClient.instance?.api?.getClientes("","")
        call1!!.enqueue(object : Callback<List<Cliente>>{
            override fun onFailure(call: Call<List<Cliente>>, t: Throwable)
            {
                showErrorMensaje(HandleErrorApi(applicationContext,t).mensaje())
                isError = true
            }

            override fun onResponse(call: Call<List<Cliente>>, response: Response<List<Cliente>>) {
                if (response.isSuccessful)
                {
                    if(!response.body().isNullOrEmpty())
                    {
                        listaClientes = response.body()
                    }
                    showStepper()
                }
            }
        })
    }

    private fun setupBtnStepper1() {
        mNextBtn1.setOnClickListener {
            if(txt_soldescripcion.text.isBlank())
            {
                mSteppers[0].errorText = "Falta la descripción de la solicitud"
                txt_soldescripcion.setBackgroundResource(R.drawable.edit_round_focus)
            }
            else
            {
                mSteppers[0].errorText= null
                mSteppers[0].isActivated = true
                mSteppers[0].nextStep()
                solicitud.descripcion = txt_soldescripcion.text.toString()
                txt_soldescripcion.setBackgroundResource(R.drawable.edit_round)
            }
        }

        mPrevBtn1.setOnClickListener {
            run {
                onBackPressed()
            }
        }
    }
    private fun setupBtnStepper2() {
        mNextBtn2.setOnClickListener {
            val clasificacion = Clasificaciones()
            clasificacion.clasosDenom = autotextClasificacion.text.toString()
            if(autotextClasificacion.text.isBlank() || !checkClasificacion())
            {
                mSteppers[indexClasificacion].errorText = "Falta la clasificación de la solicitud"
                ly_clasificacion.setBackgroundResource(R.drawable.edit_round_focus)
            }
            else
            {
                mSteppers[indexClasificacion].errorText= null
                mSteppers[indexClasificacion].isActivated = true
                mSteppers[indexClasificacion].nextStep()
                solicitud.clasosDenom = autotextClasificacion.text.toString()
                ly_clasificacion.setBackgroundResource(R.drawable.edit_round)
            }
        }

        mPrevBtn2.setOnClickListener {
            run {
                mSteppers[indexClasificacion].prevStep()
            }
        }
    }

    private fun setupBtnStepper3()
    {
        mNextBtn3.setOnClickListener {
            if(autotextEjecutor.text.isBlank() || !checkEjecutor())
            {
                mSteppers[indexEjecutor].errorText = "Falta el ejecutor de la solicitud"
                ly_ejecutor.setBackgroundResource(R.drawable.edit_round_focus)
            }
            else
            {
                mSteppers[indexEjecutor].errorText= null
                mSteppers[indexEjecutor].isActivated = true
                mSteppers[indexEjecutor].nextStep()
                solicitud.ejecutorCod = autotextEjecutor.text.toString()
                ly_ejecutor.setBackgroundResource(R.drawable.edit_round)
            }
        }

        mPrevBtn3.setOnClickListener {
            mSteppers[indexEjecutor].prevStep()
        }
    }
    private fun setupBtnStepper4()
    {
        mNextBtn4.setOnClickListener {
            if(autotextCliente.text.isBlank() || !checkCliente())
            {
                mSteppers[indexCliente].errorText = "Falta el cliente de la solicitud"
                ly_cliente.setBackgroundResource(R.drawable.edit_round_focus)
            }
            else
            {
                mSteppers[indexCliente].errorText= null
                mSteppers[indexCliente].isActivated = true
                mSteppers[indexCliente].nextStep()
                solicitud.clienteCod = autotextCliente.text.toString()
                ly_cliente.setBackgroundResource(R.drawable.edit_round)
            }
        }

        mPrevBtn4.setOnClickListener {
            mSteppers[indexCliente].prevStep()
        }
    }
    private fun setupBtnStepper5()
    {
        mNextBtn5.setOnClickListener {
            if(revisarAlgunoEnBlanco())
            {
                mSteppers[indexObjeto].errorText = "Falta el objeto de la solicitud"
            }
            else
            {
                mSteppers[indexObjeto].errorText= null
                mSteppers[indexObjeto].isActivated = true
                mSteppers[indexObjeto].nextStep()
                llenarObjetosSolicitud()
                enviarSolicitud1()
            }
        }


        mPrevBtn5.setOnClickListener {
            mSteppers[indexObjeto].prevStep()
        }
    }
    private fun setupBtnStepper6()
    {
        mBtnFinalizar.setOnClickListener {
            if(revisarAlgunoEnBlanco())
            {
                mSteppers[indexObjeto].errorText = "Falta el objeto de la solicitud"
            }
            else
            {
//                mSteppers[indexObjeto].errorText= null
//                mSteppers[indexObjeto].isActivated = true
//                mSteppers[indexObjeto].nextStep()
                llenarObjetosSolicitud()
                enviarSolicitud1()
            }
        }

        mBtnFinalizarCancelr.setOnClickListener {
            mSteppers[indexImagen].prevStep()
        }
    }

    private fun llenarObjetosSolicitud() {

        val listObjetosSol: ArrayList<ObjetosSol> = arrayListOf()
        ly_item_objetos.forEach {
            it.findViewById<LinearLayout>(R.id.ly_objeto)
            val autotexObjeto = it.findViewById<AutoCompleteTextView>(R.id.autotextObjeto)
            val objetosSol = ObjetosSol()
            objetosSol.objpatCodigo = autotexObjeto.text.toString()
            listObjetosSol.add(objetosSol)
            solicitud.Objetos = listObjetosSol
        }
    }

    private fun enviarSolicitud()
    {
        showProgresBar()

        val solicitudRequest = SolicitudRequest()
        //solicitudRequest.usuario = "GAMMA"
        solicitudRequest.usuario = Prefs.getString(PREFS.USUARIO, "")
        solicitudRequest.solicitud = solicitud
        solicitudRequest.modeAction = SolicitudRequest.MODEACTION.INS


        val callAdicionarSolicitud = RetrofitClient.instance?.api?.adicionarSolicitud(solicitudRequest)
        callAdicionarSolicitud?.enqueue(object: Callback<ResponseWS>{
            override fun onFailure(call: Call<ResponseWS>, t: Throwable)
            {
                ShowError(t.message, TIPOMENSAJE.ERROR)
                showProgresBar()
                add_solicitud = false
            }

            override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>) {
                if(response.isSuccessful)
                {
                    showProgresBar()
                    val responseWS = response.body()

                    if (responseWS!!.sdtresult?.success!!)
                    {
                        add_solicitud = true
                        onBackPressed()
                    }
                    else
                        {
                            responseWS.sdtresult?.let { MostarTodosLosMensajes(it) }
                            add_solicitud = false
                        }
                }
            }
        })
    }



    private fun revisarAlgunoEnBlanco(): Boolean
    {
        var thereIsBlank = false
        ly_item_objetos.forEach {
            val lyObjeto = it.findViewById<LinearLayout>(R.id.ly_objeto)
            val autotexObjeto = it.findViewById<AutoCompleteTextView>(R.id.autotextObjeto)
            lyObjeto.setBackgroundResource(R.drawable.edit_round_focus)

            if (autotexObjeto.text.isBlank())
                thereIsBlank =  true
            else
            {
                lyObjeto.setBackgroundResource(R.drawable.edit_round)
            }
        }
        return thereIsBlank
    }

    private fun llenarAdapterClasificacion() {

        clasifacioneAdapter = AutoCompleClasifAdapter(applicationContext,R.layout.spinner_filter,listaClasiicaciones!!)
        autotextClasificacion.setAdapter(clasifacioneAdapter)
        autotextClasificacion.setOnItemClickListener { _, _, position, _ ->
            autotextClasificacion.setText((autotextClasificacion.adapter.getItem(position) as Clasificaciones).clasosDenom)
            imageDownClasificacion.setImageResource(R.drawable.ic_close)
            isSelecClasifcacion = true
            autotextClasificacion.setSelection(autotextClasificacion.text.length)
        }
        imageDownClasificacion.setOnClickListener{

            if(isSelecClasifcacion)
            {
                autotextClasificacion.text.clear()
                imageDownClasificacion.setImageResource(R.drawable.ic_arrow_drop)
                isSelecClasifcacion = false
            }else
            {
                val im = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                im.hideSoftInputFromWindow(imageDownClasificacion.windowToken, 0)
                autotextClasificacion.showDropDown()}
        }
    }
    private fun llenarAdapterEjecutor() {

        ejecutorAdapter = AutoCompleEjecAdapter(applicationContext,R.layout.spinner_filter,listaEjecutores!!)
        autotextEjecutor.setAdapter(ejecutorAdapter)
        autotextEjecutor.setOnItemClickListener { _, _, position, _ ->
            autotextEjecutor.setText((autotextEjecutor.adapter.getItem(position) as Ejecutor).codigo)
            imageDownEjecutor.setImageResource(R.drawable.ic_close)
            isSelecEjecutor = true
            autotextEjecutor.setSelection(autotextEjecutor.text.length)

            imageDownEjecutor.setOnClickListener{

                if(isSelecEjecutor)
                {
                    autotextEjecutor.text.clear()
                    imageDownEjecutor.setImageResource(R.drawable.ic_arrow_drop)
                    isSelecEjecutor = false
                }else
                {
                    val im = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    im.hideSoftInputFromWindow(imageDownEjecutor.windowToken, 0)
                    autotextEjecutor.showDropDown()}
            }

        }

    }
    private fun llenarAdapterClientes() {

        clienteAdapter = AutoCompleClienteAdapter(applicationContext,R.layout.spinner_filter,listaClientes!!)
        autotextCliente.setAdapter(clienteAdapter)
        autotextCliente.setOnItemClickListener { _, _, position, _ ->
            autotextCliente.setText((autotextCliente.adapter.getItem(position) as Cliente).codigo)
            imageDownCliente.setImageResource(R.drawable.ic_close)
            isSelecCliente = true
            autotextCliente.setSelection(autotextCliente.text.length)

            imageDownCliente.setOnClickListener{

                if(isSelecCliente)
                {
                    autotextCliente.text.clear()
                    imageDownCliente.setImageResource(R.drawable.ic_arrow_drop)
                    isSelecCliente = false
                }else
                {
                    val im = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    im.hideSoftInputFromWindow(imageDownEjecutor.windowToken, 0)
                    autotextCliente.showDropDown()}
            }
        }

    }

    @SuppressLint("InflateParams")
    private fun addItemsLy()
    {
        if(ly_item_objetos.childCount<=5)// solo se pueden agregar 5 objetos no mas
                {
                    val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    layoutParams.setMargins(0, 30, 0, 0)

                    val lyObjeto = LayoutInflater.from(applicationContext).inflate(R.layout.item_objetos_add_solicitud,null)
                    val autotextObjeto = lyObjeto.findViewById<AutoCompleteTextView>(R.id.autotextObjeto)
                    val imageDownObjeto = lyObjeto.findViewById<ImageView>(R.id.imageDownObjeto)
                    val imgClose = lyObjeto.findViewById<ImageView>(R.id.imgClose)


                    autotextObjeto.threshold = 1
                    if (ly_item_objetos.childCount == 0)
                    {
                        imgClose.visibility = View.INVISIBLE
                    }
                    imageDownObjeto.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext,R.color.colorPrimary))
                    imageDownObjeto.setImageResource(R.drawable.ic_arrow_drop)
                    autotextObjeto.setOnItemClickListener { _, _, _, _ ->
                        imageDownObjeto.setImageResource(R.drawable.ic_close)
                    }
                    imgClose.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext,R.color.color_primary_white))
                    imgClose.setImageResource(R.drawable.ic_eliminar)
                    imgClose.setOnClickListener {
                        ly_item_objetos.removeView(lyObjeto)
                        if(ly_item_objetos.childCount < 5)
                        {
                            imgAddItems.visibility = View.VISIBLE
                        }
                    }

                   // ly_objeto.requestFocus()
                    ly_item_objetos.addView(lyObjeto,layoutParams)
                    ly_item_objetos.invalidate()
                    if(ly_item_objetos.childCount == 5)
                    {
                        imgAddItems.visibility = View.INVISIBLE
                    }
                }

    }

    private fun checkClasificacion():Boolean
    {
        listaClasiicaciones?.forEach {
            if(it.clasosDenom.equals(autotextClasificacion.text.toString()))
                return true
        }
        return false
    }
    private fun checkEjecutor():Boolean
    {
        listaEjecutores?.forEach {
            if(it.codigo.equals(autotextEjecutor.text.toString()))
                return true
        }
        return false
    }
    private fun checkCliente():Boolean
    {
        listaClientes?.forEach {
            if(it.codigo.equals(autotextCliente.text.toString()))
                return true
        }
        return false
    }
    private fun MostarTodosLosMensajes(sdtresult: ResponseSdtresult) {
        var mensaje:String?
        if (sdtresult.mensajes != null) {
            mensaje = " \n"
            for (i in sdtresult.mensajes!!.indices) {
                mensaje = """$mensaje${sdtresult.mensajes!![i].description}"""
            }
            ShowError(mensaje, TIPOMENSAJE.ERROR)
        }
    }

    fun ShowError(mensaje: String?, tipomensaje: TIPOMENSAJE?) {
        val layoutInflater = layoutInflater
        BottomSheetMensaje.showMensaje(mensaje, tipomensaje, layoutInflater)
    }



    fun imgOnclickImageSolicitud(view: View)
    {
        when {
            PermisionApp.checkCamera(this) and PermisionApp.checkwriteExternalStoragePermission(this)-> {
                opcionDialogImagen()
            }
            else -> {
                this.requestPermissions(arrayOf(Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION.CAMERA)
             //   ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), PERMISSION.CAMERA)
            }
        }
    }

    private fun opcionDialogImagen()
    {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Imagen")
        val items:Array<CharSequence?> = if (img_bitmap != null) {
            arrayOf("     Tomar Foto","     Seleccionar Foto","     Eliminar Foto")
        }
        else {
            arrayOf("     Tomar Foto","     Seleccionar Foto")
        }




        alertDialog.setItems(items,object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int)
            {
                when (which) {
                    0 -> {
                        TomarFoto()
                    }
                    1 -> {
                        SeleccionarFoto()
                    }
                    else -> {EliminarFoto()}
                }
            }
        })
        alertDialog.show()
    }

    private fun EliminarFoto()
    {
        img_bitmap = null
        img_solicitud.setImageBitmap(null)
        img_solicitud.setImageResource(R.drawable.selector_img_galery)
    }

    private fun SeleccionarFoto()
    {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUESTCODE.SELECT_PHOTO_GALERY)
    }

    private fun TomarFoto()
    {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUESTCODE.TAKE_PHOTO_CAPTURE)
    }


    /////////////////////////OVERRIDE////////////////////
    ////////////////////////////////////////////////////
    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        when (item.itemId)
        {
            android.R.id.home ->
            {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onBackPressed() {
        val data = Intent()
        if (add_solicitud) {
            setResult(Activity.RESULT_OK, data)
        } else {
            setResult(Activity.RESULT_CANCELED, data)
        }
        super.onBackPressed()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode)
        {
            PERMISSION.CAMERA ->
            {
                var cont = 0
                for (grantResult in grantResults) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        cont++
                    }
                }
                if(cont==0)
                {
                    opcionDialogImagen()
                }
                else
                {
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION.CAMERA)
                }
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == REQUESTCODE.TAKE_PHOTO_CAPTURE)
            {
                img_bitmap  = data?.extras?.get("data") as Bitmap
                img_solicitud.setImageBitmap(img_bitmap)
                fileUri = data.data
            }
            if (requestCode == REQUESTCODE.SELECT_PHOTO_GALERY)
            {
                img_bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, data!!.data)
                img_solicitud.setImageBitmap(img_bitmap)
                fileUri = data.data
            }

//
//            Toast.makeText(this,"Entra",Toast.LENGTH_SHORT).show()
//            if (requestCode == REQUESTCODE.TAKE_PHOTO_CAPTURE) {
//                var f = File(Environment.getExternalStorageDirectory().toString())
//                for (temp in f.listFiles()) {
//                    if (temp.name == "temp.jpg") {
//                        f = temp
//                        break
//                    }
//                }
//                try {
//                    val bitmap: Bitmap
//                    val bitmapOptions: BitmapFactory.Options = BitmapFactory.Options()
//                    bitmap = BitmapFactory.decodeFile(f.absolutePath,
//                            bitmapOptions)
//                    img_solicitud.setImageBitmap(bitmap)
//                    val path = (Environment
//                            .getExternalStorageDirectory()
//                            .toString() + File.separator
//                            + "Phoenix" + File.separator + "default")
//                    f.delete()
//                    var outFile: OutputStream? = null
//                    val file = File(path, System.currentTimeMillis().toString() + ".jpg")
//                    try {
//                        outFile = FileOutputStream(file)
//                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile)
//                        outFile.flush()
//                        outFile.close()
//                    } catch (e: FileNotFoundException) {
//                        e.printStackTrace()
//                    } catch (e: IOException) {
//                        e.printStackTrace()
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
//            } else if (requestCode == REQUESTCODE.SELECT_PHOTO_GALERY) {
//                val selectedImage = data!!.data
//                val filePath = arrayOf(MediaStore.Images.Media.DATA)
//                val c: Cursor? = contentResolver.query(selectedImage!!, filePath, null, null, null)
//                c!!.moveToFirst()
//                val columnIndex: Int = c.getColumnIndex(filePath[0])
//                val picturePath: String = c.getString(columnIndex)
//                c.close()
//                val thumbnail: Bitmap = BitmapFactory.decodeFile(picturePath)
//                Log.w("path of image from gallery......******************.........", picturePath + "")
//                img_solicitud.setImageBitmap(thumbnail)
//            }
        }
    }



    private fun enviarSolicitud1()
    {
        showProgresBar()

        if (img_bitmap !=null)
        {
            solicitud.foto = encodeToBase64(bitmap = img_bitmap!!)
        }
        else
        {
            solicitud.foto= ""
        }


        val solicitudRequest = SolicitudRequest()

        solicitudRequest.usuario = Prefs.getString(PREFS.USUARIO, "")
        solicitudRequest.solicitud = solicitud
        solicitudRequest.modeAction = SolicitudRequest.MODEACTION.INS
        //solicitudRequest.imagen = FileUtils.getFile(this,fileUri)

        val callAdicionarSolicitud = RetrofitClient.instance?.api?.adicionarSolicitud(solicitudRequest)
        callAdicionarSolicitud?.enqueue(object: Callback<ResponseWS>{
            override fun onFailure(call: Call<ResponseWS>, t: Throwable)
            {
                ShowError(t.message, TIPOMENSAJE.ERROR)
                showProgresBar()
                add_solicitud = false
            }

            override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>) {
                if(response.isSuccessful)
                {
                    showProgresBar()
                    val responseWS = response.body()

                    if (responseWS!!.sdtresult?.success!!)
                    {
                        add_solicitud = true
                        onBackPressed()
                    }
                    else
                    {
                        responseWS.sdtresult?.let { MostarTodosLosMensajes(it) }
                        add_solicitud = false
                    }
                }
            }
        })
    }

    private fun encodeToBase64(bitmap: Bitmap):String
    {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG,30,byteArrayOutputStream)
        val a = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(a, Base64.DEFAULT)
    }
}