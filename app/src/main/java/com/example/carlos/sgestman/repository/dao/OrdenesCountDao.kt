package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.carlos.sgestman.entity.OrdenesCount

@Dao
interface OrdenesCountDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ordenesCount: OrdenesCount?)

    @get:Query("SELECT * from OrdenesCount")
    val allOrdenesCount: LiveData<OrdenesCount?>?

    @Query("DELETE FROM OrdenesCount")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrdenesCount(ordenesCount: OrdenesCount?)
}