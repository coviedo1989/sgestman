package com.example.carlos.sgestman.actividades.ui.solicitud

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.actividades.BasicActivity
import com.example.carlos.sgestman.databinding.ActivitySolicitudDetailsBinding
import com.example.carlos.sgestman.entity.Permisos
import com.example.carlos.sgestman.model.SolicitudRequest
import com.example.carlos.sgestman.entity.Solicitudes
import com.example.carlos.sgestman.model.ResponseSdtresult
import com.example.carlos.sgestman.model.ResponseWS
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.repository.ws.UnsafeOkHttpClient
import com.example.carlos.sgestman.utils.MiFecha
import com.example.carlos.sgestman.utils.constant.INTENT
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.constant.REQUESTCODE
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.BottomSheetMensaje
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_solicitud_details.*
import kotlinx.android.synthetic.main.dialog_layout.view.*
import kotlinx.android.synthetic.main.setting_api_server.view.btnAceptar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActividadSolicitudDetails : BasicActivity() {
    private var solicitudExtra: Solicitudes = Solicitudes()
    private var del_solicitud = false
    private lateinit var solicitudDetailsBinding: ActivitySolicitudDetailsBinding
    private lateinit var toolbar: Toolbar
    private var isClasificacion = false
    private var isEjecutor = false
    private var permisoSol: Permisos?= null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        solicitudDetailsBinding = DataBindingUtil.setContentView(this,R.layout.activity_solicitud_details)
        solicitudDetailsBinding.solicitud = intent.extras?.get(INTENT.EXTRA_SOLICITUD) as Solicitudes
        solicitudDetailsBinding.fecha = MiFecha()
        setupVariables()
        setupView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(INTENT.EXTRA_SOLICITUD,solicitudExtra)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle)
    {
        super.onRestoreInstanceState(savedInstanceState)
        solicitudExtra = savedInstanceState.getSerializable(INTENT.EXTRA_SOLICITUD) as Solicitudes
    }
    private fun setupVariables()
    {
    }

    private fun setupView()
    {
        permisosSolicitudes()
        setTollbar()
        SetupPrefs()
        ActualizarSolicitud()
    }
    private fun permisosSolicitudes()
    {
        permisoSol = intent.extras?.get(INTENT.EXTRA_PERMISOS) as Permisos
        if (permisoSol==null)
        {
            permisoSol = Permisos()
        }
    }

    private fun setupObjetoSolicitud()
    {
        var i = 0
        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(0, 30, 0, 0)
        solicitudDetailsBinding.solicitud?.Objetos?.forEach {
            i++
            val lyObjeto = LayoutInflater.from(applicationContext).inflate(R.layout.item_objetos_details_solicitud,null)
            val txtObjetoCod = lyObjeto.findViewById<TextView>(R.id.txtObjetoCod)
            txtObjetoCod.text = it.objpatCodigo
            if (i!= solicitudDetailsBinding.solicitud?.Objetos?.size)
            {
                txtObjetoCod.setBackgroundResource(R.drawable.line_bottom)
            }
            if (i==1)
            {
                ly_objetos.addView(lyObjeto)
            }
            else
            {
                ly_objetos.addView(lyObjeto,layoutParams)
            }
        }


    }

    private fun estadoSolicitud()
    {
        when(solicitudDetailsBinding.solicitud?.estado)
        {
            1-> {
                solicitudDetailsBinding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(applicationContext,R.color.chart_SinAtender)
                solicitudDetailsBinding.estado.text = String.format("Sin Atender")
            }
            2-> {
                solicitudDetailsBinding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(applicationContext,R.color.chart_Atendidas)
                solicitudDetailsBinding.estado.text = String.format("Atendida")
                imgAtender.visibility = View.INVISIBLE
                imgEditar.visibility = View.INVISIBLE
                imgEliminar.visibility = View.INVISIBLE
            }
            3-> {
                solicitudDetailsBinding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(applicationContext,R.color.chart_Iniciada)
                solicitudDetailsBinding.estado.text = String.format("Iniciada")
                imgAtender.visibility = View.INVISIBLE
                imgEditar.visibility = View.INVISIBLE
                imgEliminar.visibility = View.INVISIBLE
            }
            4-> {
                solicitudDetailsBinding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(applicationContext,R.color.chart_Pendientes)
                solicitudDetailsBinding.estado.text = String.format("Pendiente")
                imgAtender.visibility = View.INVISIBLE
                imgEditar.visibility = View.INVISIBLE
                imgEliminar.visibility = View.INVISIBLE
            }
            5-> {
                solicitudDetailsBinding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(applicationContext,R.color.chart_Terminadas)
                solicitudDetailsBinding.estado.text = String.format("Terminada")
                imgAtender.visibility = View.INVISIBLE
                imgEditar.visibility = View.INVISIBLE
                imgEliminar.visibility = View.INVISIBLE
            }
            6->
            {
                solicitudDetailsBinding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(applicationContext,R.color.chart_Cerradas)
                solicitudDetailsBinding.estado.text = String.format("Cerrada")
                imgAtender.visibility = View.INVISIBLE
                imgEditar.visibility = View.INVISIBLE
                imgEliminar.visibility = View.INVISIBLE
            }
            7->
            {
                solicitudDetailsBinding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(applicationContext,R.color.chart_Cerradas)
                solicitudDetailsBinding.estado.text = String.format("Cancelada")
                imgAtender.visibility = View.INVISIBLE
                imgEditar.visibility = View.INVISIBLE
                imgEliminar.visibility = View.INVISIBLE
            }
        }
    }

    private fun SetupPrefs() {

        isEjecutor = Prefs.getBoolean(PREFS.CONFIGURACION_SOL_IS_EJECUTOR,true)
        isClasificacion = Prefs.getBoolean(PREFS.CONFIGURACION_SOL_IS_CLASIFICACION,true)
    }


    @SuppressLint("SetTextI18n")
    fun setTollbar()
    {
        toolbar = findViewById<View>(R.id.my_awesome_toolbar) as Toolbar
        toolbar.title = ""
        toolbar.navigationIcon= ContextCompat.getDrawable(this,R.drawable.ic_left_arrow)
        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        val mtitleToolbar = toolbar.findViewById(R.id.toolbar_title) as TextView
        mtitleToolbar.text = solicitudDetailsBinding.solicitud?.codigo?.trim()
    }

    private fun eliminarSolicitud()
    {
        showProgresBar()
        val solicitudRequest = SolicitudRequest()
        solicitudRequest.modeAction = SolicitudRequest.MODEACTION.DEL
        solicitudRequest.solicitud = Solicitudes()
        solicitudRequest.usuario = Prefs.getString(PREFS.USUARIO, "")//.toUpperCase(Locale.ROOT)
        //solicitudRequest.usuario = "GAMMA"
        solicitudRequest.solicitud!!.codigo = solicitudDetailsBinding.solicitud?.codigo//?.trim()
        val retrofitClient = RetrofitClient.instance?.api?.eliminarSolicitud(solicitudRequest)
        retrofitClient!!.enqueue(object:Callback<ResponseWS>{
            override fun onFailure(call: Call<ResponseWS>, t: Throwable)
            {
                showProgresBar()
                showError(t.localizedMessage!!,TIPOMENSAJE.ERROR)
                del_solicitud = false
            }

            override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>) {
                showProgresBar()
                val responseWS = response.body()
                if (responseWS!!.sdtresult?.success!!)
                {
                    del_solicitud = true
                    onBackPressed()
                }
                else
                {
                    del_solicitud = false
                    responseWS.sdtresult?.let { MostarTodosLosMensajes(it) }
                }
            }
        })
    }

    private fun editarSolicitud()
    {
        val intent = Intent(this,ActividadEditarSolicitud::class.java)
        intent.putExtra(INTENT.EXTRA_SOLICITUD,solicitudDetailsBinding.solicitud)
        startActivityForResult(intent,REQUESTCODE.ACTIVIDAD_EDIT_SOL)

    }

    private fun atenderSolicitud() {


    }
    fun showError(mensaje: String?, tipomensaje: TIPOMENSAJE?) {
        val layoutInflater = layoutInflater
        BottomSheetMensaje.showMensaje(mensaje, tipomensaje, layoutInflater)
    }
    private fun MostarTodosLosMensajes(sdtresult: ResponseSdtresult) {
        var mensaje:String?
        if (sdtresult.mensajes != null) {
            mensaje = " \n"
            for (i in sdtresult.mensajes!!.indices) {
                mensaje = """$mensaje${sdtresult.mensajes!![i].description}"""
            }
            showError(mensaje, TIPOMENSAJE.ERROR)
        }
    }



    //////////////////////////////OVERRRIDE//////////////////////
    ///////////////////////////////////////////////////////////
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_details_solicitudes, menu)

        val menuEditar = menu!!.findItem(R.id.menu_edit)
        val menuEliminar = menu.findItem(R.id.menu_delete)
        if(permisoSol!!.up == 0)
        {
            menuEditar.isVisible = false
        }
        if(permisoSol!!.del == 0)
        {
            menuEliminar.isVisible = false
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == REQUESTCODE.ACTIVIDAD_EDIT_SOL)
            {
                solicitudExtra = data!!.extras?.get(INTENT.EXTRA_SOLICITUD) as Solicitudes
                solicitudDetailsBinding.solicitud = solicitudExtra
                ActualizarSolicitud()

            }
        }
    }

    private fun ActualizarSolicitud()
    {
        setupImagen()
        estadoSolicitud()
        setupObjetoSolicitud()
    }

    private fun setupImagen() {

        val url = solicitudDetailsBinding.solicitud?.foto
       // url = url!!.replace("https","http")
        val client = UnsafeOkHttpClient.getUnsafeOkHttpClient()
        //Glide.get(this).registry.replace(GlideUrl::class.java, InputStream::class.java, OkHttpUrlLoader.Factory(client))
        Glide.with(this)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.gallery_gray)
                .listener(object :RequestListener<Drawable>{
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        return false
                    }
                })
                .into(img_solicitud)


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.menu_edit ->
            {
                editarSolicitud()
                return true
            }
            R.id.menu_delete ->
            {
                eliminarSolicitud()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onBackPressed() {
        val data = Intent()
        if (del_solicitud)
        {
            data.putExtra(INTENT.EXTRA_SOLICITUD,solicitudExtra)
            setResult(Activity.RESULT_OK, data)
        } else {
            setResult(Activity.RESULT_CANCELED, data)
        }
        super.onBackPressed()
    }

    fun btnEliminar(view: View)
    {
        if (solicitudDetailsBinding.solicitud?.estado!! <= 1)
        {
            eliminarSolicitud()
        }
        else
        {
            Mensaje("No se puede eliminar una solicitud con estado: ${solicitudDetailsBinding.solicitud?.getEstado()}")
        }

    }

    fun bntAtender(view: View) {
        atenderSolicitud()
    }
    fun bntEditar(view: View)
    {
        if (solicitudDetailsBinding.solicitud?.estado!! <= 1)
        {
            editarSolicitud()
        }
        else
        {
            Mensaje("No se puede editar una solicitud con estado: ${solicitudDetailsBinding.solicitud?.getEstado()}")
        }
    }


    private fun Mensaje(mensaje: String) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null)
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        mDialogView.texto.text = mensaje
        mDialogView.btnAceptar.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    fun onClickImagenActivity(view: View)
    {
        Toast.makeText(this,"Abrir Actividad en formato Grande",Toast.LENGTH_SHORT).show()
    }


}