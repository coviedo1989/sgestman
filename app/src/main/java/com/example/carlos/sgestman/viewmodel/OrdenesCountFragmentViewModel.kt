package com.example.carlos.sgestman.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carlos.sgestman.entity.OrdenesCount
import com.example.carlos.sgestman.repository.Databases.OrdenesCountRoomDBRepository
import com.example.carlos.sgestman.repository.ws.WebServiceRepository

class OrdenesCountFragmentViewModel(application: Application) : AndroidViewModel(application)
{
    private val ordenesCountRoomDBRepository:OrdenesCountRoomDBRepository = OrdenesCountRoomDBRepository (application)
    private val webServiceRepository:WebServiceRepository= WebServiceRepository(application)

    private var _ordenesCount = MutableLiveData<OrdenesCount>()
    val ordenesCount : LiveData<OrdenesCount?>?

    init {
        _ordenesCount = webServiceRepository.providesDashOrdenesCountWebService()
        ordenesCount = ordenesCountRoomDBRepository.allOrdenesCount
    }
}