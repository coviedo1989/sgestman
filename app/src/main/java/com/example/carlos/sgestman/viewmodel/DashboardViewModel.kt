package com.example.carlos.sgestman.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carlos.sgestman.entity.CostosMtto
import com.example.carlos.sgestman.entity.DashBoard
import com.example.carlos.sgestman.repository.Databases.OrdenesCountRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.SolicitudesCountRoomDBRepository
import com.example.carlos.sgestman.repository.ws.WebServiceRepository
import com.example.carlos.sgestman.entity.OrdenesCount
import com.example.carlos.sgestman.entity.SolicitudesCount
import com.example.carlos.sgestman.repository.Databases.CostosMttoRoomDBRepository

class DashboardViewModel(application: Application) : AndroidViewModel(application) {
    private var mallCostosMtto: LiveData<CostosMtto?>?
    private var roomDBRepositoryCostosMtto: CostosMttoRoomDBRepository
    private var webServiceRepository: WebServiceRepository
    private var roomDBRepositorySolicitudes: SolicitudesCountRoomDBRepository
    private val roomDBRepositoryOrdenes: OrdenesCountRoomDBRepository
    private var mAllSolicitudesCOunt: LiveData<SolicitudesCount?>?
    private var mAllOrdenesCOunt: LiveData<OrdenesCount?>?
//    private var obserSolicitudesCount: LiveData<SolicitudesCount>
//    private val obserOrdenesCount: LiveData<OrdenesCount>

    private var _sdtDashboard = MutableLiveData<DashBoard>()
    val sdtDashboard : LiveData<DashBoard> get() = _sdtDashboard

    //
    //    public LiveData<List<Country>> getmAllCountrys() {
    //        return mAllCountrys;
    //    }


    fun cargarViewModelAgain()
    {
        roomDBRepositorySolicitudes = SolicitudesCountRoomDBRepository(getApplication())
        webServiceRepository = WebServiceRepository(getApplication())
        _sdtDashboard = webServiceRepository.providesDashBoard()
        webServiceRepository.providesDashBoard()
        mAllOrdenesCOunt = roomDBRepositoryOrdenes.allOrdenesCount
        mAllSolicitudesCOunt = roomDBRepositorySolicitudes.allSolicitudesCount
        mallCostosMtto = roomDBRepositoryCostosMtto.allcostosMtto
    }

    fun getmAllSolicitudesCOunt(): LiveData<SolicitudesCount?>? {
        return mAllSolicitudesCOunt
    }

    fun getmAllOrdenesCOunt(): LiveData<OrdenesCount?>? {
        return mAllOrdenesCOunt
    }
    fun getmAllCostosMtto():LiveData<CostosMtto?>?
    {
        return mallCostosMtto
    }

    init {
        webServiceRepository = WebServiceRepository(application)

        _sdtDashboard = webServiceRepository.providesDashBoard()
        //obserSolicitudesCount = webServiceRepository.providesDashSolicitudesWebService()

        roomDBRepositorySolicitudes = SolicitudesCountRoomDBRepository(application)
        roomDBRepositoryOrdenes = OrdenesCountRoomDBRepository(application)
        roomDBRepositoryCostosMtto = CostosMttoRoomDBRepository(application)

        mAllSolicitudesCOunt = roomDBRepositorySolicitudes.allSolicitudesCount
        mAllOrdenesCOunt = roomDBRepositoryOrdenes.allOrdenesCount
        mallCostosMtto = roomDBRepositoryCostosMtto.allcostosMtto
    }
}