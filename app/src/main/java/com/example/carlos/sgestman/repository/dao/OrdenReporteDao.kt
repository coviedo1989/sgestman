package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.carlos.sgestman.entity.OrdenReporte

@Dao
interface OrdenReporteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ordenReporte: OrdenReporte?)

    @get:Transaction
    @get:Query("SELECT * from OrdenReporte")
    val allOrdenReporte: LiveData<List<OrdenReporte>?>?

    @Query("DELETE FROM OrdenReporte where numeroOT = :numeroOT")
    fun deleteAll(numeroOT: String)

    @Delete
    fun delete(ordenReporte: OrdenReporte?)

    @Update
    fun update(ordenReporte: OrdenReporte?)

    @Query("Update OrdenReporte set estadoOT = :estadoOT where numeroOT = :numeroOT")
    fun updateEstado(numeroOT: String,estadoOT:Int)

    @Query("Select * from OrdenReporte where numeroOT = :numeroOT")
    fun selectByNumeroOT(numeroOT:String):List<OrdenReporte>
}