package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.entity.Permisos
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.repository.dao.PermisosDao

class PermisosRoomDBRepository(application: Application?) {
    private val permisosInfoDao: PermisosDao?
    var allPermisos: LiveData<List<Permisos>?>?

    fun insertPermisos(resultModel: Permisos?) {
        InsertAsyncTask(permisosInfoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: PermisosDao?) : AsyncTask<Permisos?, Void?, Void?>() {
        override fun doInBackground(vararg globals: Permisos?): Void? {
            mAsyncTaskDao!!.insertGlobal(globals[0])
            return null
        }

    }

    init {
        val db = getDatabase(application!!)
        permisosInfoDao = db!!.permisosInfoDao()
        allPermisos = permisosInfoDao!!.allGlobal
    }
}