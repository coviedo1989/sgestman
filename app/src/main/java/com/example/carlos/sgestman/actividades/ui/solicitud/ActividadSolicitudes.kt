package com.example.carlos.sgestman.actividades.ui.solicitud

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.actividades.BasicActivity
import com.example.carlos.sgestman.adapter.CustomLoadStateAdapter
import com.example.carlos.sgestman.adapter.SolcitudAdpaterListener
import com.example.carlos.sgestman.adapter.SolicitudesAdapter
import com.example.carlos.sgestman.entity.Permisos
import com.example.carlos.sgestman.entity.Solicitudes
import com.example.carlos.sgestman.model.ResponseWS
import com.example.carlos.sgestman.model.SolicitudRequest
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.utils.HandleErrorApi
import com.example.carlos.sgestman.utils.constant.INTENT
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.constant.REQUESTCODE
import com.example.carlos.sgestman.utils.enums.EnumSolicitudes
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.viewmodel.SolicitudesViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_solicitudes.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ActividadSolicitudes : BasicActivity(), SolcitudAdpaterListener
{
    private var positionItem: Int?=null
    private var isDelSolicitud: Boolean? = null
    private var permisoSol: Permisos? = null
    private var iscargo = false
    private var isbyFilter: Boolean? = null
    private lateinit var toolbar: Toolbar
    private var estadoInt = 0
    //private val viewModelRecent by viewModels<SolicitudesViewModel1>()
    lateinit var viewModelRecent: SolicitudesViewModel
    private var adapter1 : SolicitudesAdapter? =null
    private var solicitud: Solicitudes?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_solicitudes)
        init()
        setupView()


    }
    private fun init()
    {
        permisosSolicitudes()
        viewModelRecent = SolicitudesViewModel()
        isbyFilter = Prefs.getBoolean(PREFS.FILTER_SOL_ISFILTER,false)
        //viewModelRecent = ViewModelProvider(this)[SolicitudesViewModel1::class.java]

    }

    private fun permisosSolicitudes()
    {
        permisoSol = intent.extras?.get(INTENT.EXTRA_PERMISOS) as Permisos
        if (permisoSol==null)
        {
            permisoSol = Permisos()
        }
    }

    private fun setupView()
    {
        setTollbar()
        adapter1 = SolicitudesAdapter(applicationContext,this)
        reciclerview.layoutManager = (LinearLayoutManager(this))
        reciclerview.adapter =  adapter1?.withLoadStateFooter(footer = CustomLoadStateAdapter(adapter1!!::retry,applicationContext))

        adapter1!!.addLoadStateListener { loadState ->

//            Log.e("raya" , "---------------------")
//            Log.e("loadState.append" , loadState.append.endOfPaginationReached.toString())
//            Log.e("loadState.refresh" , loadState.refresh.endOfPaginationReached.toString())
//            Log.e("loadState.source.refresh ", loadState.source.refresh.endOfPaginationReached.toString())
//            Log.e("is Refresh ", (loadState.source.refresh is LoadState.NotLoading).toString())

            isbyFilter = Prefs.getBoolean(PREFS.FILTER_SOL_ISFILTER,false)
            if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached)
            {
                when {
                    adapter1!!.itemCount < 1 -> {
                        showStateEmpty(isbyFilter!!)
                        iscargo = false
                    }
                }
            }
            else if (adapter1!!.itemCount > 1 && !iscargo)
            {
                iscargo = true
                showStateEmpty(isbyFilter!!)
            }

        }
        reciclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) // Puedes ocultarlo simplemente
                    //fab.hide();
                    // o añadir la animación deseada
                        floatingActionButton.animate().translationY(floatingActionButton.height +
                                resources.getDimension(R.dimen.fab_margin))
                               // .setInterpolator(LinearInterpolator()).duration = 100
                    else if (dy < 0) //fab.show();
                        floatingActionButton.animate().translationY(0F)
                                //.setInterpolator(LinearInterpolator()).duration = 100
                }
            })


        floatingActionButton.setOnClickListener { addSolicitud() }
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary)
        swipeRefreshLayout.setOnRefreshListener {
            viewModelRecent = SolicitudesViewModel()
            getSolicitudes()
        }


        ///permisos de Adicionar Solicitude
        if (permisoSol!!.add==0)
        {
            floatingActionButton.visibility = View.GONE
        }

        getSolicitudes()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle)
    {
        super.onRestoreInstanceState(savedInstanceState)
        permisoSol = savedInstanceState.get(INTENT.EXTRA_PERMISOS) as Permisos
    }
    override fun onSaveInstanceState(outState: Bundle)
    {
        outState.putSerializable(INTENT.EXTRA_PERMISOS,permisoSol)
        super.onSaveInstanceState(outState)

    }

    @SuppressLint("SetTextI18n")
    fun setTollbar()
    {
        toolbar = findViewById<View>(R.id.my_awesome_toolbar) as Toolbar
        toolbar.title = ""
        toolbar.navigationIcon= ContextCompat.getDrawable(this,R.drawable.ic_left_arrow)
        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        val mtitleToolbar = toolbar.findViewById(R.id.toolbar_title) as TextView
        mtitleToolbar.text = "Solicitudes"
    }


    private suspend fun showOnRecyclerView1(t: PagingData<Solicitudes>)
    {
        adapter1!!.submitData(t)


      //  reciclerview.adapter = adapter1?.withLoadStateFooter(footer = CustomLoadStateAdapter(adapter1!!::retry))

        if(swipeRefreshLayout.isRefreshing)
        {
            swipeRefreshLayout.isRefreshing = false
        }

        //showStateEmpty(isbyFilter!!)

    }

    private fun getSolicitudes()
    {
        val call1 = RetrofitClient.instance?.api?.TestConexion()
        call1?.enqueue(object : retrofit2.Callback<String?>
        {
            override fun onFailure(call: Call<String?>, t: Throwable) {
                showErrorConection(true,t)
            }

            override fun onResponse(call: Call<String?>, response: Response<String?>)
            {
                showErrorConection(false,null)
                lifecycleScope.launch{
                    //@OptIn(ExperimentalCoroutinesApi::class)
                    viewModelRecent.itemsSolicitudes.collectLatest {
                        pagingData ->
                        //showStateEmpty(isbyFilter!!)
                        showOnRecyclerView1(pagingData)

                    }
                }
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_filter, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        val id = item.itemId

        if (id == R.id.menu_filter)
        {
            showFilter()
        }
        if (id == R.id.home)
        {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun addSolicitud()
    {
        val intent = Intent(this, ActividadCrearSolicitud::class.java)
        startActivityForResult(intent, REQUESTCODE.ACTIVIDAD_ADD_SOL)
    }

    @SuppressLint("InflateParams")
    private fun showFilter()
    {

        val mBottomSheetDialog = BottomSheetDialog(this, R.style.BaseBottomSheetDialog_BottomSheetDialogThemeRound)
        val sheetView = layoutInflater.inflate(R.layout.filter_solicitudes, null)

        mBottomSheetDialog.setContentView(sheetView)
        val mBehavior = BottomSheetBehavior.from(sheetView.parent as View)
        mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        mBottomSheetDialog.setCancelable(true)
        mBottomSheetDialog.show()

        val btnAplicar = sheetView.findViewById<Button>(R.id.btnAplicar)
        val btnLimpiar = sheetView.findViewById<Button>(R.id.btnLimpiar)

        val codigoSolicitud = sheetView.findViewById<TextView>(R.id.codigo_solicitud)
        val codigoCliente = sheetView.findViewById<TextView>(R.id.codigoCliente)
        val codigoEjecutor = sheetView.findViewById<TextView>(R.id.codigoEjecutor)
        val codigoObjeto = sheetView.findViewById<TextView>(R.id.codigoObjeto)
        val estadoSol = sheetView.findViewById<ChipGroup>(R.id.estadoSol)
        val fechaInicio = sheetView.findViewById<EditText>(R.id.fechaInicio)
        val fechaFin = sheetView.findViewById<EditText>(R.id.fechaFin)


        onclickImageDateFechaView(sheetView)

        estadoSol.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.chipSintander -> {
                    Prefs.putInt(PREFS.FILTER_SOL_ESTADO, EnumSolicitudes.SINATENDER.estate)
                    estadoInt = EnumSolicitudes.SINATENDER.estate
                }
                R.id.chipAtendida -> {
                    Prefs.putInt(PREFS.FILTER_SOL_ESTADO, EnumSolicitudes.ATENDIDA.estate)
                    estadoInt = EnumSolicitudes.ATENDIDA.estate
                }
                R.id.chipIniciada -> {
                    Prefs.putInt(PREFS.FILTER_SOL_ESTADO, EnumSolicitudes.INICIADA.estate)
                    estadoInt = EnumSolicitudes.INICIADA.estate
                }
                R.id.chipPendiente -> {
                    Prefs.putInt(PREFS.FILTER_SOL_ESTADO, EnumSolicitudes.PENDIENTE.estate)
                    estadoInt = EnumSolicitudes.PENDIENTE.estate
                }
                R.id.chipTerminada -> {
                    Prefs.putInt(PREFS.FILTER_SOL_ESTADO, EnumSolicitudes.TERMINADA.estate)
                    estadoInt = EnumSolicitudes.TERMINADA.estate
                }
                R.id.chipCerrada -> {
                    Prefs.putInt(PREFS.FILTER_SOL_ESTADO, EnumSolicitudes.CERRADA.estate)
                    estadoInt = EnumSolicitudes.CERRADA.estate
                }
                R.id.chipCancelada -> {
                    Prefs.putInt(PREFS.FILTER_SOL_ESTADO, EnumSolicitudes.CANCELADA.estate)
                    estadoInt = EnumSolicitudes.CANCELADA.estate
                }
            }

        }


        addFormato(fechaInicio,fechaFin)

        ///
        llenarFiltros(sheetView)

        btnAplicar.setOnClickListener {
            filtros(codigoSolicitud.text.toString(),
                    codigoCliente.text.toString(),
                    codigoEjecutor.text.toString(),
                    codigoObjeto.text.toString(),
                    estadoInt,
                    fechaInicio.text.toString(),
                    fechaFin.text.toString())
            mBottomSheetDialog.dismiss()
            getSolicitudes()
        }
        btnLimpiar.setOnClickListener {
            limpiarFiltros()
            mBottomSheetDialog.dismiss()
            getSolicitudes()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun onclickImageDateFechaView(sheetView: View?)
    {

        val fechaInicio = sheetView?.findViewById<EditText>(R.id.fechaInicio)
        val fechaFin = sheetView?.findViewById<EditText>(R.id.fechaFin)
        val fechaInicioImg = sheetView?.findViewById<ImageView>(R.id.fechaInicioImg)
        val fechaFinImg = sheetView?.findViewById<ImageView>(R.id.fechaFinImg)


        fechaInicioImg?.setOnClickListener {

            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->

                        val dia = if (dayOfMonth < 10) {"0$dayOfMonth"}else {"$dayOfMonth"}
                        val mes = if (month < 10) {"0$month"}else {"$month"}
                        fechaInicio?.setText("$dia/$mes/$year")
                    }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }

        fechaFinImg?.setOnClickListener {

            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                        val dia = if (dayOfMonth < 10) {"0$dayOfMonth"}else {"$dayOfMonth"}
                        val mes = if (month < 10) {"0$month"}else {"$month"}
                        fechaFin?.setText("$dia/$mes/$year")
                    }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }
    }

    private fun addFormato(fechaInicio:EditText, fechaFin:EditText)
    {
        fechaInicio.addTextChangedListener(object:TextWatcher
        {
            var sb : StringBuilder = StringBuilder("")

            var isIgnore = false

            override fun afterTextChanged(s: Editable?)
            {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
            {

                if(isIgnore){
                    isIgnore = false
                    return
                }

                sb.clear()
                sb.append(if(s!!.length > 10){ s.subSequence(0,10) }else{ s })

                when (sb.lastIndex) {
                    2 -> {
                        when {
                            sb[2] != '/' -> {
                                val sb1 = sb.toString().substring(1..2)
                                if(sb1.toInt() in 1..31) {
                                    sb.insert(2,"/")
                                } else {
                                    sb.clear()
                                }
                            }
                        }

                    }
                    5 -> {
                        when {
                            sb[5] != '/' -> {
                                val sb1 = sb.toString().substring(3..4)
                                if (sb1.toInt() in 2..12) {
                                    sb.insert(5,"/")
                                } else {
                                    sb.clear()
                                }
                            }
                        }

                    }
                    9 -> {
                        val sb1 = sb.toString().substring(6,10)
                        when {
                            sb1.toInt() < 2015 || sb1.toInt() > 2030 -> {
                                sb.clear()
                            }
                        }
                    }
                }

                isIgnore = true
                fechaInicio.setText(sb.toString())
                fechaInicio.setSelection(sb.length)
            }
        })
        fechaFin.addTextChangedListener(object:TextWatcher
        {
            var sb : StringBuilder = StringBuilder("")

            var isIgnore = false

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
            {

                if(isIgnore){
                    isIgnore = false
                    return
                }

                sb.clear()
                sb.append(if(s!!.length > 10){ s.subSequence(0,10) }else{ s })

                when (sb.lastIndex) {
                    2 -> {
                        when {
                            sb[2] != '/' -> {
                                val sb1 = sb.toString().substring(1..2)
                                if(sb1.toInt() in 1..31) {
                                    sb.insert(2,"/")
                                } else {
                                    sb.clear()
                                }
                            }
                        }

                    }
                    5 -> {
                        when {
                            sb[5] != '/' -> {
                                val sb1 = sb.toString().substring(3..4)
                                if (sb1.toInt() in 2..12) {
                                    sb.insert(5,"/")
                                } else {
                                    sb.clear()
                                }
                            }
                        }

                    }
                    9 -> {
                        val sb1 = sb.toString().substring(6,10)
                        when {
                            sb1.toInt() < 2015 || sb1.toInt() > 2030 -> {
                                sb.clear()
                            }
                        }
                    }
                }

                isIgnore = true
                fechaFin.setText(sb.toString())
                fechaFin.setSelection(sb.length)
            }
        })

        fechaInicio.setOnFocusChangeListener { view, focus -> onFocusEditext(focus,view) }
        fechaFin.setOnFocusChangeListener { view, focus -> onFocusEditext(focus,view) }
    }

    private fun onFocusEditext(b: Boolean,view:View)
    {
        view as TextView
        when {
            !b && view.text.length< 10-> {
                view.text = ""
            }
        }
    }

    private fun llenarFiltros(sheetView: View)
    {
        val codigoSolicitud = sheetView.findViewById<TextView>(R.id.codigo_solicitud)
        val codigoCliente = sheetView.findViewById<TextView>(R.id.codigoCliente)
        val codigoEjecutor = sheetView.findViewById<TextView>(R.id.codigoEjecutor)
        val codigoObjeto = sheetView.findViewById<TextView>(R.id.codigoObjeto)
        val fechaInicio = sheetView.findViewById<TextView>(R.id.fechaInicio)
        val fechaFin = sheetView.findViewById<TextView>(R.id.fechaFin)
        val estadoSol = sheetView.findViewById<ChipGroup>(R.id.estadoSol)

        codigoSolicitud.text = Prefs.getString(PREFS.FILTER_SOL_CODIGO,"")
        codigoCliente.text = Prefs.getString(PREFS.FILTER_SOL_CLIENTE,"")
        codigoEjecutor.text = Prefs.getString(PREFS.FILTER_SOL_EJECUTOR,"")
        codigoObjeto.text = Prefs.getString(PREFS.FILTER_SOL_OBJETO,"")
        fechaInicio.text = Prefs.getString(PREFS.FILTER_SOL_FECHAINICIO,"")
        fechaFin.text = Prefs.getString(PREFS.FILTER_SOL_FECHAFIN,"")
        val estadoint = Prefs.getInt(PREFS.FILTER_SOL_ESTADO,0)

        when {
            estadoint != 0 -> {
                val chip = estadoSol.getChildAt(estadoint-1) as Chip
                chip.isChecked = true
            }
        }


    }


    private fun filtros(codigo:String, codigoCliente:String, codigoEjecutor:String, codigoObjeto:String, estado:Int, fechaInicio:String, fechaFin:String)
    {
        Prefs.putString(PREFS.FILTER_SOL_CODIGO, codigo)
        Prefs.putString(PREFS.FILTER_SOL_CLIENTE, codigoCliente)
        Prefs.putString(PREFS.FILTER_SOL_EJECUTOR, codigoEjecutor)
        Prefs.putString(PREFS.FILTER_SOL_OBJETO, codigoObjeto)
        Prefs.putString(PREFS.FILTER_SOL_FECHAINICIO, fechaInicio)
        Prefs.putString(PREFS.FILTER_SOL_FECHAFIN, fechaFin)
        Prefs.putInt(PREFS.FILTER_SOL_ESTADO,estado)
        Prefs.putBoolean(PREFS.FILTER_SOL_ISFILTER,true)
        viewModelRecent = SolicitudesViewModel()
    }
    private fun limpiarFiltros()
    {
        Prefs.putString(PREFS.FILTER_SOL_CODIGO,"")
        Prefs.putString(PREFS.FILTER_SOL_CLIENTE,"")
        Prefs.putString(PREFS.FILTER_SOL_EJECUTOR,"")
        Prefs.putString(PREFS.FILTER_SOL_OBJETO,"")
        Prefs.putString(PREFS.FILTER_SOL_FECHAINICIO,"")
        Prefs.putString(PREFS.FILTER_SOL_FECHAFIN,"")
        Prefs.putInt(PREFS.FILTER_SOL_ESTADO,0)
        Prefs.putBoolean(PREFS.FILTER_SOL_ISFILTER,false)
        viewModelRecent = SolicitudesViewModel()
    }
    private fun showErrorConection(isError:Boolean, t: Throwable?)
    {
        if(swipeRefreshLayout.isRefreshing)
        {
            swipeRefreshLayout.isRefreshing = false
        }
        when {
            isError -> {
                ly_estate.visibility = View.VISIBLE
                reciclerview.visibility = View.GONE
                floatingActionButton.visibility = View.GONE
                textEmptySate.text = HandleErrorApi(applicationContext,t!!).mensaje()
                imageState.setImageResource(R.drawable.ic_wifi_off)
                buttonCrearEmpty.visibility = View.VISIBLE
                buttonCrearEmpty.text = getString(R.string.reintentar)
                textTitle.text = getString(R.string.error)
                imageState.imageTintList = ContextCompat.getColorStateList(this,R.color.colorPrimary)

                buttonCrearEmpty.setOnClickListener { getSolicitudes() }
            }
            else -> {
                ly_estate.visibility = View.GONE
                reciclerview.visibility = View.VISIBLE
                floatingActionButton.visibility = View.VISIBLE
                buttonCrearEmpty.setOnClickListener {  }
            }
        }

    }

    private fun showStateEmpty(isbyFiltro:Boolean)
    {

        when {
            isbyFiltro ->
            {

                ly_estate.setOnClickListener{showFilter()}
                textTitle.setText(R.string.emptystatesolicitudes)
                textEmptySate.setText(R.string.emptystateDescFilter)
                imageState.setImageResource(R.drawable.ic_empty_state_filter)
                imageState.imageTintList = null
                buttonCrearEmpty.visibility = View.GONE
                if(reciclerview.adapter?.itemCount!! <= 1) {
                    ly_estate.visibility = View.VISIBLE
                    reciclerview.visibility = View.GONE
                    val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_out_in)
                    imageState.startAnimation(animation)
                } else {
                    ly_estate.visibility = View.GONE
                    reciclerview.visibility = View.VISIBLE
                }
            }
            else -> {

                        ly_estate.setOnClickListener{}
                        textEmptySate.setText(R.string.emptystatesolicitudesDesc)
                        imageState.setImageResource(R.drawable.ic_solicitudtrabajo)
                        buttonCrearEmpty.visibility = View.VISIBLE
                        imageState.imageTintList = ContextCompat.getColorStateList(this,R.color.colorPrimary)
                        if(reciclerview.adapter?.itemCount!! <= 1) {
                            ly_estate.visibility = View.VISIBLE
                            reciclerview.visibility = View.GONE
                            val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_out_in)
                            imageState.startAnimation(animation)
                        } else {
                            ly_estate.visibility = View.GONE
                            reciclerview.visibility = View.VISIBLE
                        }
            }
        }
        buttonCrearEmpty.setOnClickListener { addSolicitud() }
    }


    override fun onContextItemSelected(item: MenuItem): Boolean
    {
        if(item.itemId == 1)
        {
            val intent = Intent(this, ActividadSolicitudDetails::class.java)
            intent.putExtra(INTENT.EXTRA_SOLICITUD,solicitud)
            intent.putExtra(INTENT.EXTRA_PERMISOS,permisoSol)
            startActivityForResult(intent,REQUESTCODE.ACTIVIDAD_DETAILS_SOL)
        }
        if(item.itemId == 2)
        {
            eliminarSolicitud(solicitud)
        }
        return super.onContextItemSelected(item)
    }

    private fun eliminarSolicitud(solicitud: Solicitudes?)
    {
        showProgresBar()
        val solicitudRequest = SolicitudRequest()
        solicitudRequest.modeAction = SolicitudRequest.MODEACTION.DEL
        solicitudRequest.solicitud = Solicitudes()
        solicitudRequest.usuario = Prefs.getString(PREFS.USUARIO, "")//.toUpperCase(Locale.ROOT)
        //solicitudRequest.usuario = "GAMMA"
        solicitudRequest.solicitud!!.codigo = solicitud?.codigo//?.trim()
        val retrofitClient = RetrofitClient.instance?.api?.eliminarSolicitud(solicitudRequest)
        retrofitClient!!.enqueue(object: Callback<ResponseWS> {
            override fun onFailure(call: Call<ResponseWS>, t: Throwable)
            {
                showProgresBar()
                showSimpleError(t.localizedMessage!!, TIPOMENSAJE.ERROR)
                isDelSolicitud = false
            }

            override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>) {
                showProgresBar()
                val responseWS = response.body()
                if (responseWS!!.sdtresult?.success!!)
                {
                    isDelSolicitud = true
                }
                else
                {
                    isDelSolicitud = false
                    responseWS.sdtresult?.let { mostarTodosLosMensajes(it) }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK ->
            {
                if (requestCode == REQUESTCODE.ACTIVIDAD_ADD_SOL)
                {
                    viewModelRecent = SolicitudesViewModel()
                    getSolicitudes()
                }
                if (requestCode == REQUESTCODE.ACTIVIDAD_DETAILS_SOL)
                {
                    viewModelRecent = SolicitudesViewModel()
                    getSolicitudes()
                }
                if (requestCode == REQUESTCODE.ACTIVIDAD_EDIT_SOL)
                {
                    solicitud = data!!.extras?.get(INTENT.EXTRA_SOLICITUD) as Solicitudes
                    adapter1!!.cambiarSOl(positionItem!!,solicitud)
                    adapter1!!.notifyItemChanged(positionItem!!)
//                    viewModelRecent = SolicitudesViewModel()
//                    getSolicitudes()
                }
            }
        }

    }

    override fun onClickSolicitudListener(solicitudes: Solicitudes?)
    {
        if(permisoSol!!.ver==1)
        {
            solicitud = solicitudes
            val intent = Intent(this, ActividadSolicitudDetails::class.java)
            intent.putExtra(INTENT.EXTRA_SOLICITUD,solicitud)
            intent.putExtra(INTENT.EXTRA_PERMISOS,permisoSol)
            startActivityForResult(intent,REQUESTCODE.ACTIVIDAD_DETAILS_SOL)
        }
        else
        {
            showErrorDenied(permisoSol!!.opcion,"Ver")
        }
    }

    override fun onLongSolicitudListener(solicitudes: Solicitudes?,position:Int)
    {
        solicitud = solicitudes
        positionItem = position
        val mBottomSheetDialog = BottomSheetDialog(layoutInflater.context, R.style.BaseBottomSheetDialog_BottomSheetDialogThemeRound)
        val sheetView = layoutInflater.inflate(R.layout.view_bottonsheet_menu, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.setCancelable(true)

        val lyVer = sheetView.findViewById<LinearLayout>(R.id.lyVer)
        val lyEditar = sheetView.findViewById<LinearLayout>(R.id.lyEditar)
        val lyEliminar = sheetView.findViewById<LinearLayout>(R.id.lyEliminar)

        if(permisoSol!!.ver == 0)
        {
            lyVer.visibility = View.GONE
        }
        if(permisoSol!!.up == 0)
        {
            lyEditar.visibility = View.GONE
        }
        if(permisoSol!!.del == 0)
        {
            lyEliminar.visibility = View.GONE
        }

        lyVer.setOnClickListener {
            mBottomSheetDialog.dismiss()
            val intent = Intent(this, ActividadSolicitudDetails::class.java)
            intent.putExtra(INTENT.EXTRA_SOLICITUD,solicitud)
            intent.putExtra(INTENT.EXTRA_PERMISOS,permisoSol)
            startActivityForResult(intent,REQUESTCODE.ACTIVIDAD_DETAILS_SOL)
        }
        lyEditar.setOnClickListener {
            mBottomSheetDialog.dismiss()
            val intent = Intent(this,ActividadEditarSolicitud::class.java)
            intent.putExtra(INTENT.EXTRA_SOLICITUD,solicitud)
            startActivityForResult(intent,REQUESTCODE.ACTIVIDAD_EDIT_SOL)
        }
        lyEliminar.setOnClickListener {
            mBottomSheetDialog.dismiss()
            eliminarSolicitud(solicitud)
        }


        mBottomSheetDialog.show()
    }

}

