package com.example.carlos.sgestman.entity

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.example.carlos.sgestman.R
import com.google.gson.annotations.SerializedName

class Ejecutor
{
    @SerializedName("ejecCodigo")
    var codigo:String? =null

    @SerializedName("ejecDenominacion")
    var denom:String? = null
}
