package com.example.carlos.sgestman.actividades.ui.reportetrabajo

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.adapter.CustomLoadStateAdapter
import com.example.carlos.sgestman.adapter.OrdenesAdapter
import com.example.carlos.sgestman.adapter.OrdenesReporteSelectAdapter
import com.example.carlos.sgestman.adapter.OrdenesReporteSelectAdapterListener
import com.example.carlos.sgestman.entity.Ordenes
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.viewmodel.OrdenesViewModel
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_solicitudes.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ActividadSelectOrdenReporte : AppCompatActivity() , OrdenesReporteSelectAdapterListener {
    private lateinit var itemDone: MenuItem
    private lateinit var toolbar: Toolbar

    private var iscargo = false
    lateinit var viewModelRecent: OrdenesViewModel
    var adapter : OrdenesReporteSelectAdapter? =null
    var orden: Ordenes?= null
    var ordenes:ArrayList<Ordenes> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actividad_select_orden_reporte)

        init()
        setupView()
    }

    private fun setupView() {
        setTollbar()

        adapter = OrdenesReporteSelectAdapter(applicationContext,this)
        reciclerview.layoutManager = (LinearLayoutManager(this))
        reciclerview.adapter =  adapter?.withLoadStateFooter(footer = CustomLoadStateAdapter(adapter!!::retry,applicationContext))

        adapter!!.addLoadStateListener { loadState ->

//            Log.e("raya" , "---------------------")
//            Log.e("loadState.append" , loadState.append.endOfPaginationReached.toString())
//            Log.e("loadState.refresh" , loadState.refresh.endOfPaginationReached.toString())
//            Log.e("loadState.source.refresh ", loadState.source.refresh.endOfPaginationReached.toString())
//            Log.e("is Refresh ", (loadState.source.refresh is LoadState.NotLoading).toString())

            if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached)
            {
                when {
                    adapter!!.itemCount < 1 -> {
                        showStateEmpty()
                        iscargo = false
                    }
                }
            }
            else if (adapter!!.itemCount > 1 && !iscargo)
            {
                iscargo = true
                showStateEmpty()
            }

        }

        //swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary)
        //swipeRefreshLayout.setOnRefreshListener { getOrdenes() }



        getOrdenes()
    }
    private fun init()
    {
        viewModelRecent = OrdenesViewModel()
        //viewModelRecent = ViewModelProvider(this)[SolicitudesViewModel1::class.java]

    }
    @SuppressLint("SetTextI18n")
    fun setTollbar()
    {
        toolbar = findViewById<View>(R.id.my_awesome_toolbar) as Toolbar
        toolbar.title = ""
        toolbar.navigationIcon= ContextCompat.getDrawable(this,R.drawable.ic_left_arrow)
        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        val mtitleToolbar = toolbar.findViewById(R.id.toolbar_title) as TextView
        mtitleToolbar.text = "Selecionar Orden"
    }

    fun getOrdenes()
    {
        lifecycleScope.launch{
            //@OptIn(ExperimentalCoroutinesApi::class)
            viewModelRecent.itemsOrdenes.collectLatest { pagingData ->
                //showStateEmpty(isbyFilter!!)
                showOnRecyclerView1(pagingData)
            }
        }
    }
    private suspend fun showOnRecyclerView1(t: PagingData<Ordenes>)
    {
        adapter!!.submitData(t)

        //  reciclerview.adapter = adapter1?.withLoadStateFooter(footer = CustomLoadStateAdapter(adapter1!!::retry))

        if(swipeRefreshLayout.isRefreshing)
        {
            swipeRefreshLayout.isRefreshing = false
        }

        //showStateEmpty(isbyFilter!!)

    }

    private fun showStateEmpty()
    {
        ly_estate.setOnClickListener{}
        textEmptySate.setText(R.string.emptystatesolicitudesDesc)
        imageState.setImageResource(R.drawable.ic_solicitudtrabajo)
        imageState.imageTintList = ContextCompat.getColorStateList(this,R.color.colorPrimary)
        if(reciclerview.adapter?.itemCount!! <= 1) {
            ly_estate.visibility = View.VISIBLE
            reciclerview.visibility = View.GONE
            val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_out_in)
            imageState.startAnimation(animation)
        } else {
            ly_estate.visibility = View.GONE
            reciclerview.visibility = View.VISIBLE
        }
    }

    override fun onClickOrdenListener(orden: Ordenes?)
    {
        var esta = false
        ordenes.forEach {
            if (it.numeroOT == orden?.numeroOT)
            {
                ordenes.remove(orden!!)
                esta = true
            }
        }
        if (!esta)
        {
            ordenes.add(orden!!)
        }
        ponerVisibleMenu()
    }

    private fun ponerVisibleMenu()
    {
       itemDone.isVisible = ordenes.size > 0
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_select_orden_reporte, menu)
        itemDone = menu!!.findItem(R.id.done)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.done ->
            {
                selecionarOts()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    private fun selecionarOts()
    {
        val data = Intent()

        if(ordenes.size> 0)
        {
            data.putParcelableArrayListExtra("ORDENESSELECT",ordenes)
            setResult(Activity.RESULT_OK,data)
        }
        onBackPressed()
    }

//    override fun onLongOrdenListener(orden: Ordenes?) {
//        ordenes.add(orden!!)
//    }

}