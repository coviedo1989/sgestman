package com.example.carlos.sgestman.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Created by carlos on 23/08/2017.
 */
class MiFecha @SuppressLint("SimpleDateFormat") constructor() {
    var fecha: String? = null
    var fechaSemana: String? = null
    var fechaVenta: String? = null
    private var simpleDateFormat: SimpleDateFormat
    private val simpleDateFormatVenta: SimpleDateFormat
    private val simpleDateFormatSemana: SimpleDateFormat
    private val dateTimeFormatter:DateTimeFormatter
    val simpleDateFormatHome:SimpleDateFormat


    fun formatLarge(fecha: String?): String {
        val date = LocalDateTime.now()
        var fechaSalida = ""
        val dateItem = LocalDateTime.parse(fecha, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        if (dateItem.dayOfMonth + 1 == date.dayOfMonth && dateItem.monthValue == date.monthValue && dateItem.year == date.year)
        {
            val simpleDateFormat = DateTimeFormatter.ofPattern("h:mm")
            fechaSalida = "Ayer, " + simpleDateFormat.format(dateItem)
        }
        else if (dateItem.dayOfMonth == date.dayOfMonth && dateItem.monthValue == date.monthValue && dateItem.year == date.year)
        {
            val simpleDateFormat = DateTimeFormatter.ofPattern("h:mm a")
            fechaSalida = "Hoy, " + simpleDateFormat.format(dateItem)
        }
        else if (dateItem.year == date.year)
        {
            val simpleDateFormat = DateTimeFormatter.ofPattern("MMM d, hh:mm a")
            fechaSalida = simpleDateFormat.format(dateItem)
        } else
        {
            val simpleDateFormat = DateTimeFormatter.ofPattern("yyyy/MMM d, hh:mm a")
            fechaSalida = simpleDateFormat.format(dateItem)
        }
        return fechaSalida
    }
    fun formatLocal(fecha: String?): String
    {

        val date = LocalDateTime.now()
        var fechaSalida = ""
        var simpleDateFormat = DateTimeFormatter.ofPattern("yyyy/M/d h:mm a")
        val dateItem = LocalDateTime.parse(fecha, simpleDateFormat)
        if (dateItem.dayOfMonth + 1 == date.dayOfMonth && dateItem.monthValue == date.monthValue && dateItem.year == date.year)
        {
            simpleDateFormat = DateTimeFormatter.ofPattern("h:mm a")
            fechaSalida = "Ayer, " + simpleDateFormat.format(dateItem)
        }
        else if (dateItem.dayOfMonth == date.dayOfMonth && dateItem.monthValue == date.monthValue && dateItem.year == date.year)
        {
            simpleDateFormat = DateTimeFormatter.ofPattern("h:mm a")
            fechaSalida = "Hoy, " + simpleDateFormat.format(dateItem)
        }
        else if (dateItem.year == date.year)
        {
            simpleDateFormat = DateTimeFormatter.ofPattern("yyyy/MMM d, hh:mm a")
            fechaSalida = simpleDateFormat.format(dateItem)
        } else
        {
            simpleDateFormat = DateTimeFormatter.ofPattern("yyyy/MMM d, hh:mm a")
            fechaSalida = simpleDateFormat.format(dateItem)
        }
        return fechaSalida
    }
    fun formatNormal(fecha: String?): String {
        val date = LocalDateTime.now()
        var fechaSalida = ""
        val dateItem = LocalDateTime.parse(fecha, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        val simpleDateFormat = DateTimeFormatter.ofPattern("yyyy/MM/d hh:mm a")
        fechaSalida = simpleDateFormat.format(dateItem)
        return fechaSalida
    }
    fun formatShort(fecha: String?): String
    {
        val date = LocalDateTime.now()
        var fechaSalida = ""
        val dateItem = LocalDateTime.parse(fecha, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        if (dateItem.dayOfMonth + 1 == date.dayOfMonth && dateItem.monthValue == date.monthValue && dateItem.year == date.year)
        {
            val simpleDateFormat = DateTimeFormatter.ofPattern("h:mm a")
            fechaSalida = "Ayer, " + simpleDateFormat.format(dateItem)
        }
        else if (dateItem.dayOfMonth == date.dayOfMonth && dateItem.monthValue == date.monthValue && dateItem.year == date.year)
        {
            val simpleDateFormat = DateTimeFormatter.ofPattern("h:mm a")
            fechaSalida = "Hoy, " + simpleDateFormat.format(dateItem)
        }
        else if (dateItem.year == date.year)
        {
            val simpleDateFormat = DateTimeFormatter.ofPattern("yyyy/MM/d")
            fechaSalida = simpleDateFormat.format(dateItem)
        } else
        {
            val simpleDateFormat = DateTimeFormatter.ofPattern("yyyy/MM/d")
            fechaSalida = simpleDateFormat.format(dateItem)
        }
        return fechaSalida
    }
    fun fechaRequest(fecha: String?):String
    {
        var fechaSalida = ""
        var fechaAux = fecha!!.replace("-","/").replace("T"," ")
        val simpleDateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
        val dateItem = LocalDateTime.parse(fecha, DateTimeFormatter.ISO_LOCAL_DATE_TIME)

        fechaSalida = simpleDateFormat.format(dateItem)

        return fechaSalida
    }

    init {
        val date = Date()
        simpleDateFormat = SimpleDateFormat("yyyy/M/d h:mm a")
        simpleDateFormatVenta = SimpleDateFormat("yy/M")
        simpleDateFormatSemana = SimpleDateFormat("E, yyyy/M/d")
        fecha = simpleDateFormat.format(date)
        fechaVenta = simpleDateFormatVenta.format(date)
        fechaSemana = simpleDateFormatSemana.format(date)
        simpleDateFormatHome = SimpleDateFormat("EEEE, d 'de' MMMM 'del' yyyy")
        dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    }
}