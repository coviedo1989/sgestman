package com.example.carlos.sgestman.model

class DataModelMenuInicio(title: String, titleDB:String, drawableImg: Int, color: String) {
    var title: String? = title
    var titledb:String? = titleDB
    var drawable = drawableImg
    var color: String? = color
}