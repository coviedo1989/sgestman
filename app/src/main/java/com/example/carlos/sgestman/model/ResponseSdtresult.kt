package com.example.carlos.sgestman.model

import com.google.gson.annotations.SerializedName
import java.util.*

class ResponseSdtresult
{
    @SerializedName("Mensajes")
    var mensajes: List<ResponseSdtresultMensajes>? = null
        private set
    @SerializedName("Success")
    var success = false

    fun setMensajes(Mensajes: ArrayList<ResponseSdtresultMensajes>?) {
        mensajes = Mensajes
    }

}