package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.entity.OrdenReporteWS
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.repository.dao.OrdenReporteDao
import com.example.carlos.sgestman.repository.dao.OrdenReporteWSDao

class OrdenReporteWSRoomDBRepository(application: Application?) {
    private val ordenReporteWSInfoDao: OrdenReporteWSDao?
    var allOrdenReporteWs: LiveData<List<OrdenReporteWS>?>?

    fun insertOrdenesReporteWS(resultModel: OrdenReporteWS?) {
        InsertAsyncTask(ordenReporteWSInfoDao).execute(resultModel)
    }
    fun deleteOrdenesReporteWS(resultModel: OrdenReporteWS?) {
        DeleteAsyncTask(ordenReporteWSInfoDao).execute(resultModel)
    }

    fun deleteOrdenesALLReporteWS() {
        DeleteALLAsyncTask(ordenReporteWSInfoDao).execute()
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: OrdenReporteWSDao?) : AsyncTask<OrdenReporteWS?, Void?, Void?>() {
        override fun doInBackground(vararg params: OrdenReporteWS?): Void?
        {
            if(mAsyncTaskDao?.selectByNumeroOT(params[0]!!.numeroOT!!)?.count()==0)
            {
                mAsyncTaskDao.insert(params[0])
            }

            return null
        }
    }
    private class DeleteAsyncTask internal constructor(private val mAsyncTaskDao: OrdenReporteWSDao?):AsyncTask<OrdenReporteWS?,Void?,Void?>(){
        override fun doInBackground(vararg params: OrdenReporteWS?): Void? {
            mAsyncTaskDao!!.deleteByNumeroOT(params[0]!!.numeroOT!!)
            return null
        }
    }
    private class DeleteALLAsyncTask internal constructor(private val mAsyncTaskDao: OrdenReporteWSDao?):AsyncTask<Void?,Void?,Void?>(){
        override fun doInBackground(vararg params: Void?): Void? {
            mAsyncTaskDao!!.deleteAll()
            return null
        }
    }

    init {
        val db = getDatabase(application!!)
        ordenReporteWSInfoDao = db!!.ordenReporteWSInfoDao()
        allOrdenReporteWs = ordenReporteWSInfoDao.allOrdenReportews
    }
}