package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.entity.Configuraciones
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.repository.dao.ConfiguracionesDao

class ConfiguracionesRoomDBRepository(application: Application?) {
    private val configuracionesInfoDao: ConfiguracionesDao?
    var allConfiguraciones: LiveData<Configuraciones?>?

    fun insertConfiguracion(resultModel: Configuraciones?) {
        InsertAsyncTask(configuracionesInfoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: ConfiguracionesDao?) : AsyncTask<Configuraciones?, Void?, Void?>() {
        override fun doInBackground(vararg globals: Configuraciones?): Void? {
            mAsyncTaskDao!!.insertGlobal(globals[0])
            return null
        }

    }

    init {
        val db = getDatabase(application!!)
        configuracionesInfoDao = db!!.configuracionesInfo()
        allConfiguraciones = configuracionesInfoDao!!.allGlobal
    }
}