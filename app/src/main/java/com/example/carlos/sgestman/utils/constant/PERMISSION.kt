package com.example.carlos.sgestman.utils.constant

object PERMISSION {

    ///1 solicitudes
    ///2 ordenes

    const val CAMERA: Int = 40
    var ACTIVIDAD_ADD_SOL = 11
    var ACTIVIDAD_DETAILS_SOL = 12

    var ACTIVIDAD_ADD_ORD = 21
    var ACTIVIDAD_DETAILS_ORD = 22

    var ACTIVIDAD_REPORTE_SELECT_OT = 32

}
