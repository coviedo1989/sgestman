package com.example.carlos.sgestman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.model.DataModelMenuInicio


class GridLayoutMenuAdapter(context: Context, values: ArrayList<DataModelMenuInicio>, itemListener: ItemListener?) : RecyclerView.Adapter<GridLayoutMenuAdapter.ViewHolder>() {
    var mValues: ArrayList<DataModelMenuInicio>
    var mContext: Context
    protected var mListener: ItemListener?

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var textView: TextView
        var imageView: ImageView
        //var relativeLayout: RelativeLayout
        var item: DataModelMenuInicio? = null
        fun setData(item: DataModelMenuInicio) {
            this.item = item
            textView.setText(item.title)
            imageView.setImageResource(item.drawable)
            //relativeLayout.setBackgroundColor(Color.parseColor(item.color))
        }

        override fun onClick(view: View?) {
            if (mListener != null) {
                mListener!!.onItemClick(item)
            }
        }

        init {
            v.setOnClickListener(this)
            textView = v.findViewById(R.id.textView) as TextView
            imageView = v.findViewById(R.id.imageView) as ImageView
            //relativeLayout = v.findViewById(R.id.relativeLayout) as RelativeLayout
        }
    }



    override fun onBindViewHolder(Vholder: ViewHolder, position: Int) {
        Vholder.setData(mValues[position])
    }


    interface ItemListener {
        fun onItemClick(item: DataModelMenuInicio?)
    }

    init {
        mValues = values
        mContext = context
        mListener = itemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.grid_menu_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

}