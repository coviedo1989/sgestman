package com.example.carlos.sgestman.actividades

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.BitmapFactory
import android.media.VolumeShaper
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.databinding.ActivityIniciarSessionBinding
import com.example.carlos.sgestman.model.ResponseWS
import com.example.carlos.sgestman.model.UserLogin
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.utils.HandleErrorApi
import com.example.carlos.sgestman.utils.views.BlurBuilder
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.CustomProgressDialog
import com.example.carlos.sgestman.viewmodel.IniciarSesionViewModel
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_iniciar_session.*
import kotlinx.android.synthetic.main.setting_api_server.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class IniciarSession : BasicActivity()
{
    private var progressDialog: CustomProgressDialog? =null
    private var progressBar: ProgressBar? = null
    private var imgBlur: ImageView? = null
    private lateinit var biding:ActivityIniciarSessionBinding
    private val viewmodel:IniciarSesionViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        //requestWindowFeature(Window.FEATURE_NO_TITLE)

        biding = ActivityIniciarSessionBinding.inflate(layoutInflater)

        //biding.mode = applicationContext.resources.configuration



        setContentView(biding.root)
        progressDialog= CustomProgressDialog(this)


        viewmodel.responseWS.observe(this, androidx.lifecycle.Observer {
            if(it!=null)
            {
                if (!it.sdtresult?.success!!)
                {
                    it.sdtresult?.let { sdtresult -> mostarTodosLosMensajes(sdtresult) }
                }
                else
                {
                    val intentPrincipal = Intent(this@IniciarSession, ActivityPrincipal::class.java)
                    startActivity(intentPrincipal)
                    finish()
                }
            }
        })

        viewmodel.progrevisibility.observe(this, androidx.lifecycle.Observer {
            if (it!=null)
            {
                //biding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
                //showProgresBar()
            }
        })

        imgBlur = findViewById<View>(R.id.img_blur) as ImageView

        setupView()

    }

    private fun setupView()
    {

        if(Prefs.getBoolean(PREFS.OP_RECORDAR_CONTRASENA,false))
        {
            val strUser = Prefs.getString(PREFS.USUARIO,"")
            val strPass = Prefs.getString(PREFS.CONTRASENA,"")
            usuario.setText(strUser)
            contrasenaa.setText(strPass)
        }
        setupImageBlur()

    }

    private fun setupImageBlur()
    {
      //  val mode = applicationContext?.resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)
        val resultBmp = BlurBuilder.blur(this, BitmapFactory.decodeResource(resources, R.drawable.fondoiniciarsesion1))
        //imgBlur?.setImageBitmap(resultBmp)

        //val resultBmp = BlurBuilder.blur(this, BitmapFactory.decodeResource(resources, R.drawable.fondoiniciarsesion1))
       // imgBlur!!.setImageBitmap(resultBmp)
//
//        when(mode)
//        {
//            Configuration.UI_MODE_NIGHT_YES->{
//                imgBlur?.visibility = View.VISIBLE
//            }
//            Configuration.UI_MODE_NIGHT_NO->{
//                imgBlur?.visibility = View.VISIBLE
//            }
//            Configuration.UI_MODE_NIGHT_UNDEFINED->{
//                imgBlur?.visibility = View.VISIBLE
//            }
//        }

    }

    fun BtnAceptar(view:View)
    {
        clolseTeclado()

        when {
            biding.usuario.text.toString().isEmpty() ->
            {
                showSimpleError("Usuario vacío.", TIPOMENSAJE.INFO)
            }
            biding.contrasenaa.text.toString().isEmpty() ->
            {
                showSimpleError("Contraseña vacía.", TIPOMENSAJE.INFO)
            }
            Prefs.getString(PREFS.CONFIG_API_URL,"").isEmpty() ->
            {
                showSimpleError("Debe espeficicar la configuración del servidor", TIPOMENSAJE.INFO)
            }
            !Prefs.getString(PREFS.CONFIG_API_URL,"").contains("rest/")  ->
            {
                showSimpleError("Error conectando al servidor: "+Prefs.getString(PREFS.CONFIG_API_URL,""), TIPOMENSAJE.ERROR)
            }
            else->
            {
                viewmodel.onButtonClikListener(biding.usuario.text.toString(),biding.contrasenaa.text.toString(), this.progressDialog!!)
            }
        }


//        val usuario = findViewById<EditText>(R.id.usuario)
//        val contrasenaa = findViewById<EditText>(R.id.contrasenaa)
//        when {
//            usuario.text.toString().isEmpty() -> {
//                showSimpleError("Usuario vacío.", TIPOMENSAJE.INFO)
//            }
//            contrasenaa.text.toString().isEmpty() -> {
//                showSimpleError("Contraseña vacía.", TIPOMENSAJE.INFO)
//            }
//            Prefs.getString(PREFS.CONFIG_API_URL,"").isEmpty() ->{
//                showSimpleError("Debe espeficicar la configuración del servidor", TIPOMENSAJE.INFO)
//            }
//            !Prefs.getString(PREFS.CONFIG_API_URL,"").contains("rest/")  ->{
//                showSimpleError("Error conectando al servidor: "+Prefs.getString(PREFS.CONFIG_API_URL,""), TIPOMENSAJE.ERROR)
//            }
//            else -> {
//                val usser = usuario.text.toString().toLowerCase(Locale.ROOT)
//                val contra = contrasenaa.text.toString()
//                val user = UserLogin(usser, contra)
//                showProgresBar()
//
//                val call1 = RetrofitClient.instance?.api?.userLogin1(user)
//
//                call1?.enqueue(object : Callback<ResponseWS> {
//                    override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>)
//                    {
//                        if (response.isSuccessful)
//                        {
//                            val responseWS = response.body()
//                            if (responseWS!!.sdtresult?.success!!)
//                            {
//                                Prefs.putString(PREFS.USUARIO, usser)
//                                Prefs.putString(PREFS.CONTRASENA, contra)
//                                val intentPrincipal = Intent(this@IniciarSession, ActivityPrincipal::class.java)
//                                startActivity(intentPrincipal)
//                                finish()
//                            }
//                            else
//                            {
//                                showProgresBar()
//                                responseWS.sdtresult?.let { mostarTodosLosMensajes(it) }
//                            }
//                        }
//                        //showProgresBar()
//                    }
//
//                    override fun onFailure(call: Call<ResponseWS>, t: Throwable) {
//                        showProgresBar()
//                        showSimpleError(HandleErrorApi(applicationContext,t).mensaje(), TIPOMENSAJE.ERROR)
//                    }
//                })
//            }
//        }

    }

    private fun clolseTeclado() {
        val view = this.currentFocus
        if (view != null) {
            val im = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            im.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun btnCancelar(view:View) {
        onBackPressed()
    }



    fun showProgessbar() {
        if (progressBar!!.visibility == View.INVISIBLE) {
            progressBar!!.visibility = View.VISIBLE
            progressBar!!.isIndeterminate = true
        } else {
            progressBar!!.visibility = View.INVISIBLE
            progressBar!!.isIndeterminate = false
        }
    }

    fun onClickSettingsServer(view: View)
    {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.setting_api_server, null)
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mDialogView.url.setText(Prefs.getString(PREFS.CONFIG_API_URL,""))
        mDialogView.puerto.setText(if (Prefs.getInt(PREFS.CONFIG_API_PUERTO,0) == 0) "" else Prefs.getInt(PREFS.CONFIG_API_PUERTO,0).toString())
        mDialogView.btnAceptar.setOnClickListener {
            if (!mDialogView.url.text.isEmpty() || !mDialogView.puerto.text.isEmpty())
            {
                if(mDialogView.url.text.contains("https://") && mDialogView.url.text.contains("rest"))
                {
                    Prefs.putString(PREFS.CONFIG_API_URL,mDialogView.url.text.toString())
                    if(mDialogView.puerto.text.isEmpty())
                    {
                        Prefs.putInt(PREFS.CONFIG_API_PUERTO,443)
                    }
                    else
                    {
                        Prefs.putInt(PREFS.CONFIG_API_PUERTO,mDialogView.puerto.text.toString().toInt())
                    }
                    mAlertDialog.dismiss()
                }
            }
        }
        mDialogView.btnCancelar.setOnClickListener { mAlertDialog.dismiss() }
    }
}