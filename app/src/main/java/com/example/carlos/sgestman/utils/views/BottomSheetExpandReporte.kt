package com.example.carlos.sgestman.utils.views


import android.app.Dialog
import android.content.res.Resources
import android.content.res.TypedArray
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.databinding.BottomSheetReporteBinding
import com.example.carlos.sgestman.entity.OrdenReporteTiempo
import com.example.carlos.sgestman.entity.ReporteTiempo
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlin.math.roundToInt


class BottomSheetExpandReporte() : BottomSheetDialogFragment() {
    var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    var bi: BottomSheetReporteBinding? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheet = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        //inflating layout
        val view = View.inflate(context, R.layout.bottom_sheet_reporte, null)


        //binding views to data binding.
        bi = DataBindingUtil.bind(view)


       // bi!!.numeroOtFirst.text= ordenReporteTiempo.ordenReporte!!.numeroOT
      //  bi!!.numeroOtSecond.text= ordenReporteTiempo.ordenReporte!!.numeroOT

        //setting layout with bottom sheet
        bottomSheet.setContentView(view)
        bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)


        //setting Peek at the 16:9 ratio keyline of its parent.
        (bottomSheetBehavior as BottomSheetBehavior<*>).peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO


        //setting max height of bottom sheet
        bi!!.extraSpace.minimumHeight = Resources.getSystem().displayMetrics.heightPixels / 2
        (bottomSheetBehavior as BottomSheetBehavior<*>).setBottomSheetCallback(object : BottomSheetCallback() {
             override fun onStateChanged(bottomSheet: View, i: Int) {
                if (BottomSheetBehavior.STATE_EXPANDED == i) {
                    showView(bi!!.appBarLayout, actionBarSize)
                    hideAppBar(bi!!.profileLayout)
                }
                if (BottomSheetBehavior.STATE_COLLAPSED == i) {
                    hideAppBar(bi!!.appBarLayout)
                    showView(bi!!.profileLayout, actionBarSize)
                }
                if (BottomSheetBehavior.STATE_HIDDEN == i) {
                    dismiss()
                }
            }

            override fun onSlide(bottomSheet: View, v: Float) {}
        })

        //aap bar cancel button clicked
        bi!!.cancelBtn.setOnClickListener { dismiss() }

        //aap bar edit button clicked
        bi!!.editBtn.setOnClickListener { Toast.makeText(context, "Edit button clicked", Toast.LENGTH_SHORT).show() }

        //aap bar more button clicked
        bi!!.moreBtn.setOnClickListener { Toast.makeText(context, "More button clicked", Toast.LENGTH_SHORT).show() }


        //hiding app bar at the start
        hideAppBar(bi!!.appBarLayout)
        return bottomSheet
    }

    override fun onStart() {
        super.onStart()
        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun hideAppBar(view: View) {
        val params: ViewGroup.LayoutParams = view.layoutParams
        params.height = 0
        view.layoutParams = params
    }

    private fun showView(view: View, size: Float) {
        val params: ViewGroup.LayoutParams = view.layoutParams
        params.height = size.roundToInt()
        view.layoutParams = params
    }

    private val actionBarSize: Float
        get() {
            val array: TypedArray = requireContext().theme.obtainStyledAttributes(intArrayOf(R.attr.actionBarSize))
            return array.getDimension(0, 0F)
        }
}