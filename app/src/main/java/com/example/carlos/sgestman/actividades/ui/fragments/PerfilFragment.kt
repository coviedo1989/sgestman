package com.example.carlos.sgestman.actividades.ui.fragments

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.actividades.IniciarSession
import com.example.carlos.sgestman.model.ResponseSdtresult
import com.example.carlos.sgestman.model.ResponseWS
import com.example.carlos.sgestman.model.UserLogin
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.utils.HandleErrorApi
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.BottomSheetMensaje
import com.example.carlos.sgestman.utils.views.CustomProgressDialog
import com.example.carlos.sgestman.viewmodel.PerfilViewModel
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.dialog_cambiar_contrasena.view.*
import kotlinx.android.synthetic.main.fragment_perfil.*
import kotlinx.android.synthetic.main.fragment_perfil.textEmptySate
import kotlinx.android.synthetic.main.setting_api_server.view.*
import kotlinx.android.synthetic.main.setting_api_server.view.btnAceptar
import kotlinx.android.synthetic.main.setting_api_server.view.btnCancelar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PerfilFragment : Fragment()
{
    private lateinit var progressDialog: CustomProgressDialog
    private var mAlertDialog: AlertDialog? = null
    private var perfilViewModel: PerfilViewModel? = null
    var ly_estate:LinearLayout? = null
    var ly_todo:LinearLayout? = null
    var userLogin:UserLogin? = null
    var isChangeMode = false

    lateinit var txtConfigServidorUrl:TextView
    lateinit var txtConfigServidorPuerto:TextView

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        perfilViewModel = ViewModelProvider(this).get(PerfilViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_perfil, container, false)

        setupView(root)
        getData()
        return root
    }

    private fun getData()
    {
        perfilViewModel!!.getmAllUser()?.observe(viewLifecycleOwner, Observer { s ->
            if (s != null)
            {
                txtUsuario!!.text = s.usuario?.toUpperCase()
                txtCorreo!!.text = s.correo
                txtDescripcion!!.text = s.decripcion
                txtNombre!!.text = s.empleadoNombre
            }
            else
            {
                chekEmptyState()
            }
        })
    }

    private fun chekEmptyState()
    {
        if(ly_estate?.visibility == View.GONE)
        {
            ly_estate?.visibility = View.VISIBLE
            ly_todo?.visibility = View.GONE
            textEmptySate.text = String.format(requireContext().getString(R.string.error_msg_conection),Prefs.getString(PREFS.CONFIG_API_URL,""))
        }
        else
        {
            ly_estate?.visibility = View.GONE
            ly_todo?.visibility = View.VISIBLE
        }

    }

    private fun setupView(root: View?)
    {
        val txtCambiarContrasena = root!!.findViewById<TextView>(R.id.txt_cambiar_contrasena)
        val txtCerrarSesion = root.findViewById<TextView>(R.id.txtCerrarSesion)
        txtCambiarContrasena.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        txtCerrarSesion.paintFlags = Paint.UNDERLINE_TEXT_FLAG

        ly_estate = root.findViewById<LinearLayout>(R.id.ly_estate)
        ly_todo = root.findViewById<LinearLayout>(R.id.ly_todo)

        val switchInicioAutomatico = root.findViewById<SwitchCompat>(R.id.switchInciarSesionAutomaticamente)
        val switchRecodarContrasena = root.findViewById<SwitchCompat>(R.id.switchUsuarioContrasena)
        val switchDarkmode = root.findViewById<SwitchCompat>(R.id.switchDarkmode)
        val lyConfigServer = root.findViewById<LinearLayout>(R.id.ly_configServer)

        txtConfigServidorUrl = root.findViewById(R.id.txtConfigServidorUrl)
        txtConfigServidorPuerto = root.findViewById(R.id.txtConfigServidorPuerto)


        switchInicioAutomatico.setOnCheckedChangeListener { _, isChecked ->
        Prefs.putBoolean(PREFS.OP_INICIAR_SESION_AUTOMATICA,isChecked)
        }

        switchRecodarContrasena.setOnCheckedChangeListener { _, isChecked ->
            Prefs.putBoolean(PREFS.OP_RECORDAR_CONTRASENA,isChecked)
        }

        switchDarkmode.setOnCheckedChangeListener{_,isChequed->
            Prefs.putBoolean(PREFS.OP_MODE_NIGHT,isChequed)
            if (isChequed)
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }
            else
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }

        lyConfigServer.setOnClickListener { ConfigurarServer() }

        switchInicioAutomatico.isChecked = Prefs.getBoolean(PREFS.OP_INICIAR_SESION_AUTOMATICA,false)
        switchRecodarContrasena.isChecked = Prefs.getBoolean(PREFS.OP_RECORDAR_CONTRASENA,false)
        switchDarkmode.isChecked = Prefs.getBoolean(PREFS.OP_MODE_NIGHT,false)

        txtCambiarContrasena.setOnClickListener { CambiarContrasena() }

        txtCerrarSesion.setOnClickListener {
            activity?.finish()
            val intentIniciarSesion = Intent(context, IniciarSession::class.java)
            startActivity(intentIniciarSesion)
        }

        val url = Prefs.getString(PREFS.CONFIG_API_URL,"Url para APP")
        txtConfigServidorUrl.text = String.format("url: %s",url)
        val puerto = Prefs.getInt(PREFS.CONFIG_API_PUERTO,443)
        txtConfigServidorPuerto.text = String.format("puerto: %d",puerto)
    }

    private fun ConfigurarServer()
    {
        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.setting_api_server, null)
        val mBuilder = AlertDialog.Builder(requireContext())
                .setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mDialogView.url.setText(Prefs.getString(PREFS.CONFIG_API_URL,""))
        mDialogView.puerto.setText(if (Prefs.getInt(PREFS.CONFIG_API_PUERTO,0) == 0) "" else Prefs.getInt(PREFS.CONFIG_API_PUERTO,0).toString())
        mDialogView.btnAceptar.setOnClickListener {
            if (!mDialogView.url.text.isEmpty() || !mDialogView.puerto.text.isEmpty())
            {
                if(mDialogView.url.text.contains("https://") && mDialogView.url.text.contains("rest"))
                {
                    Prefs.putString(PREFS.CONFIG_API_URL,mDialogView.url.text.toString())
                    if(mDialogView.puerto.text.isEmpty())
                    {
                        Prefs.putInt(PREFS.CONFIG_API_PUERTO,443)
                    }
                    else
                    {
                        Prefs.putInt(PREFS.CONFIG_API_PUERTO,mDialogView.puerto.text.toString().toInt())
                    }
                    txtConfigServidorUrl.text = String.format("url: %s",mDialogView.url.text)
                    val puerto = Prefs.getInt(PREFS.CONFIG_API_PUERTO,443)
                    txtConfigServidorPuerto.text = String.format("puerto: %d",puerto)
                    mAlertDialog.dismiss()
                }
            }
        }
        mDialogView.btnCancelar.setOnClickListener { mAlertDialog.dismiss() }

    }

    private fun CambiarContrasena()
    {
        userLogin = UserLogin()
        userLogin?.usuario = Prefs.getString(PREFS.USUARIO,"")
        if(userLogin!!.usuario!!.isNotEmpty())
        {
            val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_cambiar_contrasena, null)
            val mBuilder = AlertDialog.Builder(requireContext())
                    .setView(mDialogView)
            mAlertDialog = mBuilder.show()
            mAlertDialog?.setCancelable(false)
            mDialogView.btnAceptar.setOnClickListener {
                if((mDialogView.contrasenaa.text.isNotEmpty() and mDialogView.contrasenarepetir.text.isNotEmpty()) and (mDialogView.contrasenarepetir.text.toString() == mDialogView.contrasenaa.text.toString()))
                {
                    userLogin?.contrasenna = mDialogView.contrasenaa.text.toString()
                    restCambiarPassword()
                    //rest cambiar contrasena
                }
                else
                {
                    ShowError("Las contraseñan no coinciden",TIPOMENSAJE.INFO)
                }
            }
            mDialogView.btnCancelar.setOnClickListener { mAlertDialog?.dismiss() }
        }

    }

    private fun restCambiarPassword()
    {
        progressDialog = CustomProgressDialog(requireContext())
        progressDialog.show()
        val call1 = RetrofitClient.instance?.api?.userCambiarPassword(userLogin)

        call1?.enqueue(object : Callback<ResponseWS> {
            override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>)
            {
                progressDialog.dimis()
                if (response.isSuccessful) {
                    val responseWS = response.body()
                    if (responseWS!!.sdtresult?.success!!)
                    {
                        mAlertDialog?.dismiss()
                    } else {
                        responseWS.sdtresult?.let { MostarTodosLosMensajes(it) }
                    }
                }
                //ShowProgessbar()
            }

            override fun onFailure(call: Call<ResponseWS>, t: Throwable)
            {
                progressDialog.dimis()
                ShowError(HandleErrorApi(requireContext(),t).mensaje(), TIPOMENSAJE.ERROR)
            }
        })
    }

    private fun MostarTodosLosMensajes(sdtresult: ResponseSdtresult) {
        var mensaje:String?
        if (sdtresult.mensajes != null) {
            mensaje = " \n"
            for (i in sdtresult.mensajes!!.indices) {
                mensaje = """$mensaje${sdtresult.mensajes!![i].description}"""
            }
            ShowError(mensaje, TIPOMENSAJE.ERROR)
        }
    }
    fun ShowError(mensaje: String?, tipomensaje: TIPOMENSAJE?)
    {
        val layoutInflater = layoutInflater
        BottomSheetMensaje.showMensaje(mensaje, tipomensaje, layoutInflater)
    }
}