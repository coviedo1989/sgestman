package com.example.carlos.sgestman.actividades.ui.introapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.carlos.sgestman.R

/**
 * A simple [Fragment] subclass.
 */
class Fragment_Intro1 : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        //        Typeface font = Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Black.ttf");
//        textView.setTypeface(font);
        return inflater.inflate(R.layout.fragment_fragment__intro1, container, false)
    }
}