package com.example.carlos.sgestman.viewmodel

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.entity.OrdenReporteTiempo
import com.example.carlos.sgestman.entity.OrdenReporteWS
import com.example.carlos.sgestman.repository.Databases.OrdenReporteRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.OrdenReporteTiempoRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.OrdenReporteWSRoomDBRepository
import com.example.carlos.sgestman.repository.ws.WebServiceRepository

class OrdenReporteViewModel(application: Application):AndroidViewModel(application)
{
    private var webServiceRepository: WebServiceRepository = WebServiceRepository(getApplication())
    private var ordenReporteWSRoomDBRepository: OrdenReporteWSRoomDBRepository = OrdenReporteWSRoomDBRepository(application)
    private var ordenReporteTiempoRoomDBRepository: OrdenReporteTiempoRoomDBRepository = OrdenReporteTiempoRoomDBRepository(application)
    var allOrdenReporteTiempo: LiveData<List<OrdenReporteTiempo>?>
    val allOrdenReporteWS: LiveData<List<OrdenReporteWS>?>
    private var _allobserver = MutableLiveData<List<OrdenReporte>?>()
    val allobserver : LiveData<List<OrdenReporte>?> get() = _allobserver

    init
    {
        _allobserver = webServiceRepository.providesOrdenReporteWebService()
        allOrdenReporteTiempo = ordenReporteTiempoRoomDBRepository.allOrdenesReporte!!
        allOrdenReporteWS = ordenReporteWSRoomDBRepository.allOrdenReporteWs!!


    }



}