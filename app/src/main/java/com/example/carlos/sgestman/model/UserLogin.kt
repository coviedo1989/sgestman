package com.example.carlos.sgestman.model

class UserLogin {
    var usuario: String? = null
    var contrasenna: String? = null

    constructor() {}
    constructor(usuario: String?, contrasenna: String?) {
        this.usuario = usuario
        this.contrasenna = contrasenna
    }
}