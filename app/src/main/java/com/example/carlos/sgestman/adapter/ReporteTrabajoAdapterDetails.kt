package com.example.carlos.sgestman.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.databinding.ItemReportetiempoBinding
import com.example.carlos.sgestman.databinding.ItemReportetiempoDetailsBinding
import com.example.carlos.sgestman.databinding.ItemReportetrabajoBinding
import com.example.carlos.sgestman.entity.OrdenReporteTiempo
import com.example.carlos.sgestman.entity.ReporteTiempo
import com.example.carlos.sgestman.utils.MiFecha

/**
 * Created by HP-HP on 05-12-2015.
 */
class ReporteTrabajoAdapterDetails(var mFeedList: ArrayList<ReporteTiempo>, private var contex: Context, private val listener: EliminarReporteListener) : RecyclerView.Adapter<ViewHolderReporteTrabajoDetails>()
{
    private var mLayoutInflater: LayoutInflater? = null
    private var count = 0
    private var lastPosition = -1



    override fun onBindViewHolder(holder: ViewHolderReporteTrabajoDetails, position: Int) {
        this.position = position
        val reporteTiempo = mFeedList[position]
        holder.bindTo(reporteTiempo,listener,position)


        //setAnimation(holder.itemView, position);
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderReporteTrabajoDetails {
        return ViewHolderReporteTrabajoDetails.from(parent)
    }
    override fun onViewDetachedFromWindow(holder: ViewHolderReporteTrabajoDetails)
    {
        super.onViewDetachedFromWindow(holder)
    }
    var position = 0

    override fun getItemCount(): Int {
        return mFeedList.size
    }

    fun setCount(count: Int) {
        this.count = count
        notifyDataSetChanged()
    }

    fun removeAt(position: Int)
    {
        //mFeedList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemChanged(position,itemCount)
    }
}
 class ViewHolderReporteTrabajoDetails(private val binding: ItemReportetiempoDetailsBinding, private val contex: Context) : RecyclerView.ViewHolder(binding.root){

     var reporteTiempo : ReporteTiempo? = null

     fun bindTo(reporteTiempo1: ReporteTiempo?, eliminarReporteListener: EliminarReporteListener, position: Int)
     {
         this.reporteTiempo = reporteTiempo1
         binding.fecha = MiFecha()
         binding.reportetiempo = reporteTiempo1


         binding.imgEliminar.setOnClickListener { eliminarReporteListener.eliminarListener(reporteTiempo!!,position) }

         binding.executePendingBindings()
     }



     private fun siHayOTPlay(mFeedList: List<OrdenReporteTiempo>): Boolean {
         var isplay = false
         mFeedList.forEach {
             if (it.ordenReporte!!.isplay!!) {
                 isplay = true
             }
         }
         return  isplay
     }

     companion object {
         fun from(parent: ViewGroup): ViewHolderReporteTrabajoDetails
         {
             val layoutInflater = LayoutInflater.from(parent.context)
             val binding = ItemReportetiempoDetailsBinding.inflate(layoutInflater, parent, false)
             return ViewHolderReporteTrabajoDetails(binding,parent.context)
         }
     }
}

interface EliminarReporteListener{
    fun eliminarListener(reporteTiempo: ReporteTiempo,position: Int)
}