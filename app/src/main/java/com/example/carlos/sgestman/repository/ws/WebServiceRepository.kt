package com.example.carlos.sgestman.repository.ws

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carlos.sgestman.entity.*
import com.example.carlos.sgestman.model.DashBoardRequest
import com.example.carlos.sgestman.model.ResponsePermisosApp
import com.example.carlos.sgestman.model.SDTSDDashboard
import com.example.carlos.sgestman.repository.Databases.*
import com.example.carlos.sgestman.utils.UrlServices
import com.example.carlos.sgestman.utils.constant.PREFS
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.pixplicity.easyprefs.library.Prefs
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import kotlin.collections.ArrayList

class WebServiceRepository(private val application: Application) {
    private val wsResponseConuntryList: List<User> = ArrayList()
    private val wsResponseDashBoard: List<SDTSDDashboard> = ArrayList()
    private var wsSolictudes:List<Solicitudes> = ArrayList()


    fun providesUserWebService(): LiveData<User>
    {
        val data = MutableLiveData<User>()
        try {
            val retrofit = Retrofit.Builder()
                    .baseUrl(UrlServices.UrlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(providesOkHttpClientBuilder())
                    .build()


            //Defining retrofit api service
            val service = retrofit.create(ApiService::class.java)
            val user = User()
            user.usuario = Prefs.getString(PREFS.USUARIO, "gamma")
            val call = service.getDataUsuario(user)
            call!!.enqueue(object : Callback<JsonObject?> {
                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                    if (response.body() != null) {
                        ResponseConvert(response.body(), data)
                    }
                }

                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    val p = ""
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return data
    }

    fun providesPermisosWebService(): MutableLiveData<List<Permisos>>
    {
        val data = MutableLiveData<List<Permisos>>()
        try {
            val retrofit = Retrofit.Builder()
                    .baseUrl(UrlServices.UrlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(providesOkHttpClientBuilder())
                    .build()


            //Defining retrofit api service
            val service = retrofit.create(ApiService::class.java)
            val user = User()
            user.usuario = Prefs.getString(PREFS.USUARIO, "gamma")
            val call = service.persmisoApp(user)
            call.enqueue(object : Callback<ResponsePermisosApp?> {
                override fun onResponse(call: Call<ResponsePermisosApp?>, responseApp: Response<ResponsePermisosApp?>) {
                    if (responseApp.body() != null)
                    {
                        ResponseConvertPermsisos(responseApp.body()!!, data)
                    }
                }

                override fun onFailure(call: Call<ResponsePermisosApp?>, t: Throwable) {
                    val p = ""
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return data
    }

    fun providesDashSolicitudesCountWebService(): MutableLiveData<SolicitudesCount> {
        val data = MutableLiveData<SolicitudesCount>()
        try {
            val retrofit = Retrofit.Builder()
                    .baseUrl(UrlServices.UrlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(providesOkHttpClientBuilder())
                    .build()


            //Defining retrofit api service
            val service = retrofit.create(ApiService::class.java)
            val dashBoardRequest = DashBoardRequest()
            dashBoardRequest.usuario = Prefs.getString(PREFS.USUARIO, "gamma")
            dashBoardRequest.tipo = 1
            val call = service.getDataDashBoardTipo(dashBoardRequest)
            call!!.enqueue(object : Callback<DashBoard?> {
                override fun onResponse(call: Call<DashBoard?>, response: Response<DashBoard?>) {
                    if (response.body() != null)
                    {
                        responseConvertDashBoardSolitudesCount(response.body()!!, data)
                    }
                }

                override fun onFailure(call: Call<DashBoard?>, t: Throwable) {
                    val p = ""
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return data
    }

    fun providesDashOrdenesCountWebService(): MutableLiveData<OrdenesCount> {
        val data = MutableLiveData<OrdenesCount>()
        try {
            val retrofit = Retrofit.Builder()
                    .baseUrl(UrlServices.UrlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(providesOkHttpClientBuilder())
                    .build()


            //Defining retrofit api service
            val service = retrofit.create(ApiService::class.java)
            val dashBoardRequest = DashBoardRequest()
            dashBoardRequest.usuario = Prefs.getString(PREFS.USUARIO, "gamma")
            dashBoardRequest.tipo = 2
            val call = service.getDataDashBoardTipo(dashBoardRequest)
            call!!.enqueue(object : Callback<DashBoard?> {
                override fun onResponse(call: Call<DashBoard?>, response: Response<DashBoard?>) {
                    if (response.body() != null) {
                        responseConvertDashBoardOrdenesCount(response.body()!!, data)
                    }
                }

                override fun onFailure(call: Call<DashBoard?>, t: Throwable) {
                    val p = ""
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return data
    }

    fun providesDashCostosMttoWebService(): MutableLiveData<CostosMtto> {
        val data = MutableLiveData<CostosMtto>()
        try {
            val retrofit = Retrofit.Builder()
                    .baseUrl(UrlServices.UrlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(providesOkHttpClientBuilder())
                    .build()


            //Defining retrofit api service
            val service = retrofit.create(ApiService::class.java)
            val dashBoardRequest = DashBoardRequest()
            dashBoardRequest.usuario = Prefs.getString(PREFS.USUARIO, "gamma")
            dashBoardRequest.tipo = 3
            val call = service.getDataDashBoardTipo(dashBoardRequest)
            call!!.enqueue(object : Callback<DashBoard?> {
                override fun onResponse(call: Call<DashBoard?>, response: Response<DashBoard?>) {
                    if (response.body() != null) {
                        responseConvertDashBoardCostosMtto(response.body()!!, data)
                    }
                }

                override fun onFailure(call: Call<DashBoard?>, t: Throwable) {
                    val p = ""
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return data
    }

    fun providesDashBoard():MutableLiveData<DashBoard> {
        val data = MutableLiveData<DashBoard>()
        try {
            val retrofit = Retrofit.Builder()
                    .baseUrl(UrlServices.UrlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(providesOkHttpClientBuilder())
                    .build()


            //Defining retrofit api service
            val service = retrofit.create(ApiService::class.java)
            val user = User()
            user.usuario = Prefs.getString(PREFS.USUARIO, "gamma")
            val call = service.getDataDashBoard1(user)
            call!!.enqueue(object : Callback<DashBoard?> {
                override fun onResponse(call: Call<DashBoard?>, response: Response<DashBoard?>) {
                    if (response.body() != null)
                    {
                        insertDashBoard(response.body()!!)
                        data.value = response.body()//comentar para comprobar estado vacio
                        //ResponseConvertDashBoardSolitudesCount(response.body(), data)
                    }
                }

                override fun onFailure(call: Call<DashBoard?>, t: Throwable) {
                    val p = ""
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return data
    }

    private fun insertDashBoard(body: DashBoard) {
        val solicitudesCountRoomDBRepository = SolicitudesCountRoomDBRepository(application)
        solicitudesCountRoomDBRepository.insertSolicitudesCount(body.sDTSDDashboard!!.solicitudesCount)

        val ordenesCountRoomDBRepository = OrdenesCountRoomDBRepository(application)
        ordenesCountRoomDBRepository.insertOrdenesCount(body.sDTSDDashboard!!.ordenesCount)

        val costosMttoRoomDBRepository = CostosMttoRoomDBRepository(application)
        costosMttoRoomDBRepository.insertCostosMtto(body.sDTSDDashboard!!.costosMtto)
    }



    fun providesConfiguracionesWebService(): LiveData<Configuraciones>
    {
        val data = MutableLiveData<Configuraciones>()
        try {
            val retrofit = Retrofit.Builder()
                    .baseUrl(UrlServices.UrlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(providesOkHttpClientBuilder())
                    .build()


            //Defining retrofit api service
            val service = retrofit.create(ApiService::class.java)
            val call = service.getConfiguraciones()
            call!!.enqueue(object : Callback<JsonObject?> {
                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                    if (response.body() != null) {
                        ResponseConvertConfiguraciones(response.body(), data)
                    }
                }

                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {

                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return data
    }

    private fun ResponseConvertConfiguraciones(jsonObject: JsonObject?, data: MutableLiveData<Configuraciones>)
    {
        val gson = Gson()
        var configuraciones : Configuraciones

        configuraciones = gson.fromJson(jsonObject.toString(), Configuraciones::class.java)
        configuraciones.id = 1
        val configuracionesRoomDBRepository = ConfiguracionesRoomDBRepository(application)
        configuracionesRoomDBRepository.insertConfiguracion(configuraciones)
        data.value = configuraciones
    }


    fun providesOrdenReporteWebService(): MutableLiveData<List<OrdenReporte>?>
    {
        val data = MutableLiveData<List<OrdenReporte>?>()
        try {
            val retrofit = Retrofit.Builder()
                    .baseUrl(UrlServices.UrlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(providesOkHttpClientBuilder())
                    .build()


            //Defining retrofit api service
            val service = retrofit.create(ApiService::class.java)
            val emplId = Prefs.getInt(PREFS.USUARIO_EMPLID,0)
            val numeroOT = Prefs.getString(PREFS.FILTER_ORD_REPORTE_NUMERO,"")
            val estadoOT = Prefs.getInt(PREFS.FILTER_ORD_REPORTE_ESTADO,0)
            val fechaInicio = Prefs.getString(PREFS.FILTER_ORD_REPORTE_FECHAINICIO,"")
            val fechaFin = Prefs.getString(PREFS.FILTER_ORD_REPORTE_FECHAFIN,"")
            val call = service.getTrabajos(emplId)
            call.enqueue(object : Callback<List<OrdenReporte>> {
               override fun onResponse(call: Call<List<OrdenReporte>>, response: Response<List<OrdenReporte>>) {
                    if (response.body()!=null)
                    {
                        if (response.body()!!.isNotEmpty())
                        {
                            val listaOrdenReporte:ArrayList<OrdenReporte> = response.body() as ArrayList<OrdenReporte>
                            val ordenReporteRoomDBRepository = OrdenReporteRoomDBRepository(application)
                            val ordenReporteWSRoomDBRepository = OrdenReporteWSRoomDBRepository(application)

                            listaOrdenReporte.forEach {
                                val ordenReporte = it
                                ordenReporte.numeroOT = it.numeroOT
                                ordenReporte.descripcion = it.descripcion
                                ordenReporte.estadoOT = it.estadoOT
                                ordenReporte.fechacreada = it.fechacreada
                                ordenReporte.fechaInicioSgm = it.fechaInicioSgm
                                ordenReporte.activada = false
                                ordenReporte.ispasuse = false
                                ordenReporte.isplay = false
                                ordenReporteRoomDBRepository.insertOrdenesReporte(ordenReporte)

                                ///inserto local en la tabla ordenesreportews que son las ordenes que hay en el servidor
                                val ordenReporteWS = OrdenReporteWS()
                                ordenReporteWS.numeroOT = ordenReporte.numeroOT
                                ordenReporteWSRoomDBRepository.insertOrdenesReporteWS(ordenReporteWS)
                            }

                        }
                        else
                        {
                            ///elimino las ordenes local que hacen referencias al servidor
                            val ordenReporteWSRoomDBRepository = OrdenReporteWSRoomDBRepository(application)
                            ordenReporteWSRoomDBRepository.deleteOrdenesALLReporteWS()
                        }
                    }
                }

                override fun onFailure(call: Call<List<OrdenReporte>>, t: Throwable) {
                    var p = t.message
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return data
    }


//    fun providesSolicitudesWebService(): LiveData<Solicitudes> {
//        val data = MutableLiveData<Solicitudes>()
//
//        // String response = "";
//        try {
//            val retrofit = Retrofit.Builder()
//                    .baseUrl(ApiService.UrlBase)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(providesOkHttpClientBuilder())
//                    .build()
//
//
//            //Defining retrofit api service
//            val service = retrofit.create(ApiService::class.java)
//
//            val call = service.getDataSolicitudes(5,5)
//            call!!.enqueue(object : Callback<JsonArray?> {
//                override fun onResponse(call: Call<JsonArray?>, response: Response<JsonArray?>) {
//                    if (response.body() != null)
//                    {
//                      //  ResponseConvertSolicitudes(response.body(), data)
//                    }
//                }
//
//                override fun onFailure(call: Call<JsonArray?>, t: Throwable) {
//                    val p = ""
//                }
//            })
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        return data
 //   }

    fun ResponseConvertSolicitudes(jsonObject: JsonObject?, data: MutableLiveData<Solicitudes>)
    {
        val gson = Gson()
        var solicitudes : Solicitudes

        val nuew = jsonObject!!["Solicitudes"].asJsonObject

        solicitudes = gson.fromJson(nuew.toString(), Solicitudes::class.java)
        val solcitudesRoomDBRepository = SolicitudesRoomDBRepository(application)
        solcitudesRoomDBRepository.insertSolicitudes(solicitudes)
        data.value = solicitudes
    }


    private fun ResponseConvert(jsonObject: JsonObject?, data: MutableLiveData<User>) {
        val nuew = jsonObject!!["SDTSDUsuario"].asJsonObject
        val user = User()
        user.usuario = Prefs.getString(PREFS.USUARIO,"")
        user.correo = nuew["correo"].asString
        user.decripcion = nuew["descripcion"].asString
        user.empresa = nuew["nombreEmpresa"].asString
        user.emplId = nuew["emplId"].asInt
        user.empleadoNombre = nuew["emplNombreApell"].asString
        user.id = 1
        val userRoomDBRepository = UserRoomDBRepository(application)
        userRoomDBRepository.insertGlobal(user)
        data.value = user
    }


    private fun ResponseConvertPermsisos(responsePermisos: ResponsePermisosApp, data: MutableLiveData<List<Permisos>>)
    {
        var lisPermisos = ArrayList<Permisos>()
        val permisosRoomDBRepository = PermisosRoomDBRepository(application)
        responsePermisos.responsePermisosApp!!.permisos!!.forEach{

            val permisos = Permisos()
            permisos.opcion = it.opcion
            permisos.add = it.responseSdtPermiso!!.add
            permisos.del = it.responseSdtPermiso!!.del
            permisos.up = it.responseSdtPermiso!!.up
            permisos.ver = it.responseSdtPermiso!!.ver
            permisos.imp = it.responseSdtPermiso!!.imp
            permisos.exec = it.responseSdtPermiso!!.exec
            permisos.acionden = it.responseSdtPermiso!!.acionden
            permisosRoomDBRepository.insertPermisos(permisos)
            lisPermisos.add(permisos)
        }

        data.value = lisPermisos
    }



    private fun responseConvertDashBoardSolitudesCount(dashBoard: DashBoard, data: MutableLiveData<SolicitudesCount>) {
        val solcitudesRoomDBRepository = SolicitudesCountRoomDBRepository(application)
        solcitudesRoomDBRepository.insertSolicitudesCount(dashBoard.sDTSDDashboard!!.solicitudesCount)
        data.value = dashBoard.sDTSDDashboard!!.solicitudesCount
    }

    private fun responseConvertDashBoardOrdenesCount(dashBoard: DashBoard, data: MutableLiveData<OrdenesCount>) {
        val ordenesRoomDBRepository = OrdenesCountRoomDBRepository(application)
        ordenesRoomDBRepository.insertOrdenesCount(dashBoard.sDTSDDashboard!!.ordenesCount)
        data.value = dashBoard.sDTSDDashboard!!.ordenesCount
    }
    private fun responseConvertDashBoardCostosMtto(dashBoard: DashBoard, data: MutableLiveData<CostosMtto>) {
        val costosMttoRoomDBRepository = CostosMttoRoomDBRepository(application)
        costosMttoRoomDBRepository.insertCostosMtto(dashBoard.sDTSDDashboard!!.costosMtto)
        data.value = dashBoard.sDTSDDashboard!!.costosMtto
    }

//    private fun ResponseConvertDashBoardOrdenesCount(jsonObject: JsonObject?, data: MutableLiveData<OrdenesCount>) {
//        val gson = Gson()
//        val sdtsdDashboard = SDTSDDashboard()
//        var ordenesCount : OrdenesCount
//        val nuew1 = jsonObject!!["SDTSDDashboard"].asJsonObject
//        val nuew = nuew1["OrdenesCount"].asJsonObject
//        ordenesCount = gson.fromJson(nuew.toString(), OrdenesCount::class.java)
//        ordenesCount.id = 1
//        val SolcitudesRoomDBRepository = OrdenesCountRoomDBRepository(application)
//        SolcitudesRoomDBRepository.insertOrdenesCount(ordenesCount)
//        sdtsdDashboard.ordenesCount = ordenesCount
//        data.value = ordenesCount
//    }

    companion object {
        private fun providesOkHttpClientBuilder(): OkHttpClient {
            val httpClient: OkHttpClient.Builder
            try {
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts = arrayOf<TrustManager>(
                        object : X509TrustManager {
                            @Throws(CertificateException::class)
                            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                            }

                            @Throws(CertificateException::class)
                            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                            }

                            override fun getAcceptedIssuers(): Array<X509Certificate> {
                                return arrayOf()
                            }
                        }
                )

                // Install the all-trusting trust manager
                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())

                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory
                httpClient = OkHttpClient.Builder()
                httpClient.sslSocketFactory(sslSocketFactory, (trustAllCerts[0] as X509TrustManager))
                httpClient.hostnameVerifier(HostnameVerifier { _, _ -> true })
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
            return httpClient.readTimeout(1200, TimeUnit.SECONDS)
                    .connectTimeout(1200, TimeUnit.SECONDS).build()
        }
    }

}