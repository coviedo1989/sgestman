package com.example.carlos.sgestman.utils.enums

enum class TIPOMENSAJE {
    INFO, SUCCES, ERROR
}