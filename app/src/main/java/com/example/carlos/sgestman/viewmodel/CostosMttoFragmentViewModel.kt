package com.example.carlos.sgestman.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carlos.sgestman.entity.CostosMtto
import com.example.carlos.sgestman.entity.OrdenesCount
import com.example.carlos.sgestman.repository.Databases.CostosMttoRoomDBRepository
import com.example.carlos.sgestman.repository.ws.WebServiceRepository

class CostosMttoFragmentViewModel(application: Application) : AndroidViewModel(application)
{
    private val costosMttoRoomDBRepository:CostosMttoRoomDBRepository = CostosMttoRoomDBRepository (application)
    private val webServiceRepository:WebServiceRepository= WebServiceRepository(application)

    private var _costosMtto = MutableLiveData<CostosMtto>()
    val costosMtto : LiveData<CostosMtto?>?

    init {
        _costosMtto = webServiceRepository.providesDashCostosMttoWebService()
        costosMtto = costosMttoRoomDBRepository.allcostosMtto
    }
}