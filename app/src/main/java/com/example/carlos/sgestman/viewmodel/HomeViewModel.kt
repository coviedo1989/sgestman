package com.example.carlos.sgestman.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carlos.sgestman.entity.Permisos
import com.example.carlos.sgestman.repository.Databases.UserRoomDBRepository
import com.example.carlos.sgestman.repository.ws.WebServiceRepository
import com.example.carlos.sgestman.entity.User
import com.example.carlos.sgestman.repository.Databases.PermisosRoomDBRepository

class HomeViewModel(application: Application) : AndroidViewModel(application)
{

    private val mText: MutableLiveData<String> = MutableLiveData()
    private val webServiceRepository: WebServiceRepository
    private val roomUserDBRepository: UserRoomDBRepository
    private val roomPermisosDBRepository: PermisosRoomDBRepository
    private val mAllUser: LiveData<User?>?
    private val obserUser: LiveData<User>
    private val mAllPermisos:LiveData<List<Permisos>?>?
    private val obserPermisos: MutableLiveData<List<Permisos>>
    val text: LiveData<String>
        get() = mText

    val getAllPermiso:LiveData<List<Permisos>?>?
    get() = mAllPermisos

    fun getmAllUser(): LiveData<User?>? {
        return mAllUser
    }

    init {
        mText.value = "This is home fragment"
        roomUserDBRepository = UserRoomDBRepository(application)
        roomPermisosDBRepository = PermisosRoomDBRepository(application)
        webServiceRepository = WebServiceRepository(getApplication())
        obserUser = webServiceRepository.providesUserWebService()
        mAllUser = roomUserDBRepository.allUser
        obserPermisos = webServiceRepository.providesPermisosWebService()
        mAllPermisos = roomPermisosDBRepository.allPermisos

    }
}