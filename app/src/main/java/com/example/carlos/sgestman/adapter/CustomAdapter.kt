package com.example.carlos.sgestman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.entity.OrdenReporteTiempo
import com.example.carlos.sgestman.entity.ReporteTiempo
import java.util.*

class CustomAdapter(private val context: Context, private val deptList: ArrayList<OrdenReporteTiempo>,private val listener: ItemPlayPauseListener1) : BaseExpandableListAdapter() {
    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        val productList = deptList[groupPosition].listReporteTiempo as ArrayList<ReporteTiempo>
        return productList[childPosition]
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean,
                              view: View, parent: ViewGroup): View {
        var view = view
        val detailInfo = getChild(groupPosition, childPosition) as ReporteTiempo
        if (view == null) {
            val infalInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = infalInflater.inflate(R.layout.item_reportetiempo_details, null)
        }
        //
//        TextView sequence = (TextView) view.findViewById(R.id.sequence);
//        sequence.setText(detailInfo.getSequence().trim() + ". ");
//        TextView childItem = (TextView) view.findViewById(R.id.childItem);
//        childItem.setText(detailInfo.getName().trim());
        return view
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        val productList = deptList[groupPosition].listReporteTiempo as ArrayList<ReporteTiempo>
        return productList.size
    }

    override fun getGroup(groupPosition: Int): Any {
        return deptList[groupPosition].ordenReporte!!
    }

    override fun getGroupCount(): Int {
        return deptList.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isLastChild: Boolean, view: View,
                              parent: ViewGroup): View {
        var view = view
        val headerInfo = getGroup(groupPosition) as OrdenReporte
        if (view == null) {
            val inf = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inf.inflate(R.layout.custom_item_reporte_trabajo, null)
        }
        val heading = view.findViewById<View>(R.id.nroOrden) as TextView
        heading.text = headerInfo.numeroOT
        view.setOnClickListener { }
        val imageView = view.findViewById<ImageView>(R.id.imgDrow2)
        imageView.setOnClickListener {
            if ((parent as ExpandableListView).isGroupExpanded(groupPosition)) {
                imageView.setImageResource(R.drawable.ic_arrow_drop)
                parent.collapseGroup(groupPosition)
            } else {
                imageView.setImageResource(R.drawable.ic_arrow_drop_up)
                parent.expandGroup(groupPosition)
            }
        }
        setupView(view, headerInfo)
        return view
    }

    private fun setupView(view: View, ordenreporte: OrdenReporte) {
        val imgPlay = view.findViewById<ImageView>(R.id.img_play)
        val imgPause = view.findViewById<ImageView>(R.id.img_pause)
        val cardview: CardView = view.findViewById(R.id.cardview)
        imgPlay.setBackgroundResource(R.drawable.ic_play)
        imgPause.setBackgroundResource(R.drawable.ic_pause)
        //cardview.setBackgroundResource(R.drawable.round_item_reporteorden_normal)
        if (ordenreporte.isplay!!) {
            imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
            imgPause.setBackgroundResource(R.drawable.ic_pause)
            cardview.setBackgroundResource(R.drawable.round_item_reporteorden_play)
        }
        if (ordenreporte.ispasuse!!) {
            imgPlay.setBackgroundResource(R.drawable.ic_play)
            imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
            cardview.setBackgroundResource(R.drawable.round_item_reporteorden_pause)
        }
        imgPlay.setOnClickListener {
            if(!ordenreporte.isplay!!)
            {
                imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
                imgPause.setBackgroundResource(R.drawable.ic_pause)
                ordenreporte.activada = true
                ordenreporte.isplay = true
                ordenreporte.ispasuse = false
                listener.playListener1(ordenreporte,doPlay = true,position = 0)

            }
        }
//        imgPause.setOnClickListener {
//            if(!ordenreporte.ispasuse!! and ordenreporte.activada!!)
//            {
//                imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
//                imgPlay.setBackgroundResource(R.drawable.ic_play)
//                ordenreporte.activada = true
//                ordenreporte.ispasuse = true
//                ordenreporte.isplay = false
//                itemPlayPauseListener.pauseListener(ordenreporte,position)
//            }
//
//        }
    }
    private fun siHayOTPlay(mFeedList: List<OrdenReporteTiempo>): Boolean {
        var isplay = false
        mFeedList.forEach {
            if (it.ordenReporte!!.isplay!!) {
                isplay = true
            }
        }
        return  isplay
    }


    override fun hasStableIds(): Boolean {
        return true
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

}
interface ItemPlayPauseListener1
{
    fun playListener1(ordenreporte:OrdenReporte?,doPlay:Boolean,position: Int)

    fun pauseListener1(ordenreporte: OrdenReporte?,position: Int)
}