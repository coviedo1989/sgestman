package com.example.carlos.sgestman

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.carlos.sgestman.actividades.BasicActivity
import com.example.carlos.sgestman.entity.*
import com.example.carlos.sgestman.model.ImageRequest
import com.example.carlos.sgestman.model.ResponseWS
import com.example.carlos.sgestman.model.SolicitudRequest
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.utils.PermisionApp
import com.example.carlos.sgestman.utils.constant.PERMISSION
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.constant.REQUESTCODE
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.FootterDashboardView
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_test.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer


class TestActivity : BasicActivity()
{
    private var fileUri: Uri? = null
    private var img_bitmap: Bitmap? =null
    private lateinit var toolbar: Toolbar
    private var isSelecEjecutor = false
    private var prueba :String? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        setTollbar()

        img_solicitud.setOnClickListener { Img_Onclick_Image_Solicitud() }
       // imageDownEjecutor.setOnClickListener{
          //  autotextEjecutor.showDropDown()}
        var footterDashboardView = findViewById<FootterDashboardView>(R.id.footterDashboardView)
        footterDashboardView.datoSrt = "12222.to"

        probarNull()
    }

    private fun probarNull()
    {
        prueba = "dsd"
        prueba = null
        textView3.text = prueba!!.length.toString()
    }

    fun setTollbar()
    {
        toolbar = findViewById<View>(R.id.my_awesome_toolbar) as Toolbar
        toolbar.title = ""
        toolbar.navigationIcon= ContextCompat.getDrawable(this,R.drawable.ic_left_arrow)
        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        val mtitleToolbar = toolbar.findViewById(R.id.toolbar_title) as TextView
        mtitleToolbar.text = "Test"
    }

    fun Img_Onclick_Image_Solicitud()
    {
        ActivityCompat.requestPermissions(this@TestActivity,arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION.CAMERA)
        var pp = PermisionApp.checkCamera(this) and PermisionApp.checkwriteExternalStoragePermission(this)
        when {
            PermisionApp.checkCamera(this) and PermisionApp.checkreadExternalStoragePermission(this) and PermisionApp.checkwriteExternalStoragePermission(this)-> {
                opcionDialogImagen()
            }
            else ->
            {
                ActivityCompat.requestPermissions(this@TestActivity,arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION.CAMERA)
                //   ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), PERMISSION.CAMERA)
            }
        }
    }
    private fun opcionDialogImagen()
    {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Imagen")
        val items:Array<CharSequence?> = if (img_bitmap != null) {
            arrayOf("     Tomar Foto","     Seleccionar Foto","     Eliminar Foto")
        }
        else {
            arrayOf("     Tomar Foto","     Seleccionar Foto")
        }




        alertDialog.setItems(items,object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int)
            {
                when (which) {
                    0 -> {
                        TomarFoto()
                    }
                    1 -> {
                        SeleccionarFoto()
                    }
                    else -> {EliminarFoto()}
                }
            }
        })
        alertDialog.show()
    }

    private fun EliminarFoto()
    {
        img_bitmap = null
        img_solicitud.setImageBitmap(null)
        img_solicitud.setImageResource(R.drawable.selector_img_galery)
    }

    private fun SeleccionarFoto()
    {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUESTCODE.SELECT_PHOTO_GALERY)
    }

    private fun TomarFoto()
    {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUESTCODE.TAKE_PHOTO_CAPTURE)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == REQUESTCODE.TAKE_PHOTO_CAPTURE)
            {
                img_bitmap  = data?.extras?.get("data") as Bitmap
                img_solicitud.setImageBitmap(img_bitmap)
                fileUri = data.data
            }
            if (requestCode == REQUESTCODE.SELECT_PHOTO_GALERY)
            {
                img_bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, data!!.data)
                img_solicitud.setImageBitmap(img_bitmap)
                fileUri = data.data
            }
        }
    }

    fun EnviarSol(view: View)
    {
        val solicitud = Solicitudes()
        solicitud.descripcion = "pruebaaa"
        solicitud.ejecutorCod = "004"
        solicitud.clienteCod = "SEI"
        solicitud.clasosDenom = "DIAGNOSTICO"
        val obj = ObjetosSol()
        obj.objpatCodigo = "ARF"
        solicitud.Objetos = listOf(obj)




        showProgresBar()

        val solicitudRequest = SolicitudRequest()
        //solicitudRequest.usuario = "GAMMA"
        solicitudRequest.usuario = Prefs.getString(PREFS.USUARIO, "")
        solicitudRequest.solicitud = solicitud
        solicitudRequest.modeAction = SolicitudRequest.MODEACTION.INS


        val prueba = encodeToBase64(img_bitmap!!)
        val imageRequest = ImageRequest()
        imageRequest.prueba = prueba
        val callAdicionarSolicitud = RetrofitClient.instance?.api?.uploadImagen1(imageRequest)
        callAdicionarSolicitud?.enqueue(object: Callback<ResponseWS> {
            override fun onFailure(call: Call<ResponseWS>, t: Throwable)
            {
                showSimpleError(t.message, TIPOMENSAJE.ERROR)
                showProgresBar()
            }

            override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>) {
                if(response.isSuccessful)
                {
                    showProgresBar()
                    val responseWS = response.body()

                    if (responseWS!!.sdtresult?.success!!)
                    {
                       Toast.makeText(applicationContext,"Enviada",Toast.LENGTH_SHORT).show()
                    }
                    else
                    {
                        responseWS.sdtresult?.let { mostarTodosLosMensajes(it) }
                    }
                }
            }
        })
    }

    @NonNull
    fun prepareFilePart( partName: String, fileUri: Uri): MultipartBody.Part
    {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri

        val file = com.example.carlos.sgestman.utils.FileUtils.getFile(this,fileUri)
        // create RequestBody instance from file
        val requestFile:RequestBody =
                RequestBody.create(
                        contentResolver.getType(fileUri)!!.toMediaTypeOrNull(),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name

        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
    }
    fun Bitmap.convertToArray():ByteArray
    {
        val size = this.byteCount
        val buffer = ByteBuffer.allocate(size)
        val bytes = ByteArray(size)

        this.copyPixelsFromBuffer(buffer)
        buffer.rewind()

        buffer.get(bytes)
        return  bytes
    }
    private fun encodeToBase64(bitmap: Bitmap):String
    {
        val byteArrayOutputStream =ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG,30,byteArrayOutputStream)
        val a = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(a,Base64.DEFAULT)
    }
}