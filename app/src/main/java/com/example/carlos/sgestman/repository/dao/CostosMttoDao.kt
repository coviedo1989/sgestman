package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.carlos.sgestman.entity.CostosMtto
import com.example.carlos.sgestman.entity.SolicitudesCount

@Dao
interface CostosMttoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(costosMtto: CostosMtto?)

    @get:Query("SELECT * from CostosMtto")
    val allcostosMtto: LiveData<CostosMtto?>?

    @Query("DELETE FROM costosmtto")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCostosMtto(costosMtto: CostosMtto?)
}