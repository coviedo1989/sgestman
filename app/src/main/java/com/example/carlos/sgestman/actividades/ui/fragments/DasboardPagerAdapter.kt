package com.example.carlos.sgestman.actividades.ui.fragments

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.carlos.sgestman.entity.CostosMtto
import com.example.carlos.sgestman.entity.DashBoard
import com.example.carlos.sgestman.entity.OrdenesCount
import com.example.carlos.sgestman.entity.SolicitudesCount

class DasboardPagerAdapter(fm: FragmentManager?, var dashBoard: DashBoard) : FragmentStatePagerAdapter(fm!!,FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> SolicitudesCountFragment()
            1 -> OrdenesCountFragment()
            2 -> CostosMttoFragment()
           else -> CostosMttoFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Solicitudes"
            1 -> "Órdenes"
            2 -> "Costos"
            else -> null
        }
    }

}