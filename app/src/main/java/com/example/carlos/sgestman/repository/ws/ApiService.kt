package com.example.carlos.sgestman.repository.ws

import com.example.carlos.sgestman.entity.*
import com.example.carlos.sgestman.model.*
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface ApiService
{
    @Headers("Content-Type: application/json")
    @POST("WSSDLogin")
    fun userLogin(
            @Field("usuario") usuario: String?,
            @Field("contrasenna") contrasenna: String?): Call<ResponseWS?>?

    @POST("WSSDLogin")
    fun userLogin1(@Body user: UserLogin?): Call<ResponseWS>

    @POST("WSSDCambioContrasena")
    fun userCambiarPassword(@Body user: UserLogin?): Call<ResponseWS>



    @POST("WSSDDatosUsuario")
    fun getDataUsuario(@Body user: User?): Call<JsonObject?>?

    //prueba conexion
    @GET("dpSDTestConection")
    fun TestConexion(): Call<String?>?

    //dasboard
    @POST("WSSDDashBoard")
    fun getDataDashBoard(@Body user: User?): Call<JsonObject?>?

    @POST("WSSDDashBoard")
    fun getDataDashBoard1(@Body user: User?): Call<DashBoard?>?

    @POST("WSSDDashBoard")
    fun getDataDashBoardTipo(@Body dashBoardRequest: DashBoardRequest?): Call<DashBoard?>?

    @GET("dpSDGetSolicitudes")
    fun getDataSolicitudes(@Query("PageNumber") pageNumber:Int,@Query("PageSize") pageSize:Int): Call<List<Solicitudes> ?>

    @GET("dpSDGetSolicitudes")
    suspend fun getDataSolicitudesAll
            (@Query("PageNumber") pageNumber: Int,
             @Query("PageSize") pageSize: Int,
             @Query("codigo") codigo: String,
             @Query("clienCod") codigoCliente: String,
             @Query("ejecCod") codigoEjecutor: String,
             @Query("objCod") codigoObjeto: String,
             @Query("solEstado") estado: Int,
             @Query("fechaInicio") fechaInicio: String,
             @Query("fechaFin") fechaFin: String): List<Solicitudes>

    @GET("dpSDGetOrdenes")
    suspend fun getDataOrdenesAll
            (@Query("PageNumber") pageNumber: Int,
             @Query("PageSize") pageSize: Int,
             @Query("codigo") codigo: String,
             @Query("numeroOT") numero: String,
             @Query("clienCod") codigoCliente: String,
             @Query("ejecCod") codigoEjecutor: String,
             @Query("objCod") codigoObjeto: String,
             @Query("estado") estado: Int,
             @Query("fechaInicio") fechaInicio: String,
             @Query("fechaFin") fechaFin: String): List<Ordenes>

    @GET("dpSDGetEjecutores")
    fun getEjecutores
            (@Query("usuario") usuario: String,
             @Query("ejecCod") ejecCod: String): Call<List<Ejecutor>>

    @GET("dpSDGetClientes")
    fun getClientes
            (@Query("usuario") usuario: String,
             @Query("clientCod") clientCod: String): Call<List<Cliente>>

    @GET("dpSDGetClasifacionesOS")
    fun getClasificaciones(): Call<List<Clasificaciones>>

    @GET("dpSDConfiguraciones")
    fun getConfiguraciones(): Call<JsonObject?>?

    @GET("dpSDGetTrabajos")
    fun getTrabajos(@Query("emplID") emplID: Int): Call<List<OrdenReporte>>

//    @GET("dpSDGetTrabajos")
//    fun getTrabajos(@Query("emplID") emplID: Int,
//                    @Query("numeroOT") numeroOT: String,
//                    @Query("estado") estado:Int,
//                    @Query("fechaInicio") fechaInicio: String,
//                    @Query("fechaFin") fechaFin: String): Call<List<OrdenReporte>>


    /////////CRUD SOLICITUD
    @POST("WSSDSolicitud")
    fun adicionarSolicitud (@Body solicitudRequest: SolicitudRequest): Call<ResponseWS>

    @POST("WSSDSolicitud")
    fun modificarSolicitud(@Body solicitudRequest: SolicitudRequest): Call<ResponseWS>

    @POST("WSSDSolicitud")
    fun eliminarSolicitud(@Body solicitudRequest: SolicitudRequest): Call<ResponseWS>

    ////////ENVIAR TRABAJO PREVISTO
    @POST("WSSDManoObraPrev")
    fun enviarTrabPrevisto(@Body ordenTrabPrevistoRequest: OrdenTrabPrevistoRequest):Call<ResponseWS>


    @POST("WSSDUploadImagen")
    fun uploadImagen1 (@Body imageRequest: ImageRequest): Call<ResponseWS>


    @POST("WSSDPermisionApp")
    fun persmisoApp (@Body user: User?): Call<ResponsePermisosApp>

}