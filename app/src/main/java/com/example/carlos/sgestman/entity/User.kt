package com.example.carlos.sgestman.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "Usuario")
class User {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id = 0

    @ColumnInfo(name = "usuario")
    var usuario: String? = null

    @ColumnInfo(name = "correo")
    var correo: String? = null

    @ColumnInfo(name = "descripcion")
    var decripcion: String? = null

    @ColumnInfo(name = "empresa")
    var empresa: String? = null

    @ColumnInfo(name = "emplId")
    var emplId: Int? = null

    @ColumnInfo(name = "emplNombreApell")
    var empleadoNombre: String? = null

    @Ignore
    constructor(id: Int, usuario: String?, correo: String?, decripcion: String?, empresa: String?,emplId:Int?,empleadoNombre:String?) {
        this.id = id
        this.usuario = usuario
        this.correo = correo
        this.decripcion = decripcion
        this.empresa = empresa
        this.emplId = emplId
        this.empleadoNombre = empleadoNombre
    }

    constructor() {}

}