package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.entity.OrdenReporteTiempo

@Dao
interface OrdenReporteTiempoDao {

    @get:Transaction
    @get:Query("SELECT * from OrdenReporte where estadoOT = 1 or estadoOT = 3")
    val allOrdenReporteTiempo: LiveData<List<OrdenReporteTiempo>?>?

    @Transaction
    @Query("SELECT * from OrdenReporte where (estadoOT = 1 or estadoOT = 3) and numeroOT = :numeroOt" )
    fun ordenReporteTiempoByNumero(numeroOt: String): LiveData<OrdenReporteTiempo>

}