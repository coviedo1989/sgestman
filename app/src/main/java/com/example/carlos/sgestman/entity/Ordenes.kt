package com.example.carlos.sgestman.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.core.content.ContextCompat
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.example.carlos.sgestman.R
import java.io.Serializable

@Entity(tableName = "Orden")
class Ordenes() : Serializable, Parcelable
{
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    var descripcion: String? = null
    var ejecutorCod: String? = null
    var fecha: String? = null
    var estado:Int? = null
    var codigo: String? = null
    var clienteCod: String? = null
    var empNombre: String? = null
    var clienteDenom: String? = null
    var empCodigo: String? = null
    var ejecutorDenom: String? = null
    var clasosDenom:String? = null

    var objpatCodigo:String? = null
    var objpatDenom:String?= null
    var numeroOT:String? = null

    @Ignore
    var iselected:Boolean = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        descripcion = parcel.readString()
        ejecutorCod = parcel.readString()
        fecha = parcel.readString()
        estado = parcel.readValue(Int::class.java.classLoader) as? Int
        codigo = parcel.readString()
        clienteCod = parcel.readString()
        empNombre = parcel.readString()
        clienteDenom = parcel.readString()
        empCodigo = parcel.readString()
        ejecutorDenom = parcel.readString()
        clasosDenom = parcel.readString()
        objpatCodigo = parcel.readString()
        objpatDenom = parcel.readString()
        numeroOT = parcel.readString()
        iselected = parcel.readByte() != 0.toByte()
    }


    public fun getEstado():String
    {
        var estadoString = ""
        when(estado)
        {
            1-> {
                estadoString = "No Iniciada"
            }
            2-> {
                estadoString = "Permiso Seguridad"
            }
            3-> {
                estadoString = "Vigente"
            }
            4-> {
                estadoString = "Pendiente"
            }
            5-> {
                estadoString = "Terminada"
            }
            6->
            {
                estadoString = "Confirmada"
            }
            7->
            {
                estadoString = "Cancelada"
            }
        }
        return estadoString
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(descripcion)
        parcel.writeString(ejecutorCod)
        parcel.writeString(fecha)
        parcel.writeValue(estado)
        parcel.writeString(codigo)
        parcel.writeString(clienteCod)
        parcel.writeString(empNombre)
        parcel.writeString(clienteDenom)
        parcel.writeString(empCodigo)
        parcel.writeString(ejecutorDenom)
        parcel.writeString(clasosDenom)
        parcel.writeString(objpatCodigo)
        parcel.writeString(objpatDenom)
        parcel.writeString(numeroOT)
        parcel.writeByte(if (iselected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Ordenes> {
        override fun createFromParcel(parcel: Parcel): Ordenes {
            return Ordenes(parcel)
        }

        override fun newArray(size: Int): Array<Ordenes?> {
            return arrayOfNulls(size)
        }
    }
}