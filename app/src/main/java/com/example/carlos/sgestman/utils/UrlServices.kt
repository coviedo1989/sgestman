package com.example.carlos.sgestman.utils

import com.example.carlos.sgestman.utils.constant.PREFS
import com.pixplicity.easyprefs.library.Prefs

object UrlServices {
    @JvmField
    var UrlBase: String = Prefs.getString(PREFS.CONFIG_API_URL,"https://10.0.2.2/SGMWEB_3_0.Net/rest/")
    //var UrlBase: String = Prefs.getString(PREFS.CONFIG_API_URL,"https://192.168.43.46/SGMWEB_3_0.Net/rest/")

    var Ordenes = "WSOrdenesUso"
    var Login = "WSSDLogin"
    var DatosUsuario = "WSSDDatosUsuario"
}