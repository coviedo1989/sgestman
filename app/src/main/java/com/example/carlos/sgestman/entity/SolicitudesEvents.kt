package com.example.carlos.sgestman.entity

sealed class SolicitudesEvents
{
    data class Edit(val solicitudes: Solicitudes):SolicitudesEvents()
    data class Remove(val solicitudes: Solicitudes):SolicitudesEvents()
    object InsertItemHeader: SolicitudesEvents()
}