package com.example.carlos.sgestman.actividades.ui.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.db.williamchart.ExperimentalFeature
import com.db.williamchart.Tooltip
import com.db.williamchart.view.HorizontalBarChartView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.utils.MiValueFormatter
import com.example.carlos.sgestman.entity.SolicitudesCount
import com.example.carlos.sgestman.utils.CustomPieChartRenderer
import com.example.carlos.sgestman.utils.views.FootterDashboardView
import com.example.carlos.sgestman.utils.views.SliderTooltip
import com.example.carlos.sgestman.viewmodel.SolicitudesCountFragmentViewModel
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import kotlinx.android.synthetic.main.solicitudes_count_fragment.*
import java.util.*
import kotlin.collections.LinkedHashMap

@ExperimentalFeature
class SolicitudesCountFragment() : Fragment()
{
    private lateinit var mContex: Context
    private lateinit var lyDatos: LinearLayout
    private lateinit var lyEstate: LinearLayout
    private lateinit var footterDashboardView:FootterDashboardView
    private var horizontalBarSet = linkedMapOf<kotlin.String, Float>()
    private var chart: PieChart? = null
    private var typeface: Typeface? = null
    private var solicitudesCount = SolicitudesCount()
    private lateinit var solicitudesCountFragmentViewModel:SolicitudesCountFragmentViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        solicitudesCountFragmentViewModel = ViewModelProvider(requireActivity()).get(SolicitudesCountFragmentViewModel::class.java)

        val root = inflater.inflate(R.layout.solicitudes_count_fragment, container, false)


        setupView(root)

        getData()

        return root
    }

    private fun setupView(root: View)
    {
        mContex = requireContext()

        lyEstate = root.findViewById(R.id.ly_estate)
        lyDatos = root.findViewById(R.id.ly_datos)

        footterDashboardView = root.findViewById(R.id.footterDashboardView)
        chart = root.findViewById(R.id.any_chart_view1)
    }

    private fun getData() {
        solicitudesCountFragmentViewModel.solicitudesCount?.observe(requireActivity(), androidx.lifecycle.Observer {

            if (it!=null)
            {
                solicitudesCount = it
                updateData()
            }
            else
            {
                checkEmptyState()
            }
        })
    }

    private fun checkEmptyState() {
        if (solicitudesCount.total != 0)
        {
            footterDashboardView.datoSrt = solicitudesCount.total.toString()
            footterDashboardView.title = "Total Solicitudes"
            lyEstate.visibility = View.GONE
            lyDatos.visibility = View.VISIBLE

        }
        else
        {
            lyEstate.visibility = View.VISIBLE
            lyDatos.visibility = View.GONE
        }
    }

    private fun updateData()
    {
        setupChart()
        checkEmptyState()
    }

//    private fun setupHorizontalChart(root: View)
//    {
//        val horizontalBarChartView = root.findViewById<HorizontalBarChartView>(R.id.horizontalBarChartView)
//
//        val tooltip :Tooltip
//        tooltip = SliderTooltip()
//                .also {
//                    it.color = Color.TRANSPARENT
//                    it.textSize = 12f
//                    it.context=requireContext()
//                }
//        horizontalBarChartView.tooltip = tooltip
//        horizontalBarChartView.animation.duration = animationDuration
//
//        horizontalBarSet = setupData()
//        horizontalBarChartView!!.animate(horizontalBarSet)
//        horizontalBarChartView.onDataPointTouchListener = { index, _, _ ->
//           val pp:String =
//                   horizontalBarSet.toList()[index]
//                            .second
//                            .toInt()
//                            .toString()
//            tooltip.Texto(pp)
//        }
//
//    }

    private fun setupData(): LinkedHashMap<kotlin.String, Float>
    {
        if (solicitudesCount.sinAtender != 0) horizontalBarSet["Sin Atender"] = solicitudesCount.sinAtender.toFloat()
        if (solicitudesCount.atendidas != 0) horizontalBarSet["Atendidas"] = solicitudesCount.atendidas.toFloat()
        if (solicitudesCount.iniciadas != 0) horizontalBarSet["Iniciadas"] = solicitudesCount.iniciadas.toFloat()
        if (solicitudesCount.pendientes != 0) horizontalBarSet["Pendientes"] = solicitudesCount.pendientes.toFloat()
        if (solicitudesCount.terminadas != 0) horizontalBarSet["Terminadas"] = solicitudesCount.terminadas.toFloat()
        if (solicitudesCount.cerradas != 0) horizontalBarSet["Cerradas"] = solicitudesCount.cerradas.toFloat()
        if (solicitudesCount.canceladas != 0) horizontalBarSet["Canceladas"] = solicitudesCount.canceladas.toFloat()

        val otro = horizontalBarSet.toMap().toList().sortedBy { (_, value) -> value }.toMap()
        horizontalBarSet = otro as LinkedHashMap<String, Float>
        return horizontalBarSet
    }

    private fun setupChart()
    {
        typeface = mContex.resources.getFont(R.font.lato_light)

        chart!!.renderer = CustomPieChartRenderer(chart!!, 10f,mContex)
        chart!!.setUsePercentValues(true)
        chart!!.description.isEnabled = false

        // chart.setCenterTextTypeface(tfLight);
        chart!!.centerText = generateCenterSpannableText()
        chart!!.isDrawHoleEnabled = true
        chart!!.setHoleColor(Color.TRANSPARENT)
        chart!!.setTransparentCircleColor(Color.WHITE)
        chart!!.setTransparentCircleAlpha(110)
        chart!!.holeRadius = 0f //radio
        chart!!.transparentCircleRadius = 0f

        //chart.setDrawCenterText(true);
        chart!!.isRotationEnabled = true
        chart!!.isHighlightPerTapEnabled = true

        //chart.setMaxAngle(180f); // HALF CHART
        chart!!.rotationAngle = 0f
        chart!!.setCenterTextOffset(0f, 0f)
        setData()
        chart!!.animateY(1400, Easing.EaseInOutQuad)
        val l = chart!!.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.isWordWrapEnabled = true
        l.setDrawInside(false)
        l.textSize = 16f
        l.textColor = mContex.getColor(R.color.charleyenda)
        l.typeface = typeface
       // l.xOffset = 40f
        chart!!.setTouchEnabled(true)
        chart!!.setDrawEntryLabels(false)
        chart!!.setExtraOffsets(25f, 0f, 25f, 0f)
        // entry label styling
        chart!!.setEntryLabelColor(Color.BLACK)
        //  chart.setEntryLabelTypeface(tfRegular);
        chart!!.setEntryLabelTextSize(14f)
        chart!!.setEntryLabelTypeface(typeface)


        chart!!.invalidate()
    }

    private fun setData() {
        val values = ArrayList<PieEntry>()
        if (solicitudesCount.sinAtender != 0) values.add(PieEntry(solicitudesCount.sinAtender.toFloat(), estadosSol[0]))
        if (solicitudesCount.atendidas != 0) values.add(PieEntry(solicitudesCount.atendidas.toFloat(), estadosSol[1]))
        if (solicitudesCount.iniciadas != 0) values.add(PieEntry(solicitudesCount.iniciadas.toFloat(), estadosSol[2]))
        if (solicitudesCount.pendientes != 0) values.add(PieEntry(solicitudesCount.pendientes.toFloat(), estadosSol[3]))
        if (solicitudesCount.terminadas != 0) values.add(PieEntry(solicitudesCount.terminadas.toFloat(), estadosSol[4]))
        if (solicitudesCount.cerradas != 0) values.add(PieEntry(solicitudesCount.cerradas.toFloat(), estadosSol[5]))
        if (solicitudesCount.canceladas != 0) values.add(PieEntry(solicitudesCount.canceladas.toFloat(), estadosSol[6]))
        val dataSet = PieDataSet(values, "")
        dataSet.sliceSpace = 1f //separacion entre pedazos del pastel
        setColorDataSet(dataSet)

        //esto pone los titulos afuera
        dataSet.valueLinePart1OffsetPercentage = 90f
        dataSet.valueLinePart1Length = 1.01f
        dataSet.valueLinePart2Length = .5f
        dataSet.valueLineWidth = 2f
        dataSet.valueTextColor = Color.BLACK
        dataSet.xValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
        dataSet.yValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
        dataSet.valueTypeface = typeface
        dataSet.valueLineColor = mContex.getColor(R.color.charLinePiechar)
        val data = PieData(dataSet)
        data.setValueFormatter(MiValueFormatter())
        data.setValueTextSize(12f)
        data.setValueTextColor(mContex.getColor(R.color.charleyenda))
        //data.setValueTypeface(tfLight);
        chart!!.data = data

        //chart!!.invalidate()
    }

    private fun setColorDataSet(dataSet: PieDataSet) {
        val colors: MutableList<Int> = ArrayList()
        val arrayCOlor = mContex.resources.getIntArray(R.array.ChartSolicitude)
        for (value in arrayCOlor) {
            colors.add(value)
        }
        dataSet.colors = colors
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    private fun generateCenterSpannableText(): SpannableString {
        val s = SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda")
        s.setSpan(RelativeSizeSpan(1.7f), 0, 14, 0)
        s.setSpan(StyleSpan(Typeface.NORMAL), 14, s.length - 15, 0)
        s.setSpan(ForegroundColorSpan(Color.GRAY), 14, s.length - 15, 0)
        s.setSpan(RelativeSizeSpan(.8f), 14, s.length - 15, 0)
        s.setSpan(StyleSpan(Typeface.ITALIC), s.length - 14, s.length, 0)
        s.setSpan(ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length - 14, s.length, 0)
        return s
    }

    protected val months = arrayOf(
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"
    )
    protected val estadosSol = arrayOf(
            "Sin Atender", "Atendidas", "Iniciadas", "Pendientes", "Terminadas", "Cerradas", "Canceladas"
    )

    companion object {
        private val lineSet = linkedMapOf(
                "label1" to 5f,
                "label2" to 4.5f,
                "label3" to 4.7f,
                "label4" to 3.5f,
                "label5" to 3.6f,
                "label6" to 7.5f,
                "label7" to 7.5f,
                "label8" to 10f,
                "label9" to 5f,
                "label10" to 6.5f,
                "label11" to 3f,
                "label12" to 4f
        )

        private val barSet = linkedMapOf(
                "JAN" to 4F,
                "FEB" to 7F,
                "MAR" to 2F,
                "MAY" to 2.3F,
                "APR" to 5F,
                "JUN" to 4F
        )



        private val donutSet = listOf(
                20f,
                80f,
                100f
        )
        private const val animationDuration = 1000L
    }
}