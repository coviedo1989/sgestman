package com.example.carlos.sgestman.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "SolicitudesCount")
class SolicitudesCount {
    @PrimaryKey(autoGenerate = true)
    var id = 0
    @SerializedName("Total")
    var total = 0

    var cerradas = 0
    var atendidas = 0
    var sinAtender = 0
    var canceladas = 0
    var iniciadas = 0
    var pendientes = 0
    var terminadas = 0

}