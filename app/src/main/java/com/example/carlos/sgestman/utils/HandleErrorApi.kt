package com.example.carlos.sgestman.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.utils.constant.PREFS
import com.pixplicity.easyprefs.library.Prefs
import java.net.ConnectException
import java.util.concurrent.TimeoutException


class HandleErrorApi (var context: Context, var throwable: Throwable)
{
    private var mensaje:String? = null

    fun mensaje():String
    {



        mensaje = when (throwable) {
            is TimeoutException -> {
                context.getString(R.string.error_msg_timeout)
            }
            is ConnectException -> {
                String.format(context.getString(R.string.error_msg_conection),Prefs.getString(PREFS.CONFIG_API_URL,""))
            }
            else -> {
                throwable.message
            }
        }
        return mensaje!!
    }
    
}