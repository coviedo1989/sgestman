package com.example.carlos.sgestman.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.databinding.ItemSolicitudesBinding
import com.example.carlos.sgestman.entity.Solicitudes
import com.example.carlos.sgestman.utils.MiFecha

class SolicitudesAdapterTest(private val contex: Context, private val cellAdpaterListener: SolcitudAdpaterListenerTest):PagingDataAdapter<Solicitudes, SolicitudesViewHolderTest>(DIFF_CALLBACK){
    private var lastPosition = -1

    override fun onBindViewHolder(holder: SolicitudesViewHolderTest, position: Int) {
        val concert: Solicitudes? = getItem(position)

        // Note that "concert" is a placeholder if it's null.
        holder.bindTo(concert)
        holder.itemView.setOnClickListener{
            cellAdpaterListener.onClickSolicitudListener(concert)
        }
        holder.itemView.setOnLongClickListener{
            cellAdpaterListener.onLongSolicitudListener(concert,position)
            false
        }
        setAnimation(holder.itemView, position)

    }

    fun editarSolicitudAdapter(position: Int, solicitudes: Solicitudes?)
    {
        getItem(position).let { it!!.descripcion = solicitudes!!.descripcion }
        notifyItemChanged(position)
    }
    fun eliminarSolicitudAdapter(position: Int, solicitudes: Solicitudes?)
    {
        notifyItemRemoved(position)
    }
    fun adicioanarSolicitudAdapter(position: Int, solicitudes: Solicitudes?)
    {
        notifyItemInserted(position)
    }
    companion object
    {
        private val DIFF_CALLBACK = object :
                DiffUtil.ItemCallback<Solicitudes>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(oldConcert: Solicitudes,
                                         newConcert: Solicitudes) = oldConcert.id == newConcert.id


            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldConcert: Solicitudes, newConcert: Solicitudes) = oldConcert == newConcert
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SolicitudesViewHolderTest
    {
        return SolicitudesViewHolderTest.from(parent)
    }

    override fun getItemCount(): Int
    {
        return super.getItemCount()
    }



    override fun onViewDetachedFromWindow(holder: SolicitudesViewHolderTest)
    {
        holder.clearAnimation(holder)
        super.onViewDetachedFromWindow(holder)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            val animation: Animation = AnimationUtils.loadAnimation(contex, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

}
class SolicitudesViewHolderTest(private val binding: ItemSolicitudesBinding, private val contex: Context) : RecyclerView.ViewHolder(binding.root)
{

    private var nameView = itemView.findViewById<TextView>(R.id.nroSolicitude)
    var solicitudes : Solicitudes? = null

    /**
     * Items might be null if they are not paged in yet. PagedListAdapter will re-bind the
     * ViewHolder when Item is loaded.
     */
    fun bindTo(solicitudes: Solicitudes?) {
        this.solicitudes = solicitudes
        binding.solicitudes = solicitudes
        binding.fecha = MiFecha()

        binding.imgEstado.background = ContextCompat.getDrawable(contex,R.drawable.circle_estado)

        if(binding.solicitudes?.Objetos?.size == 1)
        {
            binding.solObjetos.text = String.format("Objeto: %s",binding.solicitudes?.Objetos!![0].objpatCodigo)
        }
        else
        {
            var objetos=""
            binding.solicitudes?.Objetos?.forEach {
                objetos+=  it.objpatCodigo + ", "

            }
            objetos = objetos.subSequence(0,objetos.length-2).toString()
            binding.solObjetos.text = String.format("Objetos: %s",objetos)
        }

        when(binding.solicitudes!!.estado)
        {
            1-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_SinAtender)
                binding.estado.text = String.format("Sin Atender")
            }
            2-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Atendidas)
                binding.estado.text = String.format("Atendida")
            }
            3-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Iniciada)
                binding.estado.text = String.format("Iniciada")
            }
            4-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Pendientes)
                binding.estado.text = String.format("Pendiente")
            }
            5-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Terminadas)
                binding.estado.text = String.format("Terminada")
            }
            6->
            {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Cerradas)
                binding.estado.text = String.format("Cerrada")
            }
            7->
            {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Cerradas)
                binding.estado.text = String.format("Cancelada")
            }
        }
       binding.executePendingBindings()


     //   nameView.text = solicitudes?.codigo
    }

    fun clearAnimation(holder: SolicitudesViewHolderTest)
    {
        holder.itemView.clearAnimation()
    }

    companion object {
        fun from(parent: ViewGroup): SolicitudesViewHolderTest
        {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemSolicitudesBinding.inflate(layoutInflater, parent, false)
            return SolicitudesViewHolderTest(binding,parent.context )
        }
    }
}

interface SolcitudAdpaterListenerTest
{
    fun onClickSolicitudListener(solicitudes: Solicitudes?)

    fun onLongSolicitudListener(solicitudes: Solicitudes?,position: Int)
}
