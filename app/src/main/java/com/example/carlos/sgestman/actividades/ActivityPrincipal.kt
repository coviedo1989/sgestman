package com.example.carlos.sgestman.actividades

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.entity.Configuraciones
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.viewmodel.PrincipalViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.pixplicity.easyprefs.library.Prefs

class ActivityPrincipal : AppCompatActivity() {

    var configuraciones:Configuraciones? = null
    private val INTERVALO = 2000 //2 segundos para salir

    private var tiempoPrimerClick: Long = 0
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
        SetupView()
    }

    private fun SetupView() {
        val toolbar = supportActionBar
        if (toolbar != null) {
            toolbar.elevation = 0f
            toolbar.title = ""
            //toolbar.setBackgroundDrawable(getResources().getDrawable(R.color.lock_screen_background_gradient_start,null));
        }
        val navView = findViewById<BottomNavigationView>(R.id.nav_view)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_perfil, R.id.navigation_notifications)
                .build()
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        // NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController)

        setupViewModel()
    }

    private fun setupViewModel() {
        val model = PrincipalViewModel(application)
        model.getmAllConfiguraciones()?.observe(this, Observer
        {
            if(it != null)
            {
                Prefs.putBoolean(PREFS.CONFIGURACION_SOL_IS_EJECUTOR,it.usarEjecutorSolicidut!!)
                Prefs.putBoolean(PREFS.CONFIGURACION_SOL_IS_CLASIFICACION,it.usarClasificacionSolicitud!!)
            }
        })
        model.getmAllConfiguraciones()?.observe(this, Observer { it })
    }

    override fun onBackPressed() {

        if (tiempoPrimerClick + INTERVALO > System.currentTimeMillis())
        {
            finish()
            return
        }
        else
        {
            Toast.makeText(this, "Vuelve a presionar para salir", Toast.LENGTH_SHORT).show()
        }
        tiempoPrimerClick = System.currentTimeMillis()
    }
}