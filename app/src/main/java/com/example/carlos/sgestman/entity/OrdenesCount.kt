package com.example.carlos.sgestman.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "OrdenesCount")
class OrdenesCount {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @SerializedName ("Total")
    var total = 0

    var seguridad = 0
    var confirmada = 0
    var canceladas = 0
    var vigentes = 0
    var pendientes = 0
    var terminadas = 0
    var noIniciadas = 0

}