package com.example.carlos.sgestman.model

import com.google.gson.annotations.SerializedName

class ResponseOpcionPermisos {

    var opcion:String? =null

    @SerializedName("sdtpermisos")
    var responseSdtPermiso:ResponseSdtPermiso? = null
}