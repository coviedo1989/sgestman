package com.example.carlos.sgestman.utils.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.example.carlos.sgestman.R

/**
 * TODO: document your custom view class.
 */
class FootterDashboardView : LinearLayout {

    private lateinit var txtDato: TextView
    private lateinit var txtTitle: TextView
    private var _exampleString: String? = null // TODO: use a default from R.string...
    private var _exampleColor: Int = Color.RED // TODO: use a default from R.color...
    private var _exampleDimension: Float = 0f // TODO: use a default from R.dimen...
    private var _datoSrt:String = "0"
    private var _title:String = "Total"
    private lateinit var textPaint: TextPaint
    private var textWidth: Float = 0f
    private var textHeight: Float = 0f

    /**
     * The text to draw
     */
    var exampleString: String?
        get() = _exampleString
        set(value) {
            _exampleString = value
            //invalidateTextPaintAndMeasurements()
        }

    /**
     * The font color
     */
    var exampleColor: Int
        get() = _exampleColor
        set(value) {
            _exampleColor = value
            //invalidateTextPaintAndMeasurements()
        }

    /**
     * In the example view, this dimension is the font size.
     */
    var exampleDimension: Float
        get() = _exampleDimension
        set(value) {
            _exampleDimension = value
          //  invalidateTextPaintAndMeasurements()
        }

    var datoSrt:String
    get()= _datoSrt
    set(value) {
        _datoSrt = value
        txtDato.text = _datoSrt
    }
    var title:String
    get()= _title
    set(value) {
        _title = value
        txtTitle.text = _title
    }
    /**
     * In the example view, this drawable is drawn above the text.
     */
    var exampleDrawable: Drawable? = null

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }
    constructor(context: Context, attrs: AttributeSet, defStyle: Int,defRes:Int):super(context, attrs, defStyle,defRes)
    {
        init(attrs, defStyle)

    }
    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.FootterDashboardView, defStyle, 0)

        _exampleString = a.getString(
                R.styleable.FootterDashboardView_exampleString)
        _exampleColor = a.getColor(
                R.styleable.FootterDashboardView_exampleColor,
                exampleColor)
        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
        // values that should fall on pixel boundaries.
        _exampleDimension = a.getDimension(
                R.styleable.FootterDashboardView_exampleDimension,
                exampleDimension)

        if (a.hasValue(R.styleable.FootterDashboardView_exampleDrawable)) {
            exampleDrawable = a.getDrawable(
                    R.styleable.FootterDashboardView_exampleDrawable)
            exampleDrawable?.callback = this
        }

        val view = inflate(context,R.layout.footerdashboardcutomview,this)

        txtDato = view.findViewById(R.id.dato)
        txtTitle = view.findViewById(R.id.title)
        val lineCenter = view.findViewById<View>(R.id.lineCenter)
        val lineBootom = view.findViewById<View>(R.id.lineBootom)

        lineBootom.setBackgroundResource(R.color.colorPrimary)
        lineCenter.setBackgroundResource(R.color.colorPrimary)

        txtDato.text = datoSrt
        txtTitle.text = _title

        a.recycle()

        // Set up a default TextPaint object
//        textPaint = TextPaint().apply {
//            flags = Paint.ANTI_ALIAS_FLAG
//            textAlign = Paint.Align.LEFT
//        }

        // Update TextPaint and text measurements from attributes
//        invalidateTextPaintAndMeasurements()
    }

    private fun invalidateTextPaintAndMeasurements() {
        textPaint.let {
            it.textSize = exampleDimension
            it.color = exampleColor
            textWidth = it.measureText(exampleString)
            textHeight = it.fontMetrics.bottom
        }
    }

//    override fun onDraw(canvas: Canvas) {
//        super.onDraw(canvas)
//
//        // TODO: consider storing these as member variables to reduce
//        // allocations per draw cycle.
//        val paddingLeft = paddingLeft
//        val paddingTop = paddingTop
//        val paddingRight = paddingRight
//        val paddingBottom = paddingBottom
//
//        val contentWidth = width - paddingLeft - paddingRight
//        val contentHeight = height - paddingTop - paddingBottom
//
//        exampleString?.let {
//            // Draw the text.
//            canvas.drawText(it,
//                    paddingLeft + (contentWidth - textWidth) / 2,
//                    paddingTop + (contentHeight + textHeight) / 2,
//                    textPaint)
//        }
//
//        // Draw the example drawable on top of the text.
//        exampleDrawable?.let {
//            it.setBounds(paddingLeft, paddingTop,
//                    paddingLeft + contentWidth, paddingTop + contentHeight)
//            it.draw(canvas)
//        }
//    }
}