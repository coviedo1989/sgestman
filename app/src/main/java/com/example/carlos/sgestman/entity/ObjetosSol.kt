package com.example.carlos.sgestman.entity

import androidx.room.Entity
import java.io.Serializable

//@Entity(tableName = "SolObjetos")
class ObjetosSol : Serializable
{
    var objpatCodigo:String? = null
    var objpatDenom:String?= null
    var numeroOT:Int? = null
}