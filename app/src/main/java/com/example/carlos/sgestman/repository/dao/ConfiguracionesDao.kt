package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.carlos.sgestman.entity.Configuraciones

@Dao
interface ConfiguracionesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(global: Configuraciones?)

    @get:Query("SELECT * from Configuracion")
    val allGlobal: LiveData<Configuraciones?>?

    @Query("DELETE FROM Configuracion")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGlobal(resultModel: Configuraciones?)
}