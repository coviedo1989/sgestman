package com.example.carlos.sgestman.model

import com.google.gson.annotations.SerializedName

class ResponseSdtresultMensajes
{
    @SerializedName("Type")
    var type = 0
    @SerializedName("Description")
    var description: String? = null
    @SerializedName("Id")
    var id: String? = null

}