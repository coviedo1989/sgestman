package com.example.carlos.sgestman.model

import com.google.gson.annotations.SerializedName

class ResponseSdtPermiso {


    @SerializedName("Add")
    var add:Int? = null

    @SerializedName("Up")
    var up:Int? = null

    @SerializedName("Del")
    var del:Int? = null

    @SerializedName("Ver")
    var ver:Int? = null

    @SerializedName("Imp")
    var imp:Int? = null

    @SerializedName("Exec")
    var exec:Int? = null

    @SerializedName("AccDen")
    var acionden:Int? = null
}