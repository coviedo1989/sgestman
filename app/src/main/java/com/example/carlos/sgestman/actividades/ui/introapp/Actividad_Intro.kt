package com.example.carlos.sgestman.actividades.ui.introapp

import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.carlos.sgestman.actividades.ui.introapp.Fragment_Intro5.EntraApp_Listener
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.utils.views.BlurBuilder
import com.rd.PageIndicatorView
import com.rd.animation.type.AnimationType
import java.util.*

class Actividad_Intro : AppCompatActivity(), EntraApp_Listener {
    /**
     * The [PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    /**
     * The [ViewPager] that will host the section contents.
     */
    private var mViewPager: ViewPager? = null
    private var pageIndicatorView: PageIndicatorView? = null
    private var img_blur: ImageView? = null
    private val rootView: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_actividad__intro)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        img_blur = findViewById<View>(R.id.img_blur) as ImageView
        val resultBmp = BlurBuilder.blur(this, BitmapFactory.decodeResource(resources, R.drawable.fondo_intro))
        img_blur!!.setImageBitmap(resultBmp)
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        mViewPager = findViewById<View>(R.id.pager_slide_intro) as ViewPager
        pageIndicatorView = findViewById<View>(R.id.page_indicator) as PageIndicatorView
        mViewPager!!.adapter = mSectionsPagerAdapter
        pageIndicatorView!!.setViewPager(mViewPager)
        pageIndicatorView!!.setAnimationType(AnimationType.SLIDE)
    }

    override fun EntraApp(entra: Boolean) {
        if (entra) {
            finish()
        }
    }

    inner class SectionsPagerAdapter(private val mFragmentManager: FragmentManager) : FragmentPagerAdapter(mFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val mFragmentTags: MutableMap<Int, String?>
        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //Toast.makeText(getApplicationContext(), "Posicion get item: "+ position, Toast.LENGTH_SHORT).show();
            var fragment = Fragment()
            when (position) {
                0 -> fragment = Fragment_Intro1()
                1 -> fragment = Fragment_Intro2()
                2 -> fragment = Fragment_Intro3()
                3 -> fragment = Fragment_Intro4()
                4 -> fragment = Fragment_Intro5()
                else -> {
                }
            }
            return fragment
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val obj = super.instantiateItem(container, position)
            if (obj is Fragment) {
                val tag = obj.tag
                mFragmentTags[position] = tag
            }
            return obj
        }

        fun getFragment(position: Int): Fragment? {
            val tag = mFragmentTags[position] ?: return null
            return mFragmentManager.findFragmentByTag(tag)
        }

        override fun getCount(): Int {
            // Show 4 total pages.
            return 5
        }

        init {
            mFragmentTags = HashMap()
        }
    } //    @Override
    //    protected void attachBaseContext(Context newBase) {
    //        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    //    }
}