package com.example.carlos.sgestman.viewmodel

import android.content.Intent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.carlos.sgestman.actividades.ActivityPrincipal
import com.example.carlos.sgestman.entity.User
import com.example.carlos.sgestman.model.ResponseSdtresult
import com.example.carlos.sgestman.model.ResponseSdtresultMensajes
import com.example.carlos.sgestman.model.ResponseWS
import com.example.carlos.sgestman.model.UserLogin
import com.example.carlos.sgestman.repository.ws.RetrofitClient
import com.example.carlos.sgestman.utils.HandleErrorApi
import com.example.carlos.sgestman.utils.constant.PREFS
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.utils.views.CustomProgressDialog
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class IniciarSesionViewModel:ViewModel()
{
    private val _usuario = MutableLiveData<User>()
    val usuario : LiveData<User> get() = _usuario

    private val _responseWS = MutableLiveData<ResponseWS>()
    val responseWS : LiveData<ResponseWS> get() = _responseWS

    private val _progrevisibility = MutableLiveData<Boolean>()
    val progrevisibility : LiveData<Boolean> get() = _progrevisibility


    fun onButtonClikListener(user:String,pass:String,progreesBar:CustomProgressDialog)
    {
        val usuario = UserLogin()
        usuario.usuario = user
        usuario.contrasenna = pass
        showProgreesBar(progreesBar)
        viewModelScope.launch {
            _progrevisibility.value = true
            val call1 = RetrofitClient.instance?.api?.userLogin1(usuario)
            call1?.enqueue(object : Callback<ResponseWS>{
                override fun onResponse(call: Call<ResponseWS>, response: Response<ResponseWS>)
                {
                    showProgreesBar(progreesBar)
                    if (response.isSuccessful)
                    {
                        val responseWS = response.body()
                        _responseWS.value = responseWS
                        if (responseWS!!.sdtresult?.success!!)
                        {
                            Prefs.putString(PREFS.USUARIO, user)
                            Prefs.putString(PREFS.CONTRASENA, pass)

                        }
//                        else
//                        {
//
//                            //responseWS.sdtresult?.let { mostarTodosLosMensajes(it) }
//                        }
                    }
                    //showProgresBar()
                }

                override fun onFailure(call: Call<ResponseWS>, t: Throwable)
                {
                    showProgreesBar(progreesBar)
                    _progrevisibility.value = false
                    val responseWS = ResponseWS()
                    val responseSdtresult = ResponseSdtresult()
                    val listMensaje = ArrayList<ResponseSdtresultMensajes>()
                    val responseSdtresultMensajes = ResponseSdtresultMensajes()
                    responseSdtresultMensajes.description = t.message
                    listMensaje.add(responseSdtresultMensajes)
                    responseSdtresult.setMensajes(listMensaje)
                    responseSdtresult.success = false
                    responseWS.sdtresult = responseSdtresult
                    _responseWS.value = responseWS
                    //showProgresBar()
                    //showSimpleError(HandleErrorApi(applicationContext,t).mensaje(), TIPOMENSAJE.ERROR)
                }
            })

        }
    }

    fun showProgreesBar(customProgressDialog: CustomProgressDialog)
    {
        if(customProgressDialog.dialog.isShowing)
        {
            customProgressDialog.dimis()
        }
        else
        {
            customProgressDialog.show("Espere...")
        }
    }
}
