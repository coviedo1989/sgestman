package com.example.carlos.sgestman.actividades.ui.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.utils.CustomPieChartRenderer
import com.example.carlos.sgestman.utils.MiValueFormatter
import com.example.carlos.sgestman.entity.OrdenesCount
import com.example.carlos.sgestman.utils.views.FootterDashboardView
import com.example.carlos.sgestman.viewmodel.OrdenesCountFragmentViewModel
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import kotlin.collections.ArrayList

class OrdenesCountFragment() : Fragment() {
    private var ordenesCount =  OrdenesCount()
    private lateinit var ordenesCountFragmentViewModel: OrdenesCountFragmentViewModel
    private var chart: PieChart? = null
    private var typeface: Typeface? = null

    private lateinit var mContex: Context
    private lateinit var lyDatos: LinearLayout
    private lateinit var lyEstate: LinearLayout
    private lateinit var footterDashboardView: FootterDashboardView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        ordenesCountFragmentViewModel = ViewModelProvider(requireActivity()).get(OrdenesCountFragmentViewModel::class.java)

        val root = inflater.inflate(R.layout.ordenes_count_fragment, container, false)
        setupView(root)
        getData()
        return root
    }

    private fun setupView(root: View) {
        mContex = requireContext()

        lyEstate = root.findViewById(R.id.ly_estate)
        lyDatos = root.findViewById(R.id.ly_datos)

        footterDashboardView = root.findViewById(R.id.footterDashboardView)
        chart = root.findViewById(R.id.any_chart_view1)
    }
    private fun getData() {
        ordenesCountFragmentViewModel.ordenesCount!!.observe(requireActivity(), androidx.lifecycle.Observer {

            if (it!=null)
            {
                ordenesCount = it
                updateData()
            }
            else
            {
                checkEmptyState()
            }
        })
    }

    private fun checkEmptyState() {
        if (ordenesCount.total != 0)
        {
            footterDashboardView.datoSrt = ordenesCount.total.toString()
            footterDashboardView.title = "Total Órdenes"
            lyEstate.visibility = View.GONE
            lyDatos.visibility = View.VISIBLE

        }
        else
        {
            lyEstate.visibility = View.VISIBLE
            lyDatos.visibility = View.GONE
        }
    }
    private fun updateData()
    {
        setupChart()
        checkEmptyState()
    }

    private fun setupChart() {
        val context = requireContext()
        typeface = context.resources.getFont(R.font.lato_light)
        chart!!.renderer = CustomPieChartRenderer(chart!!, 10f,requireContext())
        chart!!.setUsePercentValues(true)
        chart!!.description.isEnabled = false

        // chart.setCenterTextTypeface(tfLight);
        chart!!.centerText = generateCenterSpannableText()
        chart!!.isDrawHoleEnabled = true
        chart!!.setHoleColor(Color.TRANSPARENT)
        chart!!.setTransparentCircleColor(Color.WHITE)
        chart!!.setTransparentCircleAlpha(110)
        chart!!.holeRadius = 0f //radio
        chart!!.transparentCircleRadius = 0f

        //chart.setDrawCenterText(true);
        chart!!.isRotationEnabled = true
        chart!!.isHighlightPerTapEnabled = true

        //chart.setMaxAngle(180f); // HALF CHART
        chart!!.rotationAngle = 0f
        setData()
        chart!!.animateY(1400, Easing.EaseInOutQuad)
        val l = chart!!.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.isWordWrapEnabled = true
        l.setDrawInside(false)
        l.textSize = 16f
        l.textColor = requireContext().getColor(R.color.charleyenda)
        l.typeface = typeface
        chart!!.setTouchEnabled(true)
        chart!!.setDrawEntryLabels(false)
        chart!!.setExtraOffsets(25f, 0f, 25f, 0f)
        // entry label styling
        chart!!.setEntryLabelColor(Color.BLACK)
        //  chart.setEntryLabelTypeface(tfRegular);
        chart!!.setEntryLabelTextSize(14f)
        chart!!.setEntryLabelTypeface(typeface)
    }

    private fun setData() {
        val values = ArrayList<PieEntry>()
        if (ordenesCount.noIniciadas != 0) values.add(PieEntry(ordenesCount.noIniciadas.toFloat(), estadosOT[0]))
        if (ordenesCount.seguridad != 0) values.add(PieEntry(ordenesCount.seguridad.toFloat(), estadosOT[1]))
        if (ordenesCount.vigentes != 0) values.add(PieEntry(ordenesCount.vigentes.toFloat(), estadosOT[2]))
        if (ordenesCount.pendientes != 0) values.add(PieEntry(ordenesCount.pendientes.toFloat(), estadosOT[3]))
        if (ordenesCount.terminadas != 0) values.add(PieEntry(ordenesCount.terminadas.toFloat(), estadosOT[4]))
        if (ordenesCount.confirmada != 0) values.add(PieEntry(ordenesCount.confirmada.toFloat(), estadosOT[5]))
        if (ordenesCount.canceladas != 0) values.add(PieEntry(ordenesCount.canceladas.toFloat(), estadosOT[6]))
        val dataSet = PieDataSet(values, "")
        dataSet.sliceSpace = 1f //separacion entre pedazos del pastel
        setColorDataSet(dataSet)

        //esto pone los titulos afuera
        dataSet.valueLinePart1OffsetPercentage = 90f
        dataSet.valueLinePart1Length = 1.01f
        dataSet.valueLinePart2Length = .5f
        dataSet.valueLineWidth = 2f
        dataSet.valueTextColor = Color.BLACK
        dataSet.xValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
        dataSet.yValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
        dataSet.valueTypeface = typeface
        dataSet.valueLineColor = requireContext().getColor(R.color.charLinePiechar)
        val data = PieData(dataSet)
        data.setValueFormatter(MiValueFormatter())
        data.setValueTextSize(12f)
        data.setValueTextColor(requireContext().getColor(R.color.charleyenda))
        //data.setValueTypeface(tfLight);
        chart!!.data = data
        chart!!.invalidate()
    }

    private fun setColorDataSet(dataSet: PieDataSet) {
        val colors: MutableList<Int> = ArrayList()
        val context = context
        if (context != null) {
            val arrayCOlor = context.resources.getIntArray(R.array.ChartSolicitude)
            for (value in arrayCOlor) {
                colors.add(value)
            }
            dataSet.colors = colors
        } else {
            dataSet.setColors(*ColorTemplate.MATERIAL_COLORS)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    private fun generateCenterSpannableText(): SpannableString {
        val s = SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda")
        s.setSpan(RelativeSizeSpan(1.7f), 0, 14, 0)
        s.setSpan(StyleSpan(Typeface.NORMAL), 14, s.length - 15, 0)
        s.setSpan(ForegroundColorSpan(Color.GRAY), 14, s.length - 15, 0)
        s.setSpan(RelativeSizeSpan(.8f), 14, s.length - 15, 0)
        s.setSpan(StyleSpan(Typeface.ITALIC), s.length - 14, s.length, 0)
        s.setSpan(ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length - 14, s.length, 0)
        return s
    }

    protected val months = arrayOf(
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"
    )
    protected val estadosOT = arrayOf(
            "No Iniciada", "Seguridad", "Vigente", "Pendientes", "Terminadas", "Confirmada", "Canceladas"
    )

}