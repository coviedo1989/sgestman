package com.example.carlos.sgestman.repository.ws

import com.example.carlos.sgestman.utils.UrlServices
import com.example.carlos.sgestman.utils.constant.PREFS
import com.pixplicity.easyprefs.library.Prefs
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.time.Duration
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class RetrofitClient private constructor()  {
    private val retrofit: Retrofit
    val api: ApiService
        get() = retrofit.create(ApiService::class.java)
    companion object {

        var mInstance: RetrofitClient? = null

        @get:Synchronized
        val instance: RetrofitClient?
            get() {
                if (mInstance == null) {
                    mInstance = RetrofitClient()
                }
                return mInstance
            }

        // Create a trust manager that does not validate certificate chains
        val unsafeOkHttpClient:

        // Install the all-trusting trust manager

        // Create an ssl socket factory with our all-trusting manager
                OkHttpClient.Builder
            get() = try {
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts = arrayOf<TrustManager>(
                        object : X509TrustManager {
                            @Throws(CertificateException::class)
                            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                            }

                            @Throws(CertificateException::class)
                            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                            }

                            override fun getAcceptedIssuers(): Array<X509Certificate> {
                                return arrayOf()
                            }
                        }
                )

                // Install the all-trusting trust manager
                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())

                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory
                val builder = OkHttpClient.Builder()
                builder.connectTimeout(30,TimeUnit.SECONDS)
                builder.readTimeout(30,TimeUnit.SECONDS)
                builder.writeTimeout(30,TimeUnit.SECONDS)
                builder.sslSocketFactory(sslSocketFactory, (trustAllCerts[0] as X509TrustManager))
                builder.hostnameVerifier(HostnameVerifier { hostname, session -> true })
                builder
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
    }

    init
    {

        val okHttpClient = OkHttpClient.Builder() //                .addInterceptor(
                //                        new Interceptor() {
                //                            @Override
                //                            public Response intercept(Chain chain) throws IOException {
                //                                Request original = chain.request();
                //
                //                                Request.Builder requestBuilder = original.newBuilder()
                //                                        .method(original.method(), original.body());
                //
                //                                Request request = requestBuilder.build();
                //                                return chain.proceed(request);
                //                            }
                //                        })
                .build()


        retrofit = Retrofit.Builder()
                .baseUrl(UrlServices.UrlBase)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(unsafeOkHttpClient.build())
                .build()
    }


}