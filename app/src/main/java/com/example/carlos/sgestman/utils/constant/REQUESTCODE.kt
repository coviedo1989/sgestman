package com.example.carlos.sgestman.utils.constant

object REQUESTCODE {

    const val TAKE_PHOTO_CAPTURE = 51
    const val SELECT_PHOTO_GALERY = 52

    ///1 solicitudes
    ///2 ordenes
    ///3 reporte trabajo
    var ACTIVIDAD_ADD_SOL = 11
    var ACTIVIDAD_DETAILS_SOL = 12
    var ACTIVIDAD_EDIT_SOL = 13

    var ACTIVIDAD_ADD_ORD = 21
    var ACTIVIDAD_DETAILS_ORD = 22

    var ACTIVIDAD_REPORTE_SELECT_OT = 32
    var ACTIVIDAD_REPORTE_TIEMPO_DETAILS = 33

}
