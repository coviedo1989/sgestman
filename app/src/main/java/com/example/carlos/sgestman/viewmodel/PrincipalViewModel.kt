package com.example.carlos.sgestman.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.carlos.sgestman.entity.Configuraciones
import com.example.carlos.sgestman.repository.Databases.ConfiguracionesRoomDBRepository
import com.example.carlos.sgestman.repository.ws.WebServiceRepository

class PrincipalViewModel (application: Application):ViewModel()
{
    private val webServiceRepository: WebServiceRepository
    private val roomDBRepository: ConfiguracionesRoomDBRepository
    private val mAllConfiguraciones: LiveData<Configuraciones?>?
    private val observerConfiguraciones: LiveData<Configuraciones>

    init {
        roomDBRepository = ConfiguracionesRoomDBRepository(application)
        webServiceRepository = WebServiceRepository(application)
        observerConfiguraciones = webServiceRepository.providesConfiguracionesWebService()
        mAllConfiguraciones = roomDBRepository.allConfiguraciones
    }

    fun getmAllConfiguraciones(): LiveData<Configuraciones?>? {
        return mAllConfiguraciones
    }
}