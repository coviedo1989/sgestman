package com.example.carlos.sgestman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.entity.Ejecutor


class AutoCompleEjecAdapter(context: Context, @LayoutRes private val layoutResource: Int, private var ejecutores: List<Ejecutor>):
        ArrayAdapter<Ejecutor>(context, layoutResource, ejecutores),Filterable {


    private  var mPois:List<Ejecutor> = ejecutores

    override fun getCount(): Int {
        return mPois.size
    }

    override fun getItem(p0: Int): Ejecutor {
        return mPois.get(p0)
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = createViewFromResource(convertView, parent, layoutResource)

        return bindData(getItem(position), view)
    }

    private fun bindData(item: Ejecutor, view: View): View
    {
        val title = view.findViewById<TextView>(R.id.codigo)
        val codigo = view.findViewById<TextView>(R.id.denom)
        title.text = item.codigo
        codigo.text = item.denom
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = createViewFromResource(convertView, parent!!, layoutResource)

        return bindData(getItem(position), view)
    }

    private fun createViewFromResource(convertView: View?, parent: ViewGroup, layoutResource: Int): View {
        return convertView ?: LayoutInflater.from(context).inflate(layoutResource, parent, false)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: Filter.FilterResults) {
                mPois = filterResults.values as List<Ejecutor>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): Filter.FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase()

                val filterResults = Filter.FilterResults()
                filterResults.values = if (queryString==null || queryString.isEmpty())
                    ejecutores
                else
                    ejecutores.filter {
                        it.codigo!!.toLowerCase().contains(queryString) ||
                                it.denom!!.toLowerCase().contains(queryString)
                    }
                return filterResults
            }
        }
    }

}
