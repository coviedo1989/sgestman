package com.example.carlos.sgestman.viewmodel

import androidx.lifecycle.*
import androidx.paging.*
import com.example.carlos.sgestman.entity.Permisos
import com.example.carlos.sgestman.entity.Solicitudes
import com.example.carlos.sgestman.entity.SolicitudesEvents
import com.example.carlos.sgestman.repository.Databases.PermisosRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.SolicitudesDataSource
import com.example.carlos.sgestman.utils.constant.PREFS
import com.pixplicity.easyprefs.library.Prefs

class SolicitudesViewModelTest: ViewModel()
{
    var codigo = Prefs.getString(PREFS.FILTER_SOL_CODIGO,"")
    var cliente = Prefs.getString(PREFS.FILTER_SOL_CLIENTE,"")
    var ejecutor = Prefs.getString(PREFS.FILTER_SOL_EJECUTOR,"")
    var objeto = Prefs.getString(PREFS.FILTER_SOL_OBJETO,"")
    var estado = Prefs.getInt(PREFS.FILTER_SOL_ESTADO,0)
    var fechaIncio = Prefs.getString(PREFS.FILTER_SOL_FECHAINICIO,"")
    var fechaFin = Prefs.getString(PREFS.FILTER_SOL_FECHAFIN,"")
    private var itemsSolicitudes = Pager(
            // Configure how data is loaded by passing additional properties to
            // PagingConfig, such as prefetchDistance.
            PagingConfig(
                    pageSize = 20,
                    enablePlaceholders = true)
    ) {
        SolicitudesDataSource(codigo,cliente,ejecutor,objeto,estado,fechaIncio,fechaFin)
    }.flow
            .cachedIn(viewModelScope)
            .asLiveData()
            .let { it as MutableLiveData<PagingData<Solicitudes>>}

    val paginDataSolicitudes:LiveData<PagingData<Solicitudes>> = itemsSolicitudes

//    fun onViewEvent(solicitudesEvents: SolicitudesEvents)
//    {
//        val paginData = paginDataSolicitudes.value ?: return
//
//        when (solicitudesEvents)
//        {
//            is SolicitudesEvents.Edit-> {
//                paginData.map {
//                    if(solicitudesEvents.solicitudes.codigo == it.codigo) return@map
//                    else return@map
//                }.let { itemsSolicitudes.value = it }
//            }
//        }
//    }
    fun onViewEvent(solicitudes: Solicitudes)
    {
        val paginData = paginDataSolicitudes.value

        paginData!!.map {
            if (it.codigo == solicitudes.codigo)
            {
                return@map solicitudes
            }
            else
            {
                return@map it
            }
        }.let {
            itemsSolicitudes.value = it 
        }

//        val paginData = paginDataSolicitudes.value?:return
//
//        paginData.map {
//            if(solicitudes.codigo == it.codigo)
//                return@map solicitudes
//            else
//                return@map it
//        }.let {
//            itemsSolicitudes.value = it }


    }



}