package com.example.carlos.sgestman.model

import com.example.carlos.sgestman.entity.CostosMtto
import com.example.carlos.sgestman.entity.OrdenesCount
import com.example.carlos.sgestman.entity.SolicitudesCount
import com.google.gson.annotations.SerializedName

class SDTSDDashboard
{
    @SerializedName("SDTSDSucces")
    var sDTSDSucces: Succes? = null

    @SerializedName("SolicitudesCount")
    var solicitudesCount: SolicitudesCount? = null

    @SerializedName("OrdenesCount")
    var ordenesCount: OrdenesCount? = null

    @SerializedName("CostosMtto")
    var costosMtto:CostosMtto? = null

    init {
        solicitudesCount = SolicitudesCount()
        ordenesCount = OrdenesCount()
        costosMtto = CostosMtto()
        sDTSDSucces = Succes()
    }

}