package com.example.carlos.sgestman.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.example.carlos.sgestman.entity.OrdenReporteTiempo
import com.example.carlos.sgestman.entity.ReporteTiempo
import com.example.carlos.sgestman.repository.Databases.OrdenReporteTiempoRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.ReporteTiempoRoomDBRepository

class ReporteTiempoDetailsViewModel(var application: Application,var numeroOT: String) :ViewModel()
{

  var ordenReporteTiempo : LiveData<OrdenReporteTiempo>
  private val ordenReporteTiempoRoomDBRepository=OrdenReporteTiempoRoomDBRepository(application)
  private val reporteTiempoRoomDBRepository = ReporteTiempoRoomDBRepository(application)

  private var _maxId : LiveData<Int>
  val maxId : LiveData<Int> get() = _maxId



    init
    {
        ordenReporteTiempo = ordenReporteTiempoRoomDBRepository.getOrdenReportetiempo(numeroOT)
        ordenReporteTiempoRoomDBRepository.ordenReporteTiempo
        _maxId = reporteTiempoRoomDBRepository.selectMaxID(numeroOT)
    }

    fun insertarReporte(reporteTiempo: ReporteTiempo)
    {
        reporteTiempoRoomDBRepository.insertReporteTiempo(reporteTiempo)
        _maxId = reporteTiempoRoomDBRepository.selectMaxID(numeroOT)
    }

}

class ReporteTiempoDetailsViewModelFactory(private var application: Application,private var numeroOT:String): ViewModelProvider.NewInstanceFactory()
{
  override fun <T : ViewModel?> create(modelClass: Class<T>): T
  {
    return ReporteTiempoDetailsViewModel(application,numeroOT) as T
  }
}