package com.example.carlos.sgestman.utils.constant

object INTENT {

    var EXTRA_SOLICITUD = "intent_extra_solicitud"

    var EXTRA_ORDEN_REPORTE_TIEMPO = "intent_extra_ordenReporteTiempo"

    var EXTRA_HAY_OT_PLAY = "intent_extra_hay_ot_play"

    val EXTRA_PERMISOS= "intent_extra_permisos"

    var EXTRA_ISELIMINO = "intent_isElimino"
    var EXTRA_ISEDITED = "intent_isEdited"


}