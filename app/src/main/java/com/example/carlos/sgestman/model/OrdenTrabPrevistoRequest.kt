package com.example.carlos.sgestman.model

class OrdenTrabPrevistoRequest
{
    var usuario:String? = null

    var emplID:Int? = null

    var fechaDia:String? = null

    var osejectrabobjID:Int? = null

    var modeAction: MODE_ACTION? = null

    enum class MODE_ACTION
    {
        INS,UPD,DEL
    }
}