package com.example.carlos.sgestman.model

import com.google.gson.annotations.SerializedName

class ResponsePermisosApp {

    @SerializedName("permisosApp")
    val responsePermisosApp: ResponsePermisos?= null
}