package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.carlos.sgestman.entity.ReporteTiempo

@Dao
interface ReporteTiempoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ordenReporte: ReporteTiempo?)

    @get:Query("SELECT * from ReporteTiempo")
    val allOrdenReporte: LiveData<List<ReporteTiempo>?>?

    @Query("SELECT * from ReporteTiempo where idOrdenReporte = :id and fechaFin != :nulo")
    fun selectReporte(id:Int,nulo:String): ReporteTiempo

    @Query("DELETE FROM ReporteTiempo")
    fun deleteAll()

    @Delete
    fun delete(ordenReporte: ReporteTiempo?)

    @Update
    fun update(ordenReporte: ReporteTiempo?)

    @Query("UPDATE ReporteTiempo SET fechaFin = :fechaFin where idOrdenReporte = :idOrden and fechaFin = ''")
    fun updatePause(idOrden:Int,fechaFin:String)

    @Query("Select max(idReporte) from ReporteTiempo where idOrdenReporte = :idOrden")
    fun selectMaxId(idOrden: Int):Int

    @Query("select max(idReporte) from OrdenReporte inner join ReporteTiempo on idOrden = idOrdenReporte where numeroOT = :numeroOT")
    fun maxId(numeroOT: String):LiveData<Int>
}