package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.carlos.sgestman.entity.Permisos
import com.example.carlos.sgestman.entity.User

@Dao
interface PermisosDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(global: Permisos?)

    @get:Query("SELECT * from permisos")
    val allGlobal: LiveData<List<Permisos>?>?

    @Query("DELETE FROM permisos")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGlobal(resultModel: Permisos?)

    @Query("SELECT * from permisos where opcion = :opcionIn")
    fun getPermisoByOpcion(opcionIn:String): Permisos?
}