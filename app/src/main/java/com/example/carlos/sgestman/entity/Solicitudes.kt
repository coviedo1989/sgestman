package com.example.carlos.sgestman.entity

import androidx.core.content.ContextCompat
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.example.carlos.sgestman.R
import java.io.Serializable

@Entity(tableName = "Solicitudes")
class Solicitudes : Serializable
{
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    var descripcion: String? = null
    var ejecutorCod: String? = null
    var fecha: String? = null
    var estado:Int? = null
    var codigo: String? = null
    var clienteCod: String? = null
    var empNombre: String? = null
    var clienteDenom: String? = null
    var empCodigo: String? = null
    var ejecutorDenom: String? = null
    var clasosDenom:String? = null
    var foto:String? = null
    var fotoUrl:String? = null

    @Ignore
    var Objetos: List<ObjetosSol>? = null

    fun getEstado():String
    {
        var estadoString = ""
        when(estado)
        {
            1-> {
                estadoString = "Sin Atender"
            }
            2-> {
                estadoString = "Atendida"
            }
            3-> {
                estadoString = "Iniciada"
            }
            4-> {
                estadoString = "Pendiente"
            }
            5-> {
                estadoString = "Terminada"
            }
            6->
            {
                estadoString = "Cerrada"
            }
            7->
            {
                estadoString = "Cancelada"
            }
        }
        return estadoString
    }
}