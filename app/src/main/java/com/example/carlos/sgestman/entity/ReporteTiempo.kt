package com.example.carlos.sgestman.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "ReporteTiempo",foreignKeys = [
    ForeignKey(
            entity = OrdenReporte::class,
            parentColumns = ["idOrden"],
            childColumns = ["idOrdenReporte"],
            onDelete = CASCADE
    )])
class ReporteTiempo:Serializable
{


    var idOrdenReporte:Int? = null

    @PrimaryKey(autoGenerate = true)
    var idReporte:Int? = null


    ////el id del registro de sgmweb
    var idOsRealSGM:Int? = null

    var fechaInicio:String? = null
    var fechaFin:String? = null
    var tiempoTrabajo:Float? = null
    var tipoHora:Int?= null
}