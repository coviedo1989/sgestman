package com.example.carlos.sgestman.actividades.ui.reportetrabajo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.actividades.BasicActivity
import com.example.carlos.sgestman.adapter.EliminarReporteListener
import com.example.carlos.sgestman.adapter.ReporteTrabajoAdapterDetails
import com.example.carlos.sgestman.databinding.ActivityActividadReporteTrabajoDetailsBinding
import com.example.carlos.sgestman.entity.*
import com.example.carlos.sgestman.repository.Databases.OrdenReporteRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.OrdenReporteWSRoomDBRepository
import com.example.carlos.sgestman.repository.Databases.ReporteTiempoRoomDBRepository
import com.example.carlos.sgestman.utils.MiFecha
import com.example.carlos.sgestman.utils.constant.INTENT
import com.example.carlos.sgestman.utils.enums.TIPOMENSAJE
import com.example.carlos.sgestman.viewmodel.ReporteTiempoDetailsViewModel
import com.example.carlos.sgestman.viewmodel.ReporteTiempoDetailsViewModelFactory
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo_details.*
import kotlinx.android.synthetic.main.activity_actividad_reporte_trabajo_details.ly_estate
import kotlin.collections.ArrayList

class ActividadReporteTrabajoDetails : BasicActivity(), EliminarReporteListener {


    private lateinit var ordenReporteWSRoomDBRepository: OrdenReporteWSRoomDBRepository
    private lateinit var reporteTiempoDetailsViewModel:ReporteTiempoDetailsViewModel
    //private val reporteTiempoDetailsViewModel:ReporteTiempoDetailsViewModel by viewModels()
    private lateinit var permisoReporTrab: Permisos
    private var isElimino: Boolean = false
    private var hizoCambios: Boolean = false
    private lateinit var reporteTrabajoAdapterDetails:ReporteTrabajoAdapterDetails

    private lateinit var ordenReporteRoomDBRepository : OrdenReporteRoomDBRepository
    private lateinit var reporteTiempoRoomDBRepository: ReporteTiempoRoomDBRepository

    private lateinit var reporteTrabajoDetailsBinding: ActivityActividadReporteTrabajoDetailsBinding
    private lateinit var ordenreporteTiempo: OrdenReporteTiempo
    private lateinit var ordenreporte:OrdenReporte
    private lateinit var listaReporteTiempo :ArrayList<ReporteTiempo>

    private var isOtIsPlay = false

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        reporteTrabajoDetailsBinding = ActivityActividadReporteTrabajoDetailsBinding.inflate(layoutInflater)
        setContentView(reporteTrabajoDetailsBinding.root)
        //reporteTrabajoDetailsBinding = DataBindingUtil.setContentView(this,R.layout.activity_actividad_reporte_trabajo_details)
        ordenreporteTiempo = intent.extras?.get(INTENT.EXTRA_ORDEN_REPORTE_TIEMPO) as OrdenReporteTiempo
        isOtIsPlay = intent.extras?.get(INTENT.EXTRA_HAY_OT_PLAY) as Boolean



        //prueba:ReporteTiempoDetailsViewModel by viewModels{ ReporteTiempoDetailsViewModelFactory(application,numeroOT) }

        //reporteTiempoDetailsViewModel = ReporteTiempoDetailsViewModel  by viewModels<> { ReporteTiempoDetailsViewModel }

        reporteTiempoDetailsViewModel = ViewModelProvider(this,ReporteTiempoDetailsViewModelFactory(application, ordenreporteTiempo.ordenReporte?.numeroOT!!)).get()
        reporteTrabajoDetailsBinding.viewmodel = reporteTiempoDetailsViewModel
        reporteTrabajoDetailsBinding.lifecycleOwner =this

        setupVariables()
        setupView()
        getData()
        checkEmptyState()
    }

    private fun getData()
    {
        reporteTiempoDetailsViewModel.ordenReporteTiempo.observe(this, Observer {
            if (it!=null)
            {
                listaReporteTiempo = it.listReporteTiempo as ArrayList<ReporteTiempo>
                //reporteTrabajoAdapterDetails.mFeedList = listaReporteTiempo
                listaReporteTiempo.sortByDescending { reportetiempo -> reportetiempo.idReporte }

                reporteTrabajoAdapterDetails.mFeedList = listaReporteTiempo
                ordenreporteTiempo.listReporteTiempo = listaReporteTiempo
                reporteTrabajoAdapterDetails.notifyDataSetChanged()
                checkEmptyState()

            }
        })
        reporteTiempoDetailsViewModel.maxId.observe(this, Observer
        {
        })
    }

    private fun checkEmptyState() {
        if (ordenreporteTiempo.listReporteTiempo.isEmpty())
        {
            lyRecyclerView.visibility = View.GONE
            ly_estate.visibility = View.VISIBLE
        }
        else
        {
            lyRecyclerView.visibility = View.VISIBLE
            ly_estate.visibility = View.GONE
        }
    }

    private fun setupVariables()
    {
        ordenReporteRoomDBRepository = OrdenReporteRoomDBRepository(application)
        ordenReporteWSRoomDBRepository = OrdenReporteWSRoomDBRepository(application)
        reporteTiempoRoomDBRepository = ReporteTiempoRoomDBRepository(application)

        permisoReporTrab = intent.extras?.get(INTENT.EXTRA_PERMISOS) as Permisos
       // ordenreporteTiempo = reporteTrabajoDetailsBinding.ordenreportetiempo!!
        ordenreporte = ordenreporteTiempo.ordenReporte!!
        listaReporteTiempo = ordenreporteTiempo.listReporteTiempo as ArrayList<ReporteTiempo>

        listaReporteTiempo.sortByDescending { it.idReporte }

        reporteTrabajoAdapterDetails = ReporteTrabajoAdapterDetails(listaReporteTiempo,this,this)
        recyclerview.layoutManager = LinearLayoutManager(this)
        recyclerview.adapter = reporteTrabajoAdapterDetails
    }

    private fun setupView()
    {
        imgDelete.visibility = if (permisoReporTrab.del==1) View.VISIBLE else View.GONE
        imgBack.setOnClickListener { onBackPressed() }
        imgDelete.setOnClickListener { eliminarOrdenReporte() }
        setupPlayPause()
    }


    private fun eliminarOrdenReporte()
    {
        ///aqui elimino la ornde asignada al empleado primero en ws y luego interna
        showProgresBar()
        val ordenReporteRoomDBRepository = OrdenReporteRoomDBRepository(application)
        ordenReporteRoomDBRepository.deleteOrdenesReporte(ordenreporte)

        val ordenReporteWS = OrdenReporteWS()
        ordenReporteWS.numeroOT = ordenreporteTiempo.ordenReporte!!.numeroOT
        ordenReporteWSRoomDBRepository.deleteOrdenesReporteWS(ordenReporteWS)

        isElimino = true
        onBackPressed()
    }

    private fun setupPlayPause()
    {
        if (ordenreporte.isplay!!)
        {
            imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
            imgPause.setBackgroundResource(R.drawable.ic_pause)
            //lyPlayPause.setBackgroundResource(R.drawable.color_gradiente_play)
        }
        if (ordenreporte.ispasuse!!)
        {
            imgPlay.setBackgroundResource(R.drawable.ic_play)
            imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
            //lyPlayPause.setBackgroundResource(R.drawable.color_gradiente_pause)
        }
        if (!ordenreporte.activada!!)
        {
            imgPlay.setBackgroundResource(R.drawable.ic_play)
            imgPause.setBackgroundResource(R.drawable.ic_pause)
        }

        imgPlay.setOnClickListener {
            if(!ordenreporte.isplay!!)
            {
                if(isOtIsPlay)
                {
                    showSimpleError("Ya existe iniciado otro trabajo",TIPOMENSAJE.INFO)
                }
                else
                {
                    imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
                    imgPause.setBackgroundResource(R.drawable.ic_pause)
                    ///enviar al servidor primero y luego hacer esto
                    enviarPlayWS()
                    playLocal()


                }

            }

            checkEmptyState()
        }
        imgPause.setOnClickListener {
            if (ordenreporte.isplay!!)
            {
                imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
                imgPlay.setBackgroundResource(R.drawable.ic_play)
                ///enviar al servidor primero y luego hacer esto
                enviarPausaWS()
                pausaLocal()


            }
            checkEmptyState()
        }
    }

    private fun playLocal() {
        ordenreporte.activada = true
        ordenreporte.isplay = true
        ordenreporte.ispasuse = false
        hizoCambios = true
        ordenReporteRoomDBRepository.updateOrdenesReporte(ordenreporte)
        enviarPlayOrdenSgestman(ordenreporte)
        ordenreporteTiempo.ordenReporte = ordenreporte
        ordenreporteTiempo.listReporteTiempo = listaReporteTiempo
    }

    private fun pausaLocal() {
        ordenreporte.activada = true
        ordenreporte.ispasuse = true
        ordenreporte.isplay = false
        hizoCambios = true
        ordenReporteRoomDBRepository.updateOrdenesReporte(ordenreporte)
        enviarPauseOrdenSgestman(ordenreporte)

        ordenreporteTiempo.listReporteTiempo = listaReporteTiempo
        ordenreporteTiempo.ordenReporte = ordenreporte
        isOtIsPlay = false
    }

    private fun enviarPlayWS() {

    }
    private fun enviarPausaWS() {

    }

    private fun enviarPauseOrdenSgestman(ordenreporte: OrdenReporte?)
    {
        val reporteTiempo = ReporteTiempo()
        reporteTiempo.idOrdenReporte = ordenreporte?.idOrden
        reporteTiempo.fechaFin= MiFecha().fecha
        reporteTiempoRoomDBRepository.updateOrdenesReporte(reporteTiempo)
        listaReporteTiempo.forEach { if (it.idOrdenReporte==ordenreporte?.idOrden && it.fechaFin!!.isEmpty())
        {
            it.fechaFin = MiFecha().fecha
        }}

        reporteTrabajoAdapterDetails.mFeedList = listaReporteTiempo
        reporteTrabajoAdapterDetails.notifyDataSetChanged()
        //(reciclerview.adapter as ReporteTrabajoAdapter).notifyItemChanged(position)
    }

    private fun enviarPlayOrdenSgestman(ordenreporte: OrdenReporte?)
    {
        //listOrdenReporte.sortByDescending { it.ispasuse}
        //listOrdenReporte.sortByDescending { it.isplay}
        val reporteTiempo = ReporteTiempo()
        reporteTiempo.idOrdenReporte = ordenreporte?.idOrden
        reporteTiempo.fechaInicio = MiFecha().fecha
        reporteTiempo.fechaFin = ""

        reporteTiempoDetailsViewModel.insertarReporte(reporteTiempo)

        //val id = reporteTiempoRoomDBRepository.selectMaxID(ordenreporte?.idOrden)
        //reporteTiempo.idReporte =if (id ==0) 1 else id
        listaReporteTiempo.add(0,reporteTiempo)




        reporteTrabajoAdapterDetails.mFeedList = listaReporteTiempo
        //reporteTrabajoAdapterDetails.notifyDataSetChanged()
        reporteTrabajoAdapterDetails.notifyItemInserted(0)
    }

    override fun eliminarListener(reporteTiempo: ReporteTiempo,position:Int)
    {
            eliminarReporteTiempo(reporteTiempo,position)
    }

    private fun eliminarReporteTiempo(reporteTiempo: ReporteTiempo,position: Int)
    {
        showProgresBar()
        eliminarWS(reporteTiempo)
        eliminarInternal(reporteTiempo,position)
        actualizarEstadorOrdenReporte()
        showProgresBar()
        hizoCambios = true
    }

    private fun actualizarEstadorOrdenReporte()
    {
        //si la orden no tiene reportado tiempo pues entonces quitarla de activada
        //pero si al eliminar elimino el registro que no tiene fecha fin es que estaba en play
        // y tengo que ponerla en pausa de nuevo

        val ordenReporteRoomDBRepository = OrdenReporteRoomDBRepository(application)

        if (listaReporteTiempo.size==0)
        {
            ordenreporte.isplay = false
            ordenreporte.ispasuse = false
            ordenreporte.activada = false

            ordenReporteRoomDBRepository.updateOrdenesReporte(ordenreporte)
            ordenreporteTiempo.ordenReporte = ordenreporte
            setupPlayPause()
        }
        else
        {
            if (listaReporteTiempo.any { it.fechaFin=="" || it.fechaFin==null })
            {
                ///quiere decir que estoy elimiando un registro que queda algun play solamente
                ordenreporte.isplay = true
                ordenreporte.ispasuse = false
                ordenreporte.activada = true
            }
            else
            {
                ///quiere decir que estoy eliminando un registro pero que no queda ningun play solamente
                ordenreporte.isplay = false
                ordenreporte.ispasuse = true
                ordenreporte.activada = true
            }
            ordenReporteRoomDBRepository.updateOrdenesReporte(ordenreporte)
            ordenreporteTiempo.ordenReporte = ordenreporte
            setupPlayPause()
        }



    }

    private fun eliminarInternal(reporteTiempo: ReporteTiempo, position: Int)
    {
        ///esto es si cuando se eliminar del ws eliminar lista y bd y acutualizar
        val reporteTiempoRoomDBRepository = ReporteTiempoRoomDBRepository(application)
        reporteTiempoRoomDBRepository.deleteReporteTiempo(reporteTiempo)

        listaReporteTiempo.removeAt(position)
        reporteTrabajoAdapterDetails.mFeedList = listaReporteTiempo
        reporteTrabajoAdapterDetails.removeAt(position)
        ordenreporteTiempo.listReporteTiempo = listaReporteTiempo
        checkEmptyState()
    }

    private fun eliminarWS(reporteTiempo: ReporteTiempo) {

    }


    override fun onBackPressed()
    {
        val intent = Intent()
        if (hizoCambios)
        {
            intent.putExtra(INTENT.EXTRA_ISEDITED,hizoCambios)
            intent.putExtra(INTENT.EXTRA_ORDEN_REPORTE_TIEMPO,ordenreporteTiempo)
            setResult(Activity.RESULT_OK,intent)
            Log.e("Count Listtt",ordenreporteTiempo.listReporteTiempo.size.toString())
        }
        if(isElimino)
        {
            intent.putExtra(INTENT.EXTRA_ISELIMINO,isElimino)
            setResult(Activity.RESULT_OK,intent)
        }


        super.onBackPressed()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(INTENT.EXTRA_ORDEN_REPORTE_TIEMPO,ordenreporteTiempo)
        outState.putBoolean(INTENT.EXTRA_ISELIMINO,isElimino)
        outState.putSerializable(INTENT.EXTRA_PERMISOS,permisoReporTrab)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

        super.onRestoreInstanceState(savedInstanceState)
        ordenreporteTiempo = savedInstanceState.getSerializable(INTENT.EXTRA_ORDEN_REPORTE_TIEMPO) as OrdenReporteTiempo
        isElimino = savedInstanceState.get(INTENT.EXTRA_ISELIMINO) as Boolean
        permisoReporTrab = savedInstanceState.getSerializable(INTENT.EXTRA_PERMISOS) as Permisos
    }
}