package com.example.carlos.sgestman.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.carlos.sgestman.entity.SolicitudesCount

@Dao
interface SolicitudesCountDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(solicitudesCount: SolicitudesCount?)

    @get:Query("SELECT * from SolicitudesCount")
    val allSolicitudCount: LiveData<SolicitudesCount?>?

    @Query("DELETE FROM SolicitudesCount")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSolicitudCount(solicitudesCount: SolicitudesCount?)
}