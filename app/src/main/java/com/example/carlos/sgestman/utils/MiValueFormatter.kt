package com.example.carlos.sgestman.utils

import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.DecimalFormat

class MiValueFormatter() : ValueFormatter() {
    var mFormat: DecimalFormat
    private var pieChart: PieChart? = null

    // Can be used to remove percent signs if the chart isn't in percent mode
    constructor(pieChart: PieChart?) : this() {
        this.pieChart = pieChart
    }

    override fun getFormattedValue(value: Float): String {
        return mFormat.format(value.toDouble()) + " %"
    }

    override fun getPieLabel(value: Float, pieEntry: PieEntry): String {
        return getFormattedValue(value)
    }

    init {
        mFormat = DecimalFormat("###,###,##0.00")
    }
}