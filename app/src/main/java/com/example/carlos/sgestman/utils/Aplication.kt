package com.example.carlos.sgestman.utils

import android.app.Application
import android.content.ContextWrapper
import com.pixplicity.easyprefs.library.Prefs

class Aplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(applicationContext.packageName)
                .setUseDefaultSharedPreference(true)
                .build()
    }
}