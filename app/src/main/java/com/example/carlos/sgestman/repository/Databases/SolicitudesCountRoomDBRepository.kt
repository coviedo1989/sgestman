package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.carlos.sgestman.repository.dao.SolicitudesCountDao
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.entity.SolicitudesCount

class SolicitudesCountRoomDBRepository(application: Application?) {
    private val solicitudesCountInfoDao: SolicitudesCountDao?
    var allSolicitudesCount: LiveData<SolicitudesCount?>?

    fun insertSolicitudesCount(resultModel: SolicitudesCount?) {
        InsertAsyncTask(solicitudesCountInfoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: SolicitudesCountDao?) : AsyncTask<SolicitudesCount?, Void?, Void?>() {
        override fun doInBackground(vararg globals: SolicitudesCount?): Void? {
            mAsyncTaskDao!!.insert(globals[0])
            return null
        }
    }

    init {
        val db = getDatabase(application!!)
        solicitudesCountInfoDao = db!!.solicitudesCountInfoDao()
        allSolicitudesCount = solicitudesCountInfoDao!!.allSolicitudCount
    }
}