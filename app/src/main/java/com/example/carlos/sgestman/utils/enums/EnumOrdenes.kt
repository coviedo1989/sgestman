package com.example.carlos.sgestman.utils.enums

enum class EnumOrdenes(var estate:Int)
{

        NOINICIADA(1),
        PERMISOSEG(2),
        VIGENTE(3),
        PENDIENTE(4),
        TERMINADA(5),
        CONFIRMADA(6),
        CANCELADA(5);

        companion object {
                fun fromInt(value: Int) = EnumOrdenes.values().first { it.estate == value }
        }
}