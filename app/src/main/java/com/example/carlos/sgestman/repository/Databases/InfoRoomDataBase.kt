package com.example.carlos.sgestman.repository.Databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.carlos.sgestman.entity.*
import com.example.carlos.sgestman.repository.dao.*

@Database(entities = [User::class,Permisos::class, SolicitudesCount::class, OrdenesCount::class,CostosMtto::class,Solicitudes::class,Configuraciones::class,OrdenReporte::class,ReporteTiempo::class,OrdenReporteWS::class], version = 1, exportSchema = false)
abstract class InfoRoomDataBase : RoomDatabase() {
    abstract fun userInfoDao(): UserDao?
    abstract fun solicitudesCountInfoDao(): SolicitudesCountDao?
    abstract fun ordenesCountInfoDao(): OrdenesCountDao?
    abstract fun costosMttoInfoDao(): CostosMttoDao?
    abstract fun solicutdesInfoDao():SolicitudesDao?
    abstract fun configuracionesInfo():ConfiguracionesDao?
    abstract fun ordenReporteInfoDao():OrdenReporteDao?
    abstract fun ordenReporteWSInfoDao():OrdenReporteWSDao
    abstract fun reporteTiempoInfoDao():ReporteTiempoDao?
    abstract fun ordenReporteTiempoInfoDao():OrdenReporteTiempoDao?
    abstract fun permisosInfoDao():PermisosDao?

    companion object {
        private var INSTANCE: InfoRoomDataBase? = null
        @JvmStatic
        fun getDatabase(context: Context): InfoRoomDataBase? {
            if (INSTANCE == null) {
                synchronized(InfoRoomDataBase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                InfoRoomDataBase::class.java, "sgestman_database")
                                .addCallback(sRoomDatabaseCallback)
                                .build()
                    }
                }
            }
            return INSTANCE
        }

        private val sRoomDatabaseCallback: Callback = object : Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { PopulateDbAsync(it).execute() }
            }
        }
    }
}