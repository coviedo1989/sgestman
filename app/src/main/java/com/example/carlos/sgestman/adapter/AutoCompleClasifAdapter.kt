package com.example.carlos.sgestman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.entity.Clasificaciones
import com.example.carlos.sgestman.entity.Ejecutor
import java.util.*


class AutoCompleClasifAdapter(context: Context, @LayoutRes private val layoutResource: Int, private var clasificaciones: List<Clasificaciones>):
        ArrayAdapter<Clasificaciones>(context, layoutResource, clasificaciones),Filterable {


    private  var mPois:List<Clasificaciones> = clasificaciones

    override fun getCount(): Int {
        return mPois.size
    }

    override fun getItem(p0: Int): Clasificaciones {
        return mPois.get(p0)
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = createViewFromResource(convertView, parent, layoutResource)

        return bindData(getItem(position), view)
    }

    private fun bindData(item: Clasificaciones, view: View): View
    {
        val title = view.findViewById<TextView>(R.id.codigo)
        val codigo = view.findViewById<TextView>(R.id.denom)
        title.text = item.clasosDenom
        codigo.visibility = View.GONE
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = createViewFromResource(convertView, parent!!, layoutResource)

        return bindData(getItem(position), view)
    }

    private fun createViewFromResource(convertView: View?, parent: ViewGroup, layoutResource: Int): View {
        return convertView ?: LayoutInflater.from(context).inflate(layoutResource, parent, false)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: Filter.FilterResults) {
                mPois = filterResults.values as List<Clasificaciones>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): Filter.FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase(Locale.ROOT)

                val filterResults = FilterResults()
                filterResults.values = if (queryString==null || queryString.isEmpty())
                    clasificaciones
                else
                    clasificaciones.filter {
                        it.clasosDenom!!.toLowerCase(Locale.ROOT).contains(queryString) ||
                                it.clasosDenom!!.toLowerCase(Locale.ROOT).contains(queryString)
                    }
                return filterResults
            }
        }
    }

}
