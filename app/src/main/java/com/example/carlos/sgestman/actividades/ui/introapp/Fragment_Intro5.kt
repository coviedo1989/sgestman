package com.example.carlos.sgestman.actividades.ui.introapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.carlos.sgestman.R

/**
 * A simple [Fragment] subclass.
 */
class Fragment_Intro5 : Fragment() {
    private var listener: EntraApp_Listener? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_fragment__intro5, container, false)
        val text_comenzar = view.findViewById<Button>(R.id.button2)
        text_comenzar.setOnClickListener { listener!!.EntraApp(true) }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as EntraApp_Listener
        } catch (e: Exception) {
            Log.v("test", "Error, no se ha podido llevar a cabo la comunicación")
        }
    }

    interface EntraApp_Listener {
        fun EntraApp(entra: Boolean)
    }
}