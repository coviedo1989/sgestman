package com.example.carlos.sgestman.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "CostosMtto")
class CostosMtto {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    var costosMO = 0

    var costosMAT = 0
    var costosFAC = 0
    var costosOC = 0

    var total = 0

}