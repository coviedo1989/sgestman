package com.example.carlos.sgestman.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.databinding.ItemReportetrabajoBinding
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.entity.OrdenReporteTiempo
import com.example.carlos.sgestman.utils.MiFecha
import kotlinx.android.synthetic.main.fragment_perfil.view.*

/**
 * Created by HP-HP on 05-12-2015.
 */
class ReporteTrabajoAdapter(var mFeedList: ArrayList<OrdenReporteTiempo>, private var contex: Context, private val listener: AdapterReporteTrabajoListener) : RecyclerView.Adapter<ViewHolderReporteTrabajo>()
{
    private var mLayoutInflater: LayoutInflater? = null

    private var lastPosition = -1



    override fun onBindViewHolder(holder: ViewHolderReporteTrabajo, position: Int) {
        this.position = position
        val ordenReporteTiempo = mFeedList[position]
        holder.bindTo(ordenReporteTiempo,listener,mFeedList,position)


        //setAnimation(holder.itemView, position);
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderReporteTrabajo {
        return ViewHolderReporteTrabajo.from(parent)
    }
    override fun onViewDetachedFromWindow(holder: ViewHolderReporteTrabajo)
    {
        holder.clearAnimation(holder)
        super.onViewDetachedFromWindow(holder)
    }
    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            val animation: Animation = AnimationUtils.loadAnimation(contex, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
    fun moveUp(position: Int)
    {

        position.takeIf { it > 0 }?.also { currentPosition ->
            var positionAux = currentPosition
            while (positionAux > 0)
            {
            mFeedList.removeAt(positionAux).also {
                mFeedList.add(positionAux - 1, it)
            }
                notifyItemMoved(positionAux, positionAux - 1)
                positionAux --
            }
        }
    }

    fun removeAt(position: Int)
    {
        mFeedList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemChanged(position,itemCount)
    }

//    override fun onViewRecycled(holder: ViewHolderVentas) {
//        holder.itemView.setOnLongClickListener(null)
//        super.onViewRecycled(holder)
//    }

    //    @Override
    //    public int getItemCount() {
    //        return (mFeedList!=null? mFeedList.size():0);
    //    }
    var position = 0

    override fun getItemCount(): Int {
        return mFeedList.size
    }




}
 class ViewHolderReporteTrabajo(private val binding: ItemReportetrabajoBinding, private val contex: Context) : RecyclerView.ViewHolder(binding.root){

     var ordenreporte : OrdenReporte? = null

     fun bindTo(ordenreportetiempo: OrdenReporteTiempo?, listener: AdapterReporteTrabajoListener, mFeedList: List<OrdenReporteTiempo>, position: Int)
     {
         ordenreporte = ordenreportetiempo!!.ordenReporte
         binding.fecha = MiFecha()
         binding.ordenreporte = ordenreporte
         binding.fecha = MiFecha()


         if (binding.ordenreporte!!.activada!!)
         {
             binding.lyButtonDetalles.visibility = View.VISIBLE
             binding.txtDetails.paintFlags = Paint.UNDERLINE_TEXT_FLAG
         }
         else
         {
             binding.lyButtonDetalles.visibility = View.GONE
         }

             binding.lyButtonDetalles.setOnClickListener {

                 listener.detailsListener(ordenreporte,position)
//             if (binding.lyDetails.visibility == View.GONE)
//             {
//                 animarImageDrow(binding.imgDrow)
//                 AddOrdenReporteTiempos(binding.lyDetails,mFeedList)
//                 animationLayoutIn(binding.lyDetails)
//                 //binding.txtDetalles.text = contex.getString(R.string.ocultar)
//
//             }
//             else
//             {
//                 animarImageDrow(binding.imgDrow)
//                 binding.lyDetails.visibility = View.GONE
//                 //binding.txtDetalles.text = contex.getString(R.string.ver)
//             }
         }



         binding.imgEstado.background = ContextCompat.getDrawable(contex,R.drawable.circle_estado)

         when(binding.ordenreporte!!.estadoOT) {
             1 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_SinAtender)
                 binding.estado.text = String.format("No Iniciada")
             }
             2 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_PermisoSeguridad)
                 binding.estado.text = String.format("Permiso Seguridad")
             }
             3 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Iniciada)
                 binding.estado.text = String.format("Vigente")
             }
             4 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Pendientes)
                 binding.estado.text = String.format("Pendiente")
             }
             5 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Terminadas)
                 binding.estado.text = String.format("Terminada")
             }
             6 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Cerradas)
                 binding.estado.text = String.format("Confirmada")
             }
             7 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Cerradas)
                 binding.estado.text = String.format("Cancelada")
             }

         }

         binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
         binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
         binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_normal)
         if (ordenreporte!!.isplay!!)
         {
             binding.imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
             binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
             binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_play)
         }
         if (ordenreporte!!.ispasuse!!)
         {
             binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
             binding.imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
             binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_pause)
         }

         binding.imgPlay.setOnClickListener {
             if(!ordenreporte!!.isplay!!)
             {
                 if(siHayOTPlay(mFeedList))
                 {
                     listener.playListener(ordenreporte,doPlay = false,position = position)
                 }
                 else
                 {
                     binding.imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
                     binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
                     ordenreporte!!.activada = true
                     ordenreporte!!.isplay = true
                     ordenreporte!!.ispasuse = false
                     listener.playListener(ordenreporte,doPlay = true,position = position)
                 }

             }
         }
         binding.imgPause.setOnClickListener {
             if(!ordenreporte!!.ispasuse!! and ordenreporte!!.activada!!)
             {
                 binding.imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
                 binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
                 ordenreporte!!.activada = true
                 ordenreporte!!.ispasuse = true
                 ordenreporte!!.isplay = false
                 listener.pauseListener(ordenreporte,position)
             }

         }

         binding.lyTodo.setOnClickListener {
             listener.detailsListener(ordenreporte,position)
         }

         binding.lyTodo.setOnLongClickListener {
             listener.longListener(ordenreportetiempo,position)
             true
         }


         binding.executePendingBindings()
     }

     private fun animarImageDrow(imgDrow: ImageView)
     {
         imgDrow.animate().rotationBy(180f).setDuration(100).setInterpolator(LinearInterpolator()).start();}

     private fun animationLayoutIn(lyDetails: LinearLayout)
     {
         lyDetails.visibility = View.VISIBLE
         val animation: Animation = AnimationUtils.loadAnimation(contex, android.R.anim.fade_in)
         lyDetails.startAnimation(animation)
         // lyDetails.visibility = View.VISIBLE
     }
     private fun animationLayoutOut(lyDetails: LinearLayout)
     {
         val animation: Animation = AnimationUtils.loadAnimation(contex, android.R.anim.fade_in)
         lyDetails.startAnimation(animation)
         animation.setAnimationListener(object :Animation.AnimationListener{
             override fun onAnimationRepeat(animation: Animation?) {

             }

             override fun onAnimationEnd(animation: Animation?) {

                 lyDetails.visibility = View.GONE
             }

             override fun onAnimationStart(animation: Animation?) {

             }
         })
     }

     private fun AddOrdenReporteTiempos(lyDetails: LinearLayout, mFeedList: List<OrdenReporteTiempo>)
     {
         lyDetails.removeAllViews()
         mFeedList.forEach {
             if (it.ordenReporte?.numeroOT == ordenreporte?.numeroOT)
             {
                 it.listReporteTiempo.forEach { reporteTiempo ->
                     val viewDetails = LayoutInflater.from(contex).inflate(R.layout.item_reportetiempo_details,null)
                     val fechainicio = viewDetails.findViewById<TextView>(R.id.fechaInicio)
                     val fechaFin = viewDetails.findViewById<TextView>(R.id.fechaFin)
                     val imgEliminar = viewDetails.findViewById<ImageView>(R.id.img_eliminar)
                     fechainicio.text = reporteTiempo.fechaInicio
                     fechaFin.text = reporteTiempo.fechaFin
                     viewDetails.id = reporteTiempo.idReporte!!
                     imgEliminar.setOnClickListener { Toast.makeText(contex,viewDetails.id.toString(),Toast.LENGTH_SHORT).show() }
                     lyDetails.addView(viewDetails)
                 }

             }
         }
     }

     private fun siHayOTPlay(mFeedList: List<OrdenReporteTiempo>): Boolean {
         var isplay = false
         mFeedList.forEach {
             if (it.ordenReporte!!.isplay!!) {
                 isplay = true
             }
         }
         return  isplay
     }

     fun clearAnimation(holder: ViewHolderReporteTrabajo)
     {
         holder.itemView.clearAnimation()
     }

     companion object {
         fun from(parent: ViewGroup): ViewHolderReporteTrabajo
         {
             val layoutInflater = LayoutInflater.from(parent.context)
             val binding = ItemReportetrabajoBinding.inflate(layoutInflater, parent, false)
             return ViewHolderReporteTrabajo(binding,parent.context)
         }
     }
}

interface AdapterReporteTrabajoListener
{
    fun playListener(ordenreporte:OrdenReporte?,doPlay:Boolean,position: Int)

    fun pauseListener(ordenreporte: OrdenReporte?,position: Int)

    fun detailsListener(ordenreporte:OrdenReporte?,position: Int)

    fun longListener(ordenreportetiempo: OrdenReporteTiempo?,position: Int)
}