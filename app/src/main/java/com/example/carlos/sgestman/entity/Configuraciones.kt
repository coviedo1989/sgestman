package com.example.carlos.sgestman.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Configuracion")
class Configuraciones
{
    @PrimaryKey(autoGenerate = true)
    var id:Int? = null

    @SerializedName("usarClasifSol")
    var usarClasificacionSolicitud:Boolean? = null

    @SerializedName("usarEjecSol")
    var usarEjecutorSolicidut:Boolean? =null

}