package com.example.carlos.sgestman.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable


@Entity(tableName = "OrdenReporte")
class OrdenReporte:Serializable
{
    @PrimaryKey(autoGenerate = true)
    var idOrden:Int? = null
    var numeroOT:String?= null
    var descripcion:String? = null
    var estadoOT:Int? = null
    var tiempoTrabajo:Float? = null

    @SerializedName("osmoprevFchDia")
    var fechaInicioSgm:String? = null
    var fechacreada:String? = null
    var fechaInicio:String? = null
    var fechaFin:String? = null
    var activada:Boolean? = null
    var isplay:Boolean? = null
    var ispasuse:Boolean? = null

}