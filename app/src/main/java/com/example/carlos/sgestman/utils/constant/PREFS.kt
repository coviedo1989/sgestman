package com.example.carlos.sgestman.utils.constant

object PREFS {
    @JvmField
    var USUARIO = "usuario"

    @JvmField
    var CONTRASENA = "contrasena"

    @JvmField
    var USUARIO_ID = "usuarioID"

    @JvmField
    var USUARIO_EMPLID = "usuarioEmplID"



    @JvmField
    var TAB_POSICION = "tab_position"


    ///FILTROS SOLICITUD
    @JvmField
    var FILTER_SOL_ISFILTER = "isFilterSolicitud"
    @JvmField
    var FILTER_SOL_CODIGO = "codigoSol"
    @JvmField
    var FILTER_SOL_CLIENTE = "codigoSolCliente"
    @JvmField
    var FILTER_SOL_EJECUTOR = "codigoSolEjecutor"
    @JvmField
    var FILTER_SOL_OBJETO = "codigoSolEjecutor"
    @JvmField
    var FILTER_SOL_ESTADO = "codigoSolEstado"
    @JvmField
    var FILTER_SOL_FECHAINICIO = "codigoSolFechaInicio"
    @JvmField
    var FILTER_SOL_FECHAFIN = "codigoSolFechaFin"

    ///FILTROS ORDENES
    @JvmField
    var FILTER_ORD_ISFILTER = "isFilterOrden"
    @JvmField
    var FILTER_ORD_CODIGO = "codigoOrden"
    @JvmField
    var FILTER_ORD_NUMERO = "codigoNumeroOrden"
    @JvmField
    var FILTER_ORD_CLIENTE = "codigoOrdenCliente"
    @JvmField
    var FILTER_ORD_EJECUTOR = "codigoOrdenEjecutor"
    @JvmField
    var FILTER_ORD_OBJETO = "codigoOrdenEjecutor"
    @JvmField
    var FILTER_ORD_ESTADO = "codigoOrdenEstado"
    @JvmField
    var FILTER_ORD_FECHAINICIO = "codigoOrdenFechaInicio"
    @JvmField
    var FILTER_ORD_FECHAFIN = "codigoOrdenFechaFin"

    ///FILTROS REPORTE ORDEN
    @JvmField
    var FILTER_ORD_REPORTE_ISFILTER = "isFilterReporteOrden"
   @JvmField
    var FILTER_ORD_REPORTE_NUMERO = "codigoNumeroOrdenReporte"
    @JvmField
    var FILTER_ORD_REPORTE_FECHAINICIO = "codigoOrdenReporteFechaInicio"
    @JvmField
    var FILTER_ORD_REPORTE_FECHAFIN = "codigoOrdenReporteFechaFin"
    @JvmField
    var FILTER_ORD_REPORTE_ESTADO = "codigoOrdenReporteEstado"

    @JvmField
    var FILTER_ORD_REPORTE_ITEMS_ESTADO = "codigoOrdenReporteItemsEstado"


    //CONFIG
    @JvmField
    var CONFIGURACION_SOL_IS_EJECUTOR = "configuracionSolisEjecutor"

    @JvmField
    var CONFIGURACION_SOL_IS_CLASIFICACION = "configuracionSolisClasificacion"

    @JvmField
    var CONFIG_API_URL = "configuracionApiUrl"

    @JvmField
    var CONFIG_API_PUERTO = "configuracionApiPuerto"

    var OP_INICIAR_SESION_AUTOMATICA ="opInicioAutomatico"
    var OP_RECORDAR_CONTRASENA ="opRecondarContrasenna"

    var OP_MODE_NIGHT = "opModeNight"

}