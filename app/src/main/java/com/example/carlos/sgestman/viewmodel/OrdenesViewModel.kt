package com.example.carlos.sgestman.viewmodel

import androidx.lifecycle.ViewModel
import androidx.paging.*
import com.example.carlos.sgestman.repository.Databases.OrdenesDataSource
import com.example.carlos.sgestman.utils.constant.PREFS
import com.pixplicity.easyprefs.library.Prefs

class OrdenesViewModel(): ViewModel()
{
    var codigo = Prefs.getString(PREFS.FILTER_ORD_CODIGO,"")
    var numero = Prefs.getString(PREFS.FILTER_ORD_NUMERO,"")
    var cliente = Prefs.getString(PREFS.FILTER_ORD_CLIENTE,"")
    var ejecutor = Prefs.getString(PREFS.FILTER_ORD_EJECUTOR,"")
    var objeto = Prefs.getString(PREFS.FILTER_ORD_OBJETO,"")
    var estado = Prefs.getInt(PREFS.FILTER_ORD_ESTADO,0)
    var fechaIncio = Prefs.getString(PREFS.FILTER_ORD_FECHAINICIO,"")
    var fechaFin = Prefs.getString(PREFS.FILTER_ORD_FECHAFIN,"")


    var itemsOrdenes = Pager(
            // Configure how data is loaded by passing additional properties to
            // PagingConfig, such as prefetchDistance.
            PagingConfig(
                    pageSize = 20,
                    enablePlaceholders = true)
    ) {
        OrdenesDataSource(codigo,numero,cliente,ejecutor,objeto,estado,fechaIncio,fechaFin)
    }.flow
           // .cachedIn(viewModelScope)

}