package com.example.carlos.sgestman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.databinding.ItemReportetrabajoExpandeBinding
import com.example.carlos.sgestman.entity.OrdenReporte
import com.example.carlos.sgestman.entity.OrdenReporteTiempo
import com.example.carlos.sgestman.entity.ReporteTiempo
import com.example.carlos.sgestman.utils.MiFecha

/**
 * Created by HP-HP on 05-12-2015.
 */
class ReporteTrabajoAdapterExpande(private val mFeedList: ArrayList<OrdenReporteTiempo>, private var contex: Context, private val listener: AdapterReporteTrabajoListener) : BaseExpandableListAdapter()
{
    private var mLayoutInflater: LayoutInflater? = null
    private var count = 0
    private var lastPosition = -1


    var position = 0


    fun setCount(count: Int) {
        this.count = count
        notifyDataSetChanged()
    }

    override fun getGroup(groupPosition: Int): Any {
        return this.mFeedList[groupPosition].ordenReporte!!
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        var convertViewChild = convertView
        val listTitle = getChild(groupPosition,childPosition) as ReporteTiempo
        if (convertView == null) {
            val layoutInflater = this.contex.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertViewChild = layoutInflater.inflate(R.layout.item_reportetrabajo_expande, null)
        }
        val numeroOt = convertViewChild!!.findViewById<TextView>(R.id.nroOrden)
        val descripcion = convertViewChild.findViewById<TextView>(R.id.descripcion)
        numeroOt.text = listTitle.fechaInicio
        descripcion.text = listTitle.fechaFin
        return convertViewChild
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return this.mFeedList.size
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var convertViewGroup = convertView
        val listTitle = getGroup(groupPosition) as OrdenReporte
        if (convertView == null) {
            val layoutInflater = this.contex.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertViewGroup = layoutInflater.inflate(R.layout.item_reportetrabajo, null)
        }
        val numeroOt = convertViewGroup!!.findViewById<TextView>(R.id.nroOrden)
        val descripcion = convertViewGroup.findViewById<TextView>(R.id.descripcion)
        numeroOt.text = listTitle.numeroOT
        descripcion.text = listTitle.descripcion
        return convertViewGroup
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return this.mFeedList[groupPosition].listReporteTiempo.count()
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any
    {
        return this.mFeedList[groupPosition].listReporteTiempo[childPosition]
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }


}
 class ViewHolderReporteTrabajoExpande(private val binding: ItemReportetrabajoExpandeBinding, private val contex: Context) : RecyclerView.ViewHolder(binding.root){

     var ordenreporte : OrdenReporte? = null

     fun bindTo(ordenreporte: OrdenReporte?, AdapterReporteTrabajoListener: AdapterReporteTrabajoListener, mFeedList: List<OrdenReporteTiempo>, position: Int)
     {
         this.ordenreporte = ordenreporte
         binding.ordenreporte = ordenreporte
         binding.fecha = MiFecha()

         binding.imgEstado.background = ContextCompat.getDrawable(contex,R.drawable.circle_estado)

         when(binding.ordenreporte!!.estadoOT) {
             1 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_SinAtender)
                 binding.estado.text = String.format("No Iniciada")
             }
             2 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_PermisoSeguridad)
                 binding.estado.text = String.format("Permiso Seguridad")
             }
             3 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Iniciada)
                 binding.estado.text = String.format("Vigente")
             }
             4 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Pendientes)
                 binding.estado.text = String.format("Pendiente")
             }
             5 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Terminadas)
                 binding.estado.text = String.format("Terminada")
             }
             6 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Cerradas)
                 binding.estado.text = String.format("Confirmada")
             }
             7 -> {
                 binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Cerradas)
                 binding.estado.text = String.format("Cancelada")
             }

         }

         binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
         binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
         binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_normal)
         if (ordenreporte!!.isplay!!)
         {
             binding.imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
             binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
             binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_play)
         }
         if (ordenreporte.ispasuse!!)
         {
             binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
             binding.imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
             binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_pause)
         }

         binding.imgPlay.setOnClickListener {
             if(!ordenreporte.isplay!!)
             {
                 if(siHayOTPlay(mFeedList))
                 {
                     AdapterReporteTrabajoListener.playListener(ordenreporte,doPlay = false,position = position)
                 }
                 else
                 {
                     binding.imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
                     binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
                     ordenreporte.activada = true
                     ordenreporte.isplay = true
                     ordenreporte.ispasuse = false
                     AdapterReporteTrabajoListener.playListener(ordenreporte,doPlay = true,position = position)
                 }

             }
         }
         binding.imgPause.setOnClickListener {
             if(!ordenreporte.ispasuse!! and ordenreporte.activada!!)
             {
                 binding.imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
                 binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
                 ordenreporte.activada = true
                 ordenreporte.ispasuse = true
                 ordenreporte.isplay = false
                 AdapterReporteTrabajoListener.pauseListener(ordenreporte,position)
             }

         }


         binding.executePendingBindings()
     }

     private fun siHayOTPlay(mFeedList: List<OrdenReporteTiempo>): Boolean {
         var isplay = false
         mFeedList.forEach {
             if (it.ordenReporte!!.isplay!!) {
                 isplay = true
             }
         }
         return  isplay
     }

     fun clearAnimation(holder: ViewHolderReporteTrabajoExpande)
     {
         holder.itemView.clearAnimation()
     }

     companion object {
         fun from(parent: ViewGroup): ViewHolderReporteTrabajoExpande
         {
             val layoutInflater = LayoutInflater.from(parent.context)
             val binding = ItemReportetrabajoExpandeBinding.inflate(layoutInflater, parent, false)
             return ViewHolderReporteTrabajoExpande(binding,parent.context)
         }
     }
}


class ViewHolderReporteTrabajoExpandeCHild(private val binding: ItemReportetrabajoExpandeBinding, private val contex: Context) : RecyclerView.ViewHolder(binding.root){

    var ordenreporte : OrdenReporte? = null

    fun bindTo(ordenreporte: OrdenReporte?, AdapterReporteTrabajoListener: AdapterReporteTrabajoListener, mFeedList: List<OrdenReporteTiempo>, position: Int)
    {
        this.ordenreporte = ordenreporte
        binding.ordenreporte = ordenreporte
        binding.fecha = MiFecha()

        binding.imgEstado.background = ContextCompat.getDrawable(contex,R.drawable.circle_estado)

        when(binding.ordenreporte!!.estadoOT) {
            1 -> {
                binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_SinAtender)
                binding.estado.text = String.format("No Iniciada")
            }
            2 -> {
                binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_PermisoSeguridad)
                binding.estado.text = String.format("Permiso Seguridad")
            }
            3 -> {
                binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Iniciada)
                binding.estado.text = String.format("Vigente")
            }
            4 -> {
                binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Pendientes)
                binding.estado.text = String.format("Pendiente")
            }
            5 -> {
                binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Terminadas)
                binding.estado.text = String.format("Terminada")
            }
            6 -> {
                binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Cerradas)
                binding.estado.text = String.format("Confirmada")
            }
            7 -> {
                binding.imgEstado.backgroundTintList = ContextCompat.getColorStateList(contex, R.color.chart_Cerradas)
                binding.estado.text = String.format("Cancelada")
            }

        }

        binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
        binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
        binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_normal)
        if (ordenreporte!!.isplay!!)
        {
            binding.imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
            binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
            binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_play)
        }
        if (ordenreporte.ispasuse!!)
        {
            binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
            binding.imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
            binding.cardview.setBackgroundResource(R.drawable.round_item_reporteorden_pause)
        }

        binding.imgPlay.setOnClickListener {
            if(!ordenreporte.isplay!!)
            {
                if(siHayOTPlay(mFeedList))
                {
                    AdapterReporteTrabajoListener.playListener(ordenreporte,doPlay = false,position = position)
                }
                else
                {
                    binding.imgPlay.setBackgroundResource(R.drawable.ic_play_activated)
                    binding.imgPause.setBackgroundResource(R.drawable.ic_pause)
                    ordenreporte.activada = true
                    ordenreporte.isplay = true
                    ordenreporte.ispasuse = false
                    AdapterReporteTrabajoListener.playListener(ordenreporte,doPlay = true,position = position)
                }

            }
        }
        binding.imgPause.setOnClickListener {
            if(!ordenreporte.ispasuse!! and ordenreporte.activada!!)
            {
                binding.imgPause.setBackgroundResource(R.drawable.ic_pause_activated)
                binding.imgPlay.setBackgroundResource(R.drawable.ic_play)
                ordenreporte.activada = true
                ordenreporte.ispasuse = true
                ordenreporte.isplay = false
                AdapterReporteTrabajoListener.pauseListener(ordenreporte,position)
            }

        }


        binding.executePendingBindings()
    }

    private fun siHayOTPlay(mFeedList: List<OrdenReporteTiempo>): Boolean {
        var isplay = false
        mFeedList.forEach {
            if (it.ordenReporte!!.isplay!!) {
                isplay = true
            }
        }
        return  isplay
    }

    fun clearAnimation(holder: ViewHolderReporteTrabajoExpandeCHild)
    {
        holder.itemView.clearAnimation()
    }

    companion object {
        fun from(parent: ViewGroup): ViewHolderReporteTrabajoExpandeCHild
        {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemReportetrabajoExpandeBinding.inflate(layoutInflater, parent, false)
            return ViewHolderReporteTrabajoExpandeCHild(binding,parent.context)
        }
    }
}


//interface itemPlayPauseListener
//{
//    fun playListener(ordenreporte:OrdenReporte?,doPlay:Boolean,position: Int)
//
//    fun pauseListener(ordenreporte: OrdenReporte?)
//}