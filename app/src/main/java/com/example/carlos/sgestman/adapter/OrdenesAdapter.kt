package com.example.carlos.sgestman.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.carlos.sgestman.R
import com.example.carlos.sgestman.databinding.ItemOrdenesBinding
import com.example.carlos.sgestman.entity.Ordenes
import com.example.carlos.sgestman.utils.MiFecha

class OrdenesAdapter(private val contex: Context, private val cellAdpaterListener: OrdenesAdapterListener):PagingDataAdapter<Ordenes, OrdenesReporteSelectViewHolder>(DIFF_CALLBACK), View.OnCreateContextMenuListener {
    private var lastPosition = -1

    override fun onBindViewHolder(holder: OrdenesReporteSelectViewHolder, position: Int) {
        val concert: Ordenes? = getItem(position)

        // Note that "concert" is a placeholder if it's null.
        holder.bindTo(concert)
        holder.itemView.setOnClickListener{
            cellAdpaterListener.onClickOrdenListener(concert)
        }
        holder.itemView.setOnLongClickListener{
            cellAdpaterListener.onLongOrdenListener(concert)
            false
        }
        holder.itemView.setOnCreateContextMenuListener(this)
        setAnimation(holder.itemView, position);

    }
    companion object
    {
        private val DIFF_CALLBACK = object :
                DiffUtil.ItemCallback<Ordenes>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(oldConcert: Ordenes,
                                         newConcert: Ordenes) = oldConcert.id == newConcert.id


            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldConcert: Ordenes, newConcert: Ordenes) = oldConcert == newConcert
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdenesReporteSelectViewHolder
    {
        return OrdenesViewHolder.from(parent)
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    override fun onViewDetachedFromWindow(holder: OrdenesReporteSelectViewHolder)
    {
        holder.clearAnimation(holder)
        super.onViewDetachedFromWindow(holder)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            val animation: Animation = AnimationUtils.loadAnimation(contex, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        val Edit: MenuItem = menu!!.add(Menu.NONE, 1, 1, "Edit")
        val Delete: MenuItem = menu.add(Menu.NONE, 2, 2, "Delete")


    }

}
class OrdenesViewHolder(private val binding: ItemOrdenesBinding, private val contex: Context) : RecyclerView.ViewHolder(binding.root)
{

    private var nameView = itemView.findViewById<TextView>(R.id.nroSolicitude)
    var orden : Ordenes? = null

    /**
     * Items might be null if they are not paged in yet. PagedListAdapter will re-bind the
     * ViewHolder when Item is loaded.
     */
    fun bindTo(orden: Ordenes?) {
        this.orden = orden
        binding.orden = orden
        binding.fecha = MiFecha()

        binding.imgEstado.background = ContextCompat.getDrawable(contex,R.drawable.circle_estado)

        when(binding.orden!!.estado)
        {
            1-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_SinAtender)
                binding.estado.text = String.format("No Iniciada")
            }
            2-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_PermisoSeguridad)
                binding.estado.text = String.format("Permiso Seguridad")
            }
            3-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Iniciada)
                binding.estado.text = String.format("Vigente")
            }
            4-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Pendientes)
                binding.estado.text = String.format("Pendiente")
            }
            5-> {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Terminadas)
                binding.estado.text = String.format("Terminada")
            }
            6->
            {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Cerradas)
                binding.estado.text = String.format("Confirmada")
            }
            7->
            {
                binding.imgEstado.backgroundTintList= ContextCompat.getColorStateList(contex,R.color.chart_Cerradas)
                binding.estado.text = String.format("Cancelada")
            }
        }
       binding.executePendingBindings()


     //   nameView.text = solicitudes?.codigo
    }

    fun clearAnimation(holder: OrdenesReporteSelectViewHolder)
    {
        holder.itemView.clearAnimation()
    }

    companion object {
        fun from(parent: ViewGroup): OrdenesReporteSelectViewHolder
        {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemOrdenesBinding.inflate(layoutInflater, parent, false)
            return OrdenesReporteSelectViewHolder(binding,parent.context )
        }
    }
}

interface OrdenesAdapterListener
{
    fun onClickOrdenListener(orden: Ordenes?)

    fun onLongOrdenListener(orden: Ordenes?)
}
