package com.example.carlos.sgestman.repository.Databases

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carlos.sgestman.entity.ReporteTiempo
import com.example.carlos.sgestman.repository.Databases.InfoRoomDataBase.Companion.getDatabase
import com.example.carlos.sgestman.repository.dao.ReporteTiempoDao

class ReporteTiempoRoomDBRepository(application: Application?) {
    private val reporteTiempoInfoDao: ReporteTiempoDao?
    var allReportes: LiveData<List<ReporteTiempo>?>?
    lateinit var maxId: LiveData<Int>


    fun selectMaxID(numeroOT: String?): LiveData<Int>
    {
        return reporteTiempoInfoDao!!.maxId(numeroOT!!)
    }


    fun insertReporteTiempo(resultModel: ReporteTiempo?) {
        InsertAsyncTask(reporteTiempoInfoDao).execute(resultModel)
    }
    fun deleteReporteTiempo(resultModel: ReporteTiempo?)
    {
        DeleteAsyncTask(reporteTiempoInfoDao).execute(resultModel)
    }

    fun updateOrdenesReporte(resultModel: ReporteTiempo?) {
        UpdateAsyncTask(reporteTiempoInfoDao).execute(resultModel)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: ReporteTiempoDao?) : AsyncTask<ReporteTiempo?, Void?, Void?>() {
        override fun doInBackground(vararg params: ReporteTiempo?): Void?
        {
            mAsyncTaskDao!!.insert(params[0])

            return null
        }
    }

    private class UpdateAsyncTask internal constructor(private val mAsyncTaskDao: ReporteTiempoDao?) : AsyncTask<ReporteTiempo?, Void?, Void?>() {
        override fun doInBackground(vararg params: ReporteTiempo?): Void? {
            mAsyncTaskDao!!.updatePause(params[0]!!.idOrdenReporte!!,params[0]!!.fechaFin!!)
            return null
        }
    }

    private class DeleteAsyncTask internal constructor(private val mAsyncTaskDao: ReporteTiempoDao?):AsyncTask<ReporteTiempo?,Void?,Void?>()
    {
        override fun doInBackground(vararg params: ReporteTiempo?): Void? {
            mAsyncTaskDao!!.delete(params[0])
            return null
        }

    }

    private class SelectAsyncTask internal constructor(private val mAsyncTaskDao: ReporteTiempoDao?,var maxId:Int) : AsyncTask<Int?, Void?, Void?>() {
        override fun doInBackground(vararg params: Int?): Void?
        {
            maxId = mAsyncTaskDao!!.selectMaxId(params[0]!!)
            return null
        }
    }

    init {
        val db = getDatabase(application!!)
        reporteTiempoInfoDao = db!!.reporteTiempoInfoDao()
        allReportes = reporteTiempoInfoDao!!.allOrdenReporte
    }
}