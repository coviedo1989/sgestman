R_DEF: Internal format may change without notice
local
attr? chart_axis
attr? chart_axisBorderSpacing
attr? chart_axisColor
attr? chart_axisLabelsSpacing
attr? chart_axisThickness
attr? chart_axisTopSpacing
attr? chart_barBackgroundColor
attr? chart_barSpacing
attr? chart_cornerRadius
attr? chart_fontSize
attr? chart_labelColor
attr? chart_labels
attr? chart_setSpacing
attr? chart_shadowColor
attr? chart_shadowDx
attr? chart_shadowDy
attr? chart_shadowRadius
attr? chart_typeface
dimen axis_border_spacing
dimen axis_labels_spacing
dimen axis_thickness
dimen axis_top_spacing
dimen bar_spacing
dimen corner_radius
dimen dot_region_radius
dimen font_size
dimen grid_thickness
dimen set_spacing
id inside
id none
id outside
styleable BarChartAttrs chart_barSpacing chart_setSpacing chart_cornerRadius chart_barBackgroundColor
styleable ChartAttrs chart_axis chart_axisThickness chart_axisColor chart_axisBorderSpacing chart_axisTopSpacing chart_axisLabelsSpacing chart_labels chart_labelColor chart_fontSize chart_typeface chart_shadowColor chart_shadowDx chart_shadowDy chart_shadowRadius
